﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="WOGRPSectionDetails.aspx.cs" Inherits="WOGRPSectionDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Plugin Css-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

    <link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- Plugins css-->
    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />

    <link href="assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">WO GRPSection Details</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li>
                                <a href="WOGRPSection.aspx">WO GRPSection</a>
                            </li>
                            <li class="active">WO GRPSection Details
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="m-b-30">
                                    <button class="btn btn-primary waves-effect waves-light" onclick="backtowopage()">Back</button>
                                </div>
                            </div>
                        </div>

                        <div class="" style='overflow: scroll; overflow-y: hidden;'>
                            <table id="tablelist" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>WO No.</th>
                                        <th>Quantity</th>
                                        <th>Supervisor</th>
                                        <th>ProcessStation</th>
                                        <th>Product</th>
                                        <th>Machine Number</th>
                                        <th>Created Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->

        <!-- MODAL -->
        <div class="modal fade" id="modal-delete-wo" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Confirm</h4>
                    </div>

                    <div class="modal-body">
                        Are you sure, you want to delete current record!
                    </div>

                    <div class="modal-footer">
                        <input type="hidden" id="hidDelID" name="hidDelID" value="0" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info" id="btnDeleteWorkOrder" onclick="DeleteWO()">Continue</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end Modal -->

    </div>
    <!-- sample modal content -->
    <div class="modal fade bs-example-modal-lg" id="modal-AddEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="frmAddEdit" name="frmAddEdit" method="post">
                    <input type="hidden" id="hidEditID" name="hidEditID" value="0" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">WO GRP Section Detail</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">WO No.</label>
                                    <input type="text" class="form-control" readonly="readonly" id="txtwo" name="txtwo" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Quantity</label>
                                    <input type="text" class="form-control" readonly="readonly" onkeypress="return isNumberKey(event)" maxlength="4" id="txtquantity" name="txtquantity" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Products</label>
                                    <select class="selectpicker" id="lstproduct" disabled="disabled" name="lstproduct" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Process Station</label>
                                    <input type="text" class="form-control" id="txtprocessstation" readonly="" name="txtprocessstation" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Machine</label>
                                    <input type="text" class="form-control" id="txtmachine" readonly="" name="txtmachine" placeholder="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Supervisor</label>
                                    <select class="selectpicker" id="lstsupr" disabled="disabled" name="lstsupr" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Hours</label>
                                    <input type="text" class="form-control" id="txthrs" readonly="" name="txthrs" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" style="color: red" class="control-label">Status</label>
                                    <select class="selectpicker" id="lstwost" name="lstwost" data-live-search="true" data-style="btn-white">
                                        <option value="0">Not Completed</option>
                                        <option value="1">Completed</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <h4 class="modal-header" id="lblproduction">Production</h4>

                        <div class="row">
                            <div class="col-md-2" id="chkbxmixingoflmp1">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxmixingoflmp" type="checkbox" />
                                        <label for="chkbxmixingoflmp">
                                            Mixing of LMP
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="chkbxmixingofwhitepigment1">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxmixingofwhitepigment" type="checkbox" />
                                        <label for="chkbxmixingofwhitepigment">
                                            Mixing White Pigment
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="chkbxstartcompound1">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxstartcompound" type="checkbox" />
                                        <label for="chkbxstartcompound">
                                            Start Compound
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="chkbxstartsheet1">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxstartsheet" type="checkbox" />
                                        <label for="chkbxstartsheet">
                                            Start Sheet
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="chkbxcheckoil1">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxcheckoil" type="checkbox" />
                                        <label for="chkbxcheckoil">
                                            Check Oil
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="chkbxstartcirculation1">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxstartcirculation" type="checkbox" />
                                        <label for="chkbxstartcirculation">
                                            Start Circulation
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="chkbxcheckleakages1">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxcheckleakages" type="checkbox" />
                                        <label for="chkbxcheckleakages">
                                            Check Leakages
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="chkbxstartboiler1">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxstartboiler" type="checkbox" />
                                        <label for="chkbxstartboiler">
                                            Start Boiler
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" id="chkbxwaittemp1">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxwaittemp" type="checkbox" />
                                        <label for="chkbxwaittemp">
                                            Wait for require Temperature
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" id="chkbxstartmachine1">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxstartmachine" type="checkbox" />
                                        <label for="chkbxstartmachine">
                                            Start Machine and Run Process
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2" id="chkbxrepair1">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxrepair" type="checkbox" />
                                        <label for="chkbxrepair">Repair</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="chkbxsales1">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxsales" type="checkbox" />
                                        <label for="chkbxsales">Sales</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="modal-header" id="lblinstallation">Installation / Delivery</h4>

                        <div class="row">


                            <div class="col-md-2" id="dvdeliveryfactory">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxdeliveryfactory" type="checkbox" />
                                        <label for="chkbxdeliveryfactory">
                                            Delivery Factory
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="dvdeliverysite">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxdeliverysite" type="checkbox" />
                                        <label for="chkbxdeliverysite">
                                            Delivery Site
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="dvsteel">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxsteel" type="checkbox" />
                                        <label for="chkbxsteel">
                                            Steel
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" id="dvcompleteassemble">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxcompleteassemble" type="checkbox" />
                                        <label for="chkbxcompleteassemble">
                                            Complete Assemble On Site
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="dvdeliveryaccessories">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxdeliveryaccessories" type="checkbox" />
                                        <label for="chkbxdeliveryaccessories">
                                            Delivery Accessories
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                    <label for="field-7" class="control-label">Note</label>
                                    <textarea class="form-control autogrow" id="txtnote" name="txtnote" placeholder="Write something about Work Order"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <button type="button" class="btn btn-default waves-effect" id="btncloseworkorder" data-dismiss="modal" onclick="resetForm()">Close</button>
                            <button class="btn btn-primary waves-effect waves-light" id="btneditworkorder" type="submit" hidden="hidden">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>

    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/moment.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.js"></script>
    <script src="Scripts/filedownload.js"></script>

    <script>
        var woid = '<%= Request["wo"] %>';

        $(document).ready(function () {
            var resizefunc = [];
            var currentDate = new Date();
            var processstationid = 0, grptypeid = 0;
            // Date Picker

            $('#txtfrmdate').datetimepicker({ format: 'DD/MM/YYYY' });
            $('#txttodate').datetimepicker({ format: 'DD/MM/YYYY' });

            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#frmAddEdit").validate({
                rules: {
                    "txtprocessstation": "required",
                    "txtquantity": "required",
                },
                messages: {
                    "txtprocessstation": "Process Station is required",
                    "txtquantity": "Quant. is required",
                },
                submitHandler: function (form) {
                    if ($("#hidEditID").val() > 0) {
                        EditWO();
                    }
                    else {
                        AddWO();
                    }
                    return false;
                }
            });
            bindProducts();
            bindTabletUsers();
            hideshow();

            if (woid != null && woid != "") {
                getHistoryData(woid);
            }
            else {
                getHistoryData(-1);
            }
        });


        function hideshow() {

            if (($("#txtprocessstation").val() == "Process Station 1") && ($('#txtmachine').val() == "Machine-1")) {
                //alert($("#txtprocessstation").val() + " " + $('#txtmachine').val());

                $("#chkbxmixingoflmp1").show();
                $("#chkbxmixingofwhitepigment1").show();
                $("#chkbxstartcompound1").show();
                $("#chkbxstartsheet1").show();
                $("#chkbxmixingoflmp").prop("checked", true);
                $("#chkbxmixingofwhitepigment").prop("checked", true);
                $("#chkbxstartcompound").prop("checked", true);
                $("#chkbxstartsheet").prop("checked", true);
                $("#chkbxcheckoil1").hide();
                $("#chkbxstartcirculation1").hide();
                $("#chkbxcheckleakages1").hide();
                $("#chkbxstartboiler1").hide();
                $("#chkbxwaittemp1").hide();
                $("#chkbxstartmachine1").hide();
                $("#chkbxcheckoil").prop("checked", false);
                $("#chkbxstartcirculation").prop("checked", false);
                $("#chkbxcheckleakages").prop("checked", false);
                $("#chkbxstartboiler").prop("checked", false);
                $("#chkbxwaittemp").prop("checked", false);
                $("#chkbxstartmachine").prop("checked", false);
            }
            else if (($("#txtprocessstation").val() == "Process Station 2") && ($('#txtmachine').val() == "Machine-1")) {
                $("#chkbxmixingoflmp1").hide();
                $("#chkbxmixingofwhitepigment1").hide();
                $("#chkbxstartcompound1").hide();
                $("#chkbxstartsheet1").hide();
                $("#chkbxmixingoflmp").prop("checked", false);
                $("#chkbxmixingofwhitepigment").prop("checked", false);
                $("#chkbxstartcompound").prop("checked", false);
                $("#chkbxstartsheet").prop("checked", false);
                $("#chkbxcheckoil1").show();
                $("#chkbxstartcirculation1").show();
                $("#chkbxcheckleakages1").show();
                $("#chkbxstartboiler1").show();
                $("#chkbxwaittemp1").show();
                $("#chkbxstartmachine1").hide();
                $("#chkbxcheckoil").prop("checked", true);
                $("#chkbxstartcirculation").prop("checked", true);
                $("#chkbxcheckleakages").prop("checked", true);
                $("#chkbxstartboiler").prop("checked", true);
                $("#chkbxwaittemp").prop("checked", true);
                $("#chkbxstartmachine").prop("checked", false);
            }
            else {
                $("#chkbxmixingoflmp1").hide();
                $("#chkbxmixingofwhitepigment1").hide();
                $("#chkbxstartcompound1").hide();
                $("#chkbxstartsheet1").hide();
                $("#chkbxcheckoil1").hide();
                $("#chkbxstartcirculation1").hide();
                $("#chkbxcheckleakages1").hide();
                $("#chkbxstartboiler1").hide();
                $("#chkbxwaittemp1").hide();
                $("#chkbxstartmachine1").show();
                $("#chkbxmixingoflmp").prop("checked", false);
                $("#chkbxmixingofwhitepigment").prop("checked", false);
                $("#chkbxstartcompound").prop("checked", false);
                $("#chkbxstartsheet").prop("checked", false);
                $("#chkbxcheckoil").prop("checked", false);
                $("#chkbxstartcirculation").prop("checked", false);
                $("#chkbxcheckleakages").prop("checked", false);
                $("#chkbxstartboiler").prop("checked", false);
                $("#chkbxwaittemp").prop("checked", false);
                $("#chkbxstartmachine").prop("checked", true);
            }

        }

        function resetForm() {
            $("#hidEditID").val("0");
            $("#hidDelID").val("0");
            $("#txtwo").val("");
            $("#txtprocessstation").val("");
            $("#txtmachine").val("");
            $("#txtquantity").val("");
            $("#txthrs").val("");
            $("#txtnote").val("");
            processstationid = 0;
            grptypeid = 0;
            bindProducts();
            bindTabletUsers();

            $("#chkbxdeliverysite").prop("checked", false);
            $("#chkbxdeliveryfactory").prop("checked", false);
            $("#chkbxsteel").prop("checked", false);
            $("#chkbxcompleteassemble").prop("checked", false);
            $("#chkbxdeliveryaccessories").prop("checked", false);
            $("#chkbxrepair").prop("checked", false);
            $("#chkbxsales").prop("checked", false);
            hideshow();

            var validator = $("#frmAddEdit").validate();
            validator.resetForm();
        }

        function getHistoryData(woid) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWOGRPSectionDetail',
                data: 'id=0&woid=' + woid,
                async: true,
                success: function (data) {

                    $('#tablelist').dataTable().fnClearTable();
                    $('#tablelist').dataTable().fnDestroy();

                    $.each(data, function (i, item) {
                        var trHTML = '';


                        var date = new Date(parseInt(item.CreatedDate.replace("/Date(", "").replace(")/", ""), 10));
                        var ds = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + ' ' + formatAMPM(date);

                        trHTML += '<tr><td>' + item.WOID + '</td><td>' + item.Quantity + '</td><td>' + item.SupervisorName + '</td><td>' + item.ProcessStationName + '</td><td>' + item.ProductName + '</td><td>' + item.MachineNumber + '</td><td>' + ds + '</td><td>' +
                            '<button type="button" class="btn btn-info waves-effect btnEdit" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="FillWO(' + item.ID + ')"><i class="fa fa-pencil-square-o"></i></button>'
                            //+ '<button class="btn btn-danger waves-effect waves-light btnDelete" onclick="confirmWODelete(' + item.ID + ')"><i class="fa fa-times"></i></button>'
                        + '</td></tr>';
                        $('#tablelist').append(trHTML);
                    });
                    $('#tablelist').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "bSort": false
                        //order: [[0, 'desc']]
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function backtowopage() {
            window.location.href = 'WOGRPSection.aspx';
        }

        function FillWO(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWOGRPSectionDetail',
                data: 'id=' + id + '&woid=0',
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        $("#hidEditID").val(id);
                        $("#txtwo").val(item.WOID);
                        FillWOSteps(item.WOID);
                        $("#txtquantity").val(item.Quantity);
                        $("#lstproduct").val(item.ProductName);
                        $("#lstproduct").selectpicker('refresh');
                        $("#txtmachine").val(item.MachineNumber);
                        $("#txtnote").val(item.Notes);
                        $("#txthrs").val(item.Hours);
                        $("#txtprocessstation").val(item.ProcessStationName);

                        $('#lstsupr').val(item.SupervisorID);
                        $('#lstsupr').selectpicker('refresh');
                        $('#lstwost').val(item.isCompleted == 0 ? 0 : 1);
                        $('#lstwost').selectpicker('refresh');

                        if (item.isMixingofLMPs == 0 ? $("#chkbxmixingoflmp").prop("checked", false) : $("#chkbxmixingoflmp").prop("checked", true));
                        if (item.isMixingofWhitePigment == 0 ? $("#chkbxmixingofwhitepigment").prop("checked", false) : $("#chkbxmixingofwhitepigment").prop("checked", true));
                        if (item.isStartCompound == 0 ? $("#chkbxstartcompound").prop("checked", false) : $("#chkbxdoprocess").prop("checked", true));
                        if (item.isStartSheet == 0 ? $("#chkbxstartsheet").prop("checked", false) : $("#chkbxoutbags").prop("checked", true));
                        if (item.isCheckOil == 0 ? $("#chkbxcheckoil").prop("checked", false) : $("#chkbxframeinside").prop("checked", true));
                        if (item.isStartCiculation == 0 ? $("#chkbxstartcirculation").prop("checked", false) : $("#chkbxframeoutside").prop("checked", true));
                        if (item.isCheckLeakage == 0 ? $("#chkbxcheckleakages").prop("checked", false) : $("#chkbxtank").prop("checked", true));
                        if (item.isStartboiler == 0 ? $("#chkbxstartboiler").prop("checked", false) : $("#chkbxtank").prop("checked", true));
                        if (item.isWaitTemperature == 0 ? $("#chkbxwaittemp").prop("checked", false) : $("#chkbxtank").prop("checked", true));
                        if (item.isStartMachine == 0 ? $("#chkbxstartmachine").prop("checked", false) : $("#chkbxtank").prop("checked", true));
                        if (item.isRepair == 0 ? $("#chkbxrepair").prop("checked", false) : $("#chkbxrepair").prop("checked", true));
                        if (item.isSales == 0 ? $("#chkbxsales").prop("checked", false) : $("#chkbxsales").prop("checked", true));

                        if (item.isDeliveryPanelinFactory == 0 ? $("#chkbxdeliveryfactory").prop("checked", false) : $("#chkbxdeliveryfactory").prop("checked", true));
                        if (item.isDeliveryPanelinSite == 0 ? $("#chkbxdeliverysite").prop("checked", false) : $("#chkbxdeliverysite").prop("checked", true));
                        if (item.isSteel == 0 ? $("#chkbxsteel").prop("checked", false) : $("#chkbxsteel").prop("checked", true));
                        if (item.isCompleteAssembleonSite == 0 ? $("#chkbxcompleteassemble").prop("checked", false) : $("#chkbxcompleteassemble").prop("checked", true));
                        if (item.isDeliveryAccessories == 0 ? $("#chkbxdeliveryaccessories").prop("checked", false) : $("#chkbxdeliveryaccessories").prop("checked", true));

                        //hideshow();
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function FillWOSteps(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWOGRPSections',
                data: 'id=' + id,
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        if (item.isMixingofLMPs == 0 ? $("#chkbxmixingoflmp1").hide() : $("#chkbxmixingoflmp1").show());
                        if (item.isMixingofWhitePigment == 0 ? $("#chkbxmixingofwhitepigment1").hide() : $("#chkbxmixingofwhitepigment1").show());
                        if (item.isStartCompound == 0 ? $("#chkbxstartcompound1").hide() : $("#chkbxdoprocess1").show());
                        if (item.isStartSheet == 0 ? $("#chkbxstartsheet1").hide() : $("#chkbxstartsheet1").show());
                        if (item.isCheckOil == 0 ? $("#chkbxcheckoil1").hide() : $("#chkbxcheckoil1").show());
                        if (item.isStartCiculation == 0 ? $("#chkbxstartcirculation1").hide() : $("#chkbxstartcirculation1").show());
                        if (item.isCheckLeakage == 0 ? $("#chkbxcheckleakages1").hide() : $("#chkbxcheckleakages1").show());
                        if (item.isStartboiler == 0 ? $("#chkbxstartboiler1").hide() : $("#chkbxstartboiler1").show());
                        if (item.isWaitTemperature == 0 ? $("#chkbxwaittemp1").hide() : $("#chkbxwaittemp1").show());
                        if (item.isStartMachine == 0 ? $("#chkbxstartmachine1").hide() : $("#chkbxstartmachine1").show());
                        if (item.isRepair == 0 ? $("#chkbxrepair1").hide() : $("#chkbxrepair1").show());
                        if (item.isSales == 0 ? $("#chkbxsales1").hide() : $("#chkbxsales1").show());

                        if (item.isDeliveryPanelinFactory == 0 ? $("#dvdeliveryfactory1").hide() : $("#dvdeliveryfactory").show());
                        if (item.isDeliveryPanelinSite == 0 ? $("#dvdeliverysite").hide() : $("#dvdeliverysite").show());
                        if (item.isSteel == 0 ? $("#dvsteel").hide() : $("#dvsteel").show());
                        if (item.isCompleteAssembleonSite == 0 ? $("#dvcompleteassemble").hide() : $("#dvcompleteassemble").show());
                        if (item.isDeliveryAccessories == 0 ? $("#dvdeliveryaccessories").hide() : $("#dvdeliveryaccessories").show());
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function AddWO() {
            var ismixingoflmp = $(chkbxmixingoflmp).prop("checked") == false ? "0" : "1";
            var ismixingofwhitepigment = $(chkbxmixingofwhitepigment).prop("checked") == false ? "0" : "1";
            var isstartcompound = $(chkbxstartcompound).prop("checked") == false ? "0" : "1";
            var isstartsheet = $(chkbxstartsheet).prop("checked") == false ? "0" : "1";
            var ischeckoil = $(chkbxcheckoil).prop("checked") == false ? "0" : "1";
            var isstartcirculation = $(chkbxstartcirculation).prop("checked") == false ? "0" : "1";
            var ischeckleakage = $(chkbxcheckleakages).prop("checked") == false ? "0" : "1";
            var isstartboiler = $(chkbxstartboiler).prop("checked") == false ? "0" : "1";
            var iswaittemp = $(chkbxwaittemp).prop("checked") == false ? "0" : "1";
            var isstartmachine = $(chkbxstartmachine).prop("checked") == false ? "0" : "1";
            var isrepair = $(chkbxrepair).prop("checked") == false ? "0" : "1";
            var issales = $(chkbxsales).prop("checked") == false ? "0" : "1";

            var isdeliverysite = $(chkbxdeliverysite).prop("checked") == false ? "0" : "1";
            var isdeliveryfactory = $(chkbxdeliveryfactory).prop("checked") == false ? "0" : "1";
            var issteel = $(chkbxsteel).prop("checked") == false ? "0" : "1";
            var iscompleteassemble = $(chkbxcompleteassemble).prop("checked") == false ? "0" : "1";
            var isdeliveryaccessories = $(chkbxdeliveryaccessories).prop("checked") == false ? "0" : "1";

            //var data2 = 'id=0&quantity=' + $("#txtquantity").val() + '&processstaionID=' + processstationid + '&productname=' + $("#lstproduct").val() + '&machinenumber=' + $("#txtmachine").val() + '&ismixingoflmp=' + ismixingoflmp + '&ismixingofwhitepigment=' + ismixingofwhitepigment + '&isstartcompound=' + isstartcompound + '&isstartsheet=' + isstartsheet + '&ischeckoil=' + ischeckoil + '&isstartcirculation=' + isstartcirculation + '&ischeckleakage=' + ischeckleakage + '&isstartboiler=' + isstartboiler + '&iswaittemp=' + iswaittemp + '&isstartmachine=' + isstartmachine + '&notes=' + $("#txtnote").val();
            //alert(data2);

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditWOGRPSectionDetail',
                data: 'id=0&quantity=' + $("#txtquantity").val() + '&processstaionID=' + processstationid + '&productname=' + $("#lstproduct").val()
                + '&machinenumber=' + $("#txtmachine").val() + '&ismixingoflmp=' + ismixingoflmp + '&ismixingofwhitepigment=' + ismixingofwhitepigment
                + '&isstartcompound=' + isstartcompound + '&isstartsheet=' + isstartsheet + '&ischeckoil=' + ischeckoil + '&isstartcirculation=' + isstartcirculation
                + '&ischeckleakage=' + ischeckleakage + '&isstartboiler=' + isstartboiler + '&iswaittemp=' + iswaittemp + '&isstartmachine=' + isstartmachine
                + '&isdeliveryfactory=' + isdeliveryfactory + '&isdeliverysite=' + isdeliverysite + '&issteel=' + issteel + '&iscompleteassembleonsite=' + iscompleteassemble + '&isdeliveryaccessories=' + isdeliveryaccessories
                + '&notes=' + $("#txtnote").val() + '&isrepair=' + isrepair + '&issales=' + issales + '&supervisorid=' + $("#lstsupr").val() + '&hours=' + $("#txthrs").val() + '&grptypeid=' + grptypeid
                  + '&woid=' + $("#txtwo").val() + '&iscompleted=' + $("#lstwost").val(),
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "Work Order has been saved successfully");
                        resetForm();
                        getHistoryData(woid);
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function EditWO() {
            var id = $("#hidEditID").val();

            var ismixingoflmp = $(chkbxmixingoflmp).prop("checked") == false ? "0" : "1";
            var ismixingofwhitepigment = $(chkbxmixingofwhitepigment).prop("checked") == false ? "0" : "1";
            var isstartcompound = $(chkbxstartcompound).prop("checked") == false ? "0" : "1";
            var isstartsheet = $(chkbxstartsheet).prop("checked") == false ? "0" : "1";
            var ischeckoil = $(chkbxcheckoil).prop("checked") == false ? "0" : "1";
            var isstartcirculation = $(chkbxstartcirculation).prop("checked") == false ? "0" : "1";
            var ischeckleakage = $(chkbxcheckleakages).prop("checked") == false ? "0" : "1";
            var isstartboiler = $(chkbxstartboiler).prop("checked") == false ? "0" : "1";
            var iswaittemp = $(chkbxwaittemp).prop("checked") == false ? "0" : "1";
            var isstartmachine = $(chkbxstartmachine).prop("checked") == false ? "0" : "1";
            var isrepair = $(chkbxrepair).prop("checked") == false ? "0" : "1";
            var issales = $(chkbxsales).prop("checked") == false ? "0" : "1";

            var isdeliverysite = $(chkbxdeliverysite).prop("checked") == false ? "0" : "1";
            var isdeliveryfactory = $(chkbxdeliveryfactory).prop("checked") == false ? "0" : "1";
            var issteel = $(chkbxsteel).prop("checked") == false ? "0" : "1";
            var iscompleteassemble = $(chkbxcompleteassemble).prop("checked") == false ? "0" : "1";
            var isdeliveryaccessories = $(chkbxdeliveryaccessories).prop("checked") == false ? "0" : "1";

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditWOGRPSectionDetail',
                data: 'id=' + id + '&quantity=' + $("#txtquantity").val() + '&processstaionID=' + processstationid + '&productname=' + $("#lstproduct").val()
                + '&machinenumber=' + $("#txtmachine").val() + '&ismixingoflmp=' + ismixingoflmp + '&ismixingofwhitepigment=' + ismixingofwhitepigment
                + '&isstartcompound=' + isstartcompound + '&isstartsheet=' + isstartsheet + '&ischeckoil=' + ischeckoil + '&isstartcirculation=' + isstartcirculation
                + '&ischeckleakage=' + ischeckleakage + '&isstartboiler=' + isstartboiler + '&iswaittemp=' + iswaittemp + '&isstartmachine=' + isstartmachine
                + '&isdeliveryfactory=' + isdeliveryfactory + '&isdeliverysite=' + isdeliverysite + '&issteel=' + issteel + '&iscompleteassembleonsite=' + iscompleteassemble + '&isdeliveryaccessories=' + isdeliveryaccessories
                + '&notes=' + $("#txtnote").val() + '&isrepair=' + isrepair + '&issales=' + issales + '&supervisorid=' + $("#lstsupr").val() + '&hours=' + $("#txthrs").val() + '&grptypeid=' + grptypeid
                 + '&woid=' + $("#txtwo").val() + '&iscompleted=' + $("#lstwost").val(),
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "Work Order has been updated successfully");
                        resetForm();
                        getHistoryData(woid);
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function formatAMPM(date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        }

        function confirmWODelete(id) {
            $("#hidDelID").val(id);
            jQuery('#modal-delete-wo').modal('show', { backdrop: 'static' });
        }

        function DeleteWO() {
            var id = $('input#hidDelID').val();

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/DeleteWOTanks',
                data: "id=" + id,
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('warning', 'top right', 'Deleted', 'Work Order Tanks deleted successfully.');
                        getSearchWorkOrder();
                        $('#modal-delete-wo').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function bindProducts() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetGRPSectionTypes',
                data: 'id=0&productname=',
                async: true,
                success: function (data) {
                    $("#lstproduct").append("").val("").html("");
                    //$("#lstproduct").append($("<option></option>").val("-1").html("Select"));

                    $("#lstsearchproduct").append("").val("").html("");
                    $("#lstsearchproduct").append($("<option></option>").val("-1").html("Select"));

                    $.each(data, function (key, val) {
                        $("#lstproduct").append($("<option></option>").val(val.ProductName).html(val.ProductName));

                        $("#lstsearchproduct").append($("<option></option>").val(val.ProductName).html(val.ProductName));
                    });
                    $('#lstproduct').selectpicker('refresh');

                    $('#lstsearchproduct').selectpicker('refresh');
                    getproductinfo($('#lstproduct').val());
                }
            });
        }

        $('#lstproduct').change(function () {
            getproductinfo($('#lstproduct').val());
        });

        function getproductinfo(pname) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetGRPSectionTypes',
                data: 'id=0&productname=' + pname,
                async: true,
                success: function (data) {
                    $("#txtprocessstation").val("");
                    $("#txtmachine").val("");

                    $.each(data, function (key, val) {
                        $("#txtprocessstation").val(val.ProcessStationName);
                        $("#txtmachine").val(val.MachineNumber);
                        processstationid = val.ProcessStationID;
                        grptypeid = val.ID;
                    });
                    hideshow();

                }
            });
        }

        function bindTabletUsers() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTabletUsers',
                data: "user=0&tabletroleid=1",
                async: true,
                success: function (data) {
                    $("#lstsupr").append("").val("").html("");

                    $.each(data, function (key, val) {
                        $("#lstsupr").append($("<option></option>").val(val.ID).html(val.Name));
                    });
                    $('#lstsupr').selectpicker('refresh');
                }
            });
        }


    </script>
</asp:Content>




