﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TaskMandoop.aspx.cs" Inherits="TaskMandoop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Plugin Css-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <%-- <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />--%>
    <link href="assets/plugins/datatables/newdatatables/buttons.bootstrap4.min.css" rel="stylesheet" />
    <link href="assets/plugins/datatables/newdatatables/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <link href="assets/plugins/datatables/newdatatables/responsive.bootstrap4.min.css" rel="stylesheet" />

    <link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- Plugins css-->
    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />

    <link href="assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Task Mandoop</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li>
                                <a href="#">Create Tasks</a>
                            </li>
                            <li class="active">Task Mandoop
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Task No.</label>
                                    <input type="text" class="form-control" id="txtsearchwono" name="txtsearchwono" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Task Type</label>
                                    <select id="lstsrchtsktype" onkeydown="return (event.keyCode!=13);" name="lstsrchtsktype" class="selectpicker" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Status</label>
                                    <select id="lstsrchstatus" onkeydown="return (event.keyCode!=13);" name="lstsrchstatus" class="selectpicker" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label for="field-1" class="control-label">Created Date From</label>

                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="From Date" id="txtfrmdate" />
                                    <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label for="field-1" class="control-label">Created Date To</label>

                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="To Date" id="txttodate" />
                                    <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <button class="btn btn-primary waves-effect waves-light" onclick="clearsearch()">Clear</button>
                                <button class="btn btn-primary waves-effect waves-light" onclick="getSearchWorkOrder()">Search</button>
                            </div>
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="resetForm()">Add New</button>
                            </div>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->

                <div class="panel">
                    <div class="panel-body">
                        <div class="" style='overflow-x: scroll; overflow-y: hidden;'>
                            <table id="tablelist" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Task No.</th>
                                        <th>Assign Date</th>
                                        <th>Customer Name</th>
                                        <th>Task For</th>
                                        <th>Type</th>
                                        <th class="th_ID">Description</th>
                                        <th class="th_ID">Remark</th>
                                        <th>Created Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->

        <!-- MODAL -->
        <div class="modal fade" id="modal-delete-wo" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Confirm</h4>
                    </div>

                    <div class="modal-body">
                        Are you sure, you want to delete current record!
                    </div>

                    <div class="modal-footer">
                        <input type="hidden" id="hidDelID" name="hidDelID" value="0" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info" id="btnDeleteWorkOrder" onclick="DeleteWO()">Continue</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end Modal -->

    </div>
    <!-- sample modal content -->
    <div class="modal fade bs-example-modal-lg" id="modal-AddEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="width: 75%;">
            <div class="modal-content">
                <form id="frmAddEdit" name="frmAddEdit" method="post">
                    <input type="hidden" id="hidEditID" name="hidEditID" value="0" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Task Mandoop</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Customer Name</label>
                                    <input type="text" class="form-control" onkeydown="return (event.keyCode!=13);" id="txtcustmr" maxlength="50" name="txtcustmr" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="field-1" class="control-label">Assign Date</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" onkeydown="return (event.keyCode!=13);" placeholder="Date" name="txtdt" id="txtdt" />
                                    <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Task For</label>
                                    <select id="lsttskfr" onkeydown="return (event.keyCode!=13);" name="lsttskfr" class="selectpicker" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label" style="color: red">Status</label>
                                    <select id="lstst" onkeydown="return (event.keyCode!=13);" name="lstst" class="selectpicker" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Task Type</label>
                                    <select id="lsttsktype" onkeydown="return (event.keyCode!=13);" name="lsttsktype" class="selectpicker" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                    <label for="field-7" class="control-label">Description</label>
                                    <textarea class="form-control autogrow" id="txtdesc" name="txtdesc" placeholder="Description for Task"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                    <label for="field-7" class="control-label">Remark</label>
                                    <textarea class="form-control autogrow" id="txtrmk" name="txtrmk" placeholder=""></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="" style='overflow-x: scroll; overflow-y: hidden;'>
                            <table id="tblimg" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Uploaded Image</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <button type="button" class="btn btn-default waves-effect" id="btncloseworkorder" data-dismiss="modal" onclick="resetForm()">Close</button>
                            <button class="btn btn-primary waves-effect waves-light" id="btneditworkorder" type="submit" hidden="hidden">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <!-- Required datatable js -->
    <script src="assets/plugins/datatables/newdatatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="assets/plugins/datatables/newdatatables/dataTables.buttons.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/buttons.bootstrap4.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/jszip.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/pdfmake.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/vfs_fonts.js"></script>
    <script src="assets/plugins/datatables/newdatatables/buttons.html5.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/buttons.print.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="assets/plugins/datatables/newdatatables/dataTables.responsive.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/responsive.bootstrap4.min.js"></script>

    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>

    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/moment.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.js"></script>
    <script src="Scripts/filedownload.js"></script>

    <script>
        var roleid = '<%=Session["Admin_Role"].ToString() %>';
        $(document).ready(function () {
            var resizefunc = [];
            var currentDate = new Date();

            // Date Picker

            $('#txtfrmdate').datetimepicker({ format: 'DD/MM/YYYY' });
            $('#txttodate').datetimepicker({ format: 'DD/MM/YYYY' });
            $('#txtdt').datetimepicker({ format: 'DD/MM/YYYY hh:mm A' });

            //$(window).keydown(function (event) {
            //    if (event.keyCode == 13) {
            //        event.preventDefault();
            //        return false;
            //    }
            //});

            $("#frmAddEdit").validate({
                rules: {
                    "txtcustmr": "required",
                    "txtdt": "required"
                },
                messages: {
                    "txtcustmr": "Customer Name is required",
                    "txtdt": "Date is required"
                },
                submitHandler: function (form) {
                    if ($("#hidEditID").val() > 0) {
                        EditWO();
                    }
                    else {
                        AddWO();
                    }
                    return false;
                }
            });
            bindtasktypes();
            bindTaskStatus();
            bindTabletUsers();
            getSearchWorkOrder();
        });


        function allowdotnnumeric(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /^[0-9.,]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function formatAMPM(date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function clearsearch() {
            $('#txtfrmdate').val("");
            $('#txttodate').val(""); 
            $('#txtsearchwono').val("");
            $('#lstsrchtsktype').val("");
            $('#lstsrchtsktype').selectpicker('refresh');
            $('#lstsrchstatus').val(0);
            $('#lstsrchstatus').selectpicker('refresh');
            getSearchWorkOrder();
        }

        function getSearchWorkOrder() {
            debugger;
            var frmdate = $('#txtfrmdate').val();
            var todate = $('#txttodate').val();

            var fd = "", td = "";
            if (frmdate == null || frmdate == "") {

            } else {
                frmdate = $("#txtfrmdate").val().split('/');
                fd = frmdate[1] + '/' + frmdate[0] + '/' + frmdate[2] + ' 00:00:00';

            }
            if (todate == null || todate == "") {

            }
            else {
                todate = $("#txttodate").val().split('/');
                td = todate[1] + '/' + todate[0] + '/' + todate[2] + ' 23:59:59';
            }
            //alert($('#txtsearchwono').val());
            var id = 0;

            if ($('#txtsearchwono').val() == null || $('#txtsearchwono').val() == "") {
            } else {
                id = $('#txtsearchwono').val();
            }

            var typeid = $("#lstsrchtsktype").val();
            if ($('#lstsrchtsktype').val() == null || $('#lstsrchtsktype').val() == "") {
                typeid = 0;
            } else {
                typeid = $('#lstsrchtsktype').val();
            }
            var statusid = $("#lstsrchstatus").val();
            if ($('#lstsrchstatus').val() == null || $('#lstsrchstatus').val() == "") {
                statusid = 0;
            } else {
                statusid = $('#lstsrchstatus').val();
            }

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTaskMandoop',
                data: 'id=' + id + '&tasktypeid=' + typeid + '&taskstatus=' + statusid + '&frmdate=' + fd + '&todate=' + td,
                async: true,
                success: function (data) {

                    $('#tablelist').dataTable().fnClearTable();
                    $('#tablelist').dataTable().fnDestroy();

                    $.each(data, function (i, item) {
                        var trHTML = '';
                        var ds = '', ct = '';
                        if (item.DateTime != null) {
                            var date = new Date(parseInt(item.DateTime.replace("/Date(", "").replace(")/", ""), 10));
                            ds = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + ' ' + formatAMPM(date);
                        }
                        if (item.CreatedDate != null) {
                            var date = new Date(parseInt(item.CreatedDate.replace("/Date(", "").replace(")/", ""), 10));
                            ct = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + ' ' + formatAMPM(date);
                        }
                        trHTML += '<tr><td>' + item.ID + '</td><td>' + ds + '</td><td>' + item.CustomerName + '</td><td>' + item.TaskForName + '</td><td>' + item.TypeName + '</td><td class="td_ID">' + item.Description + '</td><td class="td_ID">' + item.Remark + '</td><td>' + ct + '</td><td>' + item.StatusName + '</td><td>' +
                            '<button type="button" class="btn btn-info waves-effect " data-toggle="modal" data-target=".bs-example-modal-lg" onclick="FillWO(' + item.ID + ')"><i class="fa fa-pencil-square-o"></i></button>'
                            + '&nbsp; <button class="btn btn-danger waves-effect waves-light " onclick="confirmWODelete(' + item.ID + ')"><i class="fa fa-times"></i></button>' + '</td></tr>';
                        $('#tablelist').append(trHTML);
                        $(".th_ID").hide();
                        $(".td_ID").hide();
                    });
                    var table = $('#tablelist').DataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "bSort": false,
                        buttons: ['excel']
                    });

                    table.buttons().container()
                            .appendTo('#tablelist_wrapper .col-md-6:eq(0)');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function resetForm() {
            $("#hidEditID").val("0");
            $("#hidDelID").val("0");
            $("#txtcustmr").val("");
            $("#txtdt").val("");

            $('#lsttskfr').val("");
            $('#lsttskfr').selectpicker('refresh');
            $('#lstst').val(1);
            $('#lstst').selectpicker('refresh');
            $('#lsttsktype').val(1);
            $('#lsttsktype').selectpicker('refresh');
            $("#txtdesc").val("");
            $("#txtrmk").val("");
            getimages($("#hidEditID").val());

            var validator = $("#frmAddEdit").validate();
            validator.resetForm();
        }

        function FillWO(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTaskMandoop',
                data: 'id=' + id + '&tasktypeid=0&taskstatus=0&frmdate=&todate=',
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        $("#hidEditID").val(id);
                        $("#txtcustmr").val(item.CustomerName);
                        var ds = '';
                        if (item.DateTime != null) {
                            var date = new Date(parseInt(item.DateTime.replace("/Date(", "").replace(")/", ""), 10));
                            ds = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + ' ' + formatAMPM(date);
                        }
                        $("#txtdt").val(ds);

                        $('#lsttskfr').val(item.TaskForUserID);
                        $('#lsttskfr').selectpicker('refresh');
                        $('#lstst').val(item.TaskStatusID);
                        $('#lstst').selectpicker('refresh');
                        $('#lsttsktype').val(item.TaskTypeID);
                        $('#lsttsktype').selectpicker('refresh');
                        $("#txtdesc").val(item.Description);
                        $("#txtrmk").val(item.Remark);
                        getimages(id);
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function getimages(taskmandoopid) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetMandoopImages',
                data: 'taskmandoopid=' + taskmandoopid,
                async: true,
                success: function (data) {
                    $('#tblimg').dataTable().fnClearTable();
                    $('#tblimg').dataTable().fnDestroy();

                    $.each(data, function (i, item) {
                        var trHTML = '';
                        var picture = '';
                        picture = '<img src="' + item.ImagePath + '" width="100" height="50" />';

                        trHTML += '<tr><td>' + picture + '</td></tr>';
                        $('#tblimg').append(trHTML);
                    });
                    $('#tblimg').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "bSort": false
                        //order: [[0, 'desc']]
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function AddWO() {
            debugger;
            var frmdate = $("#txtdt").val(); //$("#txtdt").val();
            var fd = ""
            if (frmdate == null || frmdate == "") {

            } else {
                frmdate = $("#txtdt").val().split('/');
                fd = frmdate[1] + '/' + frmdate[0] + '/' + frmdate[2];
            }
            if ($('#lsttskfr').val() == null || $('#lsttskfr').val() == "") {
                $.Notification.autoHideNotify('error', 'top right', 'Error', 'please select Task For');
            }
            else {
                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: 'AdminWeb.asmx/AddEditTaskMandoop',
                    data: 'id=0&typeid=' + $('#lsttsktype').val() + '&date=' + fd + '&custname=' + $("#txtcustmr").val() + '&desc=' + $("#txtdesc").val()
                        + '&rmk=' + $("#txtrmk").val() + '&tskstatus=' + $('#lstst').val() + '&taskfor=' + $('#lsttskfr').val(),
                    async: true,
                    success: function (data) {
                        if (data[0]["Result"] > 0) {
                            $.Notification.autoHideNotify('success', 'top right', 'Success', "Task has been saved successfully");
                            resetForm();
                            getSearchWorkOrder();
                            $('#modal-AddEdit').modal('toggle');
                        }
                        else {
                            $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
            }
        }

        function EditWO() {
            debugger;
            //var data2= 'id=0&typeid=' + $('#lsttsktype').val() + '&date=' + fd + '&custname=' + $("#txtcustmr").val() + '&desc=' + $("#txtdesc").val()
            //        + '&rmk=' + $("#txtrmk").val() + '&tskstatus=' + $('#lstst').val() + '&taskfor=' + $('#lsttskfr').val();
            //alert(data2);

            var frmdate = $("#txtdt").val(); //$("#txtdt").val();
            var fd = ""
            if (frmdate == null || frmdate == "") {

            } else {
                frmdate = $("#txtdt").val().split('/');
                fd = frmdate[1] + '/' + frmdate[0] + '/' + frmdate[2];
            }
            if ($('#lsttskfr').val() == null || $('#lsttskfr').val() == "") {
                $.Notification.autoHideNotify('error', 'top right', 'Error', 'please select Task For');
            }
            else {
                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: 'AdminWeb.asmx/AddEditTaskMandoop',
                    data: 'id=' + $("#hidEditID").val() + '&typeid=' + $('#lsttsktype').val() + '&date=' + fd + '&custname=' + $("#txtcustmr").val() + '&desc=' + $("#txtdesc").val()
                        + '&rmk=' + $("#txtrmk").val() + '&tskstatus=' + $('#lstst').val() + '&taskfor=' + $('#lsttskfr').val(),
                    async: true,
                    success: function (data) {
                        if (data[0]["Result"] > 0) {
                            $.Notification.autoHideNotify('success', 'top right', 'Success', "Task has been updated successfully");
                            resetForm();
                            getSearchWorkOrder();
                            $('#modal-AddEdit').modal('toggle');
                        }
                        else {
                            $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
            }
        }

        function confirmWODelete(id) {
            $("#hidDelID").val(id);
            jQuery('#modal-delete-wo').modal('show', { backdrop: 'static' });
        }

        function DeleteWO() {
            var id = $('input#hidDelID').val();

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/DeleteTaskMandoop',
                data: "id=" + id,
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('warning', 'top right', 'Deleted', 'Task Mandoop deleted successfully.');
                        getSearchWorkOrder();
                        $('#modal-delete-wo').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function bindtasktypes() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTaskTypes',
                async: true,
                success: function (data) {
                    $("#lsttsktype").append("").val("").html("");
                    //$("#lsttsktype").append($("<option></option>").val("").html("Select"));

                    $("#lstsrchtsktype").append("").val("").html("");
                    $("#lstsrchtsktype").append($("<option></option>").val("").html("Select"));

                    $.each(data, function (key, val) {
                        $("#lsttsktype").append($("<option></option>").val(val.ID).html(val.TypeName));

                        $("#lstsrchtsktype").append($("<option></option>").val(val.ID).html(val.TypeName));
                    });
                    $('#lsttsktype').selectpicker('refresh');

                    $('#lstsrchtsktype').selectpicker('refresh');
                }
            });
        }

        function bindTabletUsers() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTabletUsers',
                data: "user=0&tabletroleid=5",
                async: true,
                success: function (data) {
                    $("#lsttskfr").append("").val("").html("");
                    $("#lsttskfr").append($("<option></option>").val("").html("Select"));

                    $.each(data, function (key, val) {
                        $("#lsttskfr").append($("<option></option>").val(val.ID).html(val.Name));
                    });
                    $('#lsttskfr').selectpicker('refresh');
                }
            });
        }

        function bindTaskStatus() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTaskStatus',
                data: "id=0",
                async: true,
                success: function (data) {
                    $("#lstst").append("").val("").html("");
                    $("#lstsrchstatus").append("").val("").html("");
                    $("#lstsrchstatus").append($("<option></option>").val("0").html("Select"));

                    $.each(data, function (key, val) {
                        $("#lstst").append($("<option></option>").val(val.ID).html(val.StatusName));
                        $("#lstsrchstatus").append($("<option></option>").val(val.ID).html(val.StatusName));
                    });
                    $('#lstst').selectpicker('refresh');
                    $('#lstsrchstatus').selectpicker('refresh');
                }
            });
        }

    </script>

</asp:Content>









