﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DoorTypes.aspx.cs" Inherits="DoorTypes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Plugin Css-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

    <link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- Plugins css-->
    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Door Types</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li>
                                <a href="#">Settings</a>
                            </li>
                            <li class="active">Door Types
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="m-b-30">
                                    <button class="btn btn-primary waves-effect waves-light btnAdd" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="resetForm()">Add New</button>
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <table id="tablelist" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Door Type</th>
                                        <th>Serial Number</th>
                                        <th>Picture</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->


    </div>

    <!-- MODAL -->
    <div class="modal fade" id="modal-delete-doortype" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Confirm</h4>
                </div>

                <div class="modal-body">
                    Are you sure, you want to delete current record!
                </div>

                <div class="modal-footer">
                    <input type="hidden" id="hidDelID" name="deleteID" value="0" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info" id="btnDelete" onclick="DeleteDoorType()">Continue</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end Modal -->


    <!-- sample modal content -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" id="modal-AddEdit" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form name="frmAddEdit" id="frmAddEdit" method="post">
                    <input type="hidden" id="hidEditID" name="hidEditID" value="0" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="resetForm()">×</button>
                        <h4 class="modal-title" id="myModalLabel">Door Types Detail</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Name</label>
                                    <input type="text" class="form-control" id="txtname" maxlength="45" name="txtname" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Serial Number</label>
                                    <input type="text" class="form-control" id="txtserialno" maxlength="45" name="txtserialno" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Picture</label>
                                    <input type="file" id="filPicture" name="filPicture" class="filestyle" data-placeholder="No file" data-size="sm" data-max-option="1" accept="image/*" data-buttonname="btn-white" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal" onclick="resetForm()">Close</button>
                            <button class="btn btn-primary waves-effect waves-light" type="submit" id="btnaddedit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        var resizefunc = [];

        $(document).ready(function () {
            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#frmAddEdit").validate({
                rules: {
                    "txtname": "required",
                    "txtserialno": "required",

                },
                messages: {
                    "txtname": "Name  is required",
                    "txtserialno": "Serial No.  is required",
                },
                submitHandler: function (form) {
                    if ($("#hidEditID").val() > 0) {
                        EditDoorType();
                    }
                    else {
                        AddDoorType();
                    }
                    return false;
                }
            });
            getHistoryData();

        });

        function resetForm() {
            $("#hidEditID").val("0");
            $("#hidDelID").val("0");
            $("#txtname").val("");
            $("#txtserialno").val("");
            $("#filPicture").val("");

            $("#frmAddEdit").trigger('reset');
            var validator = $("#frmAddEdit").validate();
            validator.resetForm();
        }

        function getHistoryData() {

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetDoorTypes',
                data: 'id=0',
                async: true,
                success: function (data) {

                    $('#tablelist').dataTable().fnClearTable();
                    $('#tablelist').dataTable().fnDestroy();

                    $.each(data, function (i, item) {
                        var trHTML = '';
                        var picture = '';
                        picture = '<img src="' + item.Picture + '" width="100" height="50" />';
                        trHTML += '<tr><td>' + item.DoorTypeName + '</td><td>' + item.SerialNumber + '</td><td>' + picture + '</td><td>' +
                            '<button type="button" class="btn btn-info waves-effect btnEdit" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="FillDoorType(' + item.ID + ')"><i class="fa fa-pencil-square-o"></i></button><button class="btn btn-danger waves-effect waves-light btnDelete" onclick="confirmDoorTypeDelete(' + item.ID + ')"><i class="fa fa-times"></i></button>' + '</td></tr>';
                        $('#tablelist').append(trHTML);
                    });
                    $('#tablelist').dataTable({
                        "bPaginate": true,
                        "bLengthChange": true,
                        "bFilter": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "bSort": false
                        //order: [[0, 'desc']]
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function FillDoorType(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetDoorTypes',
                data: 'id=' + id,
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        $("#txtname").val(item.DoorTypeName);
                        $("#txtserialno").val(item.SerialNumber);
                        $("#hidEditID").val(item.ID);
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function AddDoorType() {
            //var id = $("#hidEditID").val();
            var picture = "";

            var dd = new FormData();
            var d = "id=" + $("#hidEditID").val();
            d += '&typename=' + $("#txtname").val();
            d += + '&serialno=' + $("#txtserialno").val();

            var files = $("#filPicture").get(0).files;
            if (files.length > 0) {
                dd.append("picture", files[0]);
            }

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditDoorType?' + d,
                data: dd,
                cache: false,
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "DoorType has been saved successfully");
                        resetForm();
                        getHistoryData();
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function EditDoorType() {
            var picture = "";

            var dd = new FormData();
            var d = "id=" + $("#hidEditID").val();
            d += '&typename=' + $("#txtname").val();
            d += '&serialno=' + $("#txtserialno").val();

            var files = $("#filPicture").get(0).files;
            if (files.length > 0) {
                dd.append("picture", files[0]);
            }

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditDoorType?' + d,
                data: dd,
                cache: false,
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "DoorType has been updated successfully");
                        resetForm();
                        getHistoryData();
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function confirmDoorTypeDelete(id) {
            $("#hidDelID").val(id);
            jQuery('#modal-delete-doortype').modal('show', { backdrop: 'static' });
        }

        function DeleteDoorType() {
            var id = $('input#hidDelID').val();

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/DeleteDoorType',
                data: "id=" + id,
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('warning', 'top right', 'Deleted', 'DoorType deleted successfully.');
                        getHistoryData();
                        $('#modal-delete-doortype').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

    </script>


</asp:Content>





