﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TabletRole.aspx.cs" Inherits="TabletRole" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Plugin Css-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

    <!-- Plugins css-->
    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <style>
        .permissionGrid {
            background-color: #EFEFEF;
            width: 100%;
        }

            .permissionGrid th {
                font-weight: bold;
            }

            .permissionGrid th, td {
                padding: 5px;
                text-align: center;
                border-left: solid 1px #FFF;
                border-bottom: solid 1px #FFF;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Tablet Roles</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li>
                                <a href="#">Tablet Users</a>
                            </li>
                            <li class="active">Tablet Roles
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">

                       <%-- <div class="row">
                            <div class="col-sm-6">
                                <div class="m-b-30">
                                    <button class="btn btn-primary waves-effect waves-light btnAdd" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="resetForm()">Add New</button>
                                </div>
                            </div>
                        </div>--%>

                        <div class="" style='overflow: scroll; overflow-y: hidden;'>
                            <table id="userroletable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->

    </div>

    <div class="modal fade" id="modal-delete-confirm" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Confirm</h4>
                </div>

                <div class="modal-body">
                    Are you sure, you want to delete current record!
                </div>

                <div class="modal-footer">
                    <input type="hidden" id="hidDeleteID" name="deleteID" value="0" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info" id="btnDelete" onclick="deleteData()">Continue</button>
                </div>
            </div>
        </div>
    </div>


    <!-- sample modal content -->
    <div class="modal fade bs-example-modal-lg" id="modal-AddEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="frmAddEdit" id="frmAddEdit" method="post">
                    <input type="hidden" id="hidEditID" name="editID" value="0" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Tablet Role Detail</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Name</label>
                                    <input type="text" class="form-control" id="txtname" name="name" placeholder="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal" onclick="resetForm()">Close</button>
                            <button class="btn btn-primary waves-effect waves-light" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            var resizefunc = [];

            $("#frmAddEdit").validate({
                rules: {
                    "name": "required",
                },
                messages: {
                    "name": "Role Name is required",
                },
                submitHandler: function (form) {
                    saveChanges();
                    return false;
                }
            });

            resetForm();
            getUserRoles();

        });

        function saveChanges() {
            return $.ajax({
                dataType: "json",
                type: "Post",
                url: 'AdminWeb.asmx/SaveTabletRoles',
                data: "id=" + $("#hidEditID").val() + "&name=" + $("#txtname").val(),
                //context: $("#table-list"),
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "Tablet Role has been saved successfully");
                        $('#modal-AddEdit').modal('toggle');
                        resetForm();
                        getUserRoles();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    /*alert("textStatus->" + textStatus);
                    alert("errorThrown->" + errorThrown);
                    if (textStatus == "timeout") {
                        alert("Request Timeout");
                    }*/
                }
            });
        }

        function getUserRoles() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTabletRoles',
                data: "role=0",
                async: true,
                success: function (data) {

                    $('#userroletable').dataTable().fnClearTable();
                    $('#userroletable').dataTable().fnDestroy();
                    $.each(data, function (i, item) {

                        var trHTML = '';

                        trHTML += '<tr><td>' + item.Name + '</td><td>' +
                            '<button type="button" class="btn btn-info waves-effect btnEdit" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="filluserroledata(' + item.ID + ')"><i class="fa fa-pencil-square-o"></i></button>' + '</td></tr>';
                        //<button class="btn btn-danger waves-effect waves-light btnDelete" onclick="confirmDelete(' + item.ID + ')"><i class="fa fa-times"></i></button>
                        $('#userroletable').append(trHTML);

                    });

                    $('#userroletable').dataTable({
                        "bPaginate": true,
                        "bLengthChange": true,
                        "bFilter": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "bSort": false
                        //order: [[0, 'desc']]
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function filluserroledata(id) {
            resetForm();
            $.ajax(
            {
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTabletRoles',
                data: "role=" + id,
                async: true,
                context: $("#table-list-data"),
                //beforeSend: function () {
                //    blockIt("table-list-data");
                //},
                //complete: function () {
                //    unblockIt("table-list-data");
                //},
                success: function (data) {
                    $("#hidEditID").val(data[0].ID);
                    $("#txtname").val(data[0].Name);

                    jQuery('#modal-AddEdit').modal('show', { backdrop: 'static' })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    /*alert("textStatus->" + textStatus);
                    alert("errorThrown->" + errorThrown);
                    if (textStatus == "timeout") {
                        alert("Request Timeout");
                    }*/
                }
            });
        }

        function resetForm() {
            $("#hidEditID").val("0");
            $("#hidDeleteID").val("0");
            $("#txtname").val("");

            var validator = $("#frmAddEdit").validate();
            validator.resetForm();
        }

        function confirmDelete(id) {
            $("#hidDeleteID").val(id);
            jQuery('#modal-delete-confirm').modal('show', { backdrop: 'static' });
        }

        function deleteData() {
            if ($("#hidDeleteID").val() != "0") {
                return $.ajax(
                {
                    dataType: "json",
                    type: "Post",
                    data: "role=" + $("#hidDeleteID").val(),
                    url: 'AdminWeb.asmx/DeleteTabletRoles',
                    context: $("#lstArea"),
                    //beforeSend: function () {
                    //    blockIt("table-list");
                    //},
                    //complete: function () {
                    //    unblockIt("table-list");
                    //},
                    success: function (data) {
                        if (data[0]["Result"] == "-1") {
                            alert("Role cannot be deleted");
                        }
                        else if (data[0]["Result"] > 0) {
                            $.Notification.autoHideNotify('warning', 'top right', 'Deleted', 'Tablet Role has deleted successfully.');
                            $('#modal-delete-confirm').modal('toggle');
                            resetForm();
                            getUserRoles();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        /*alert("textStatus->" + textStatus);
                        alert("errorThrown->" + errorThrown);
                        if (textStatus == "timeout") {
                            alert("Request Timeout");
                        }*/
                    }
                });
            }
        }

    </script>


</asp:Content>




