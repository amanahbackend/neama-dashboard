﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DataAccess
/// </summary>
public class DataAccess
{
    static string connString;
    static DataAccess()
    {
        //
        // TODO: Add constructor logic here
        //

        connString = ConfigurationManager.ConnectionStrings["neama_dbEntities"].ConnectionString;
    }
    public static DataTable GetDataTableFromStoredProcedure(string ProcedureName, SqlParameter[] SqlParameters)
    {
        SqlConnection conn = new SqlConnection(connString);
        using (SqlConnection con = new SqlConnection(connString))
        {
            using (SqlCommand cmd = new SqlCommand(ProcedureName, con))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter Parameter in SqlParameters)
                {
                    if (Parameter.SqlDbType == SqlDbType.Bit)
                    {
                        if (Parameter.Value.ToString() == "True")
                            Parameter.Value = 1;
                        else if (Parameter.Value.ToString() == "False")
                            Parameter.Value = 0;
                    }
                    cmd.Parameters.Add(Parameter);
                }

                DataTable dt = new DataTable();
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.Fill(dt);
                }
                return dt;
            }
        }
    }

    public static string GetJsonFromDataTable(DataTable dt)
    {
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row = null;

        foreach (DataRow dr in dt.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in dt.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }
        serializer.MaxJsonLength = 2147483647;
        string s = serializer.Serialize(rows);
        return s;
        //return new System.Web.Script.Serialization.JavaScriptSerializer().DeserializeObject(s.Replace(@"\", @"\\"));
    }
}