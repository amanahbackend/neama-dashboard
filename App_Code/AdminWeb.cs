﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for AdminWeb
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class AdminWeb : System.Web.Services.WebService
{
    string domainurl = System.Configuration.ConfigurationManager.AppSettings["domainurl"].ToString();

    //public AdminWeb()
    //{

    //    //Uncomment the following line if using designed components 
    //    //InitializeComponent(); 
    //}

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }


    #region DoorTypes


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDoorTypes(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_DoorTypes", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditDoorType()
    {

        int id = Convert.ToInt32(HttpContext.Current.Request["id"]);
        string typename = HttpContext.Current.Request["typename"];
        string serialno = HttpContext.Current.Request["serialno"];

        string picture = "", filepath = "";
        if (HttpContext.Current.Request.Files.AllKeys.Any())
        {
            // Get the uploaded image from the Files collection
            var httpPostedFile = HttpContext.Current.Request.Files["picture"];
            if (httpPostedFile != null)
            {
                picture = GetUniqueAlphaNumericKey(15) + ".png";
                // Get the complete file path
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("content/doortypes"), picture);
                // Save the uploaded file to "UploadedFiles" folder
                httpPostedFile.SaveAs(fileSavePath);
                filepath = domainurl + "/content/doortypes/" + picture;
            }
        }


        SqlParameter[] Parameters = new SqlParameter[4];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "TypeName";
        Parameters[1].Value = typename;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "serialnumber";
        Parameters[2].Value = serialno;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "picture";
        Parameters[3].Value = filepath;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_DoorType", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteDoorType(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_DoorType", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    #region Work Order

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetWOs(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_WOs", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditWO(int id, string quantity, string serialno, string width, string height, string color, string wallwidth
        , int doortypeid, int iscollection, int isfoom, int iscuttingforglass, int iscleaning, int isglassfitting, int ispainting
        , int isframeinside, int isframeoutside, int isdoor, int isrepair, int issales, string notes, int supervisorid, string hours, int iscompleted)
    {
        SqlParameter[] Parameters = new SqlParameter[24];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "serialnumber";
        Parameters[2].Value = serialno;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "Width";
        Parameters[3].Value = width;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "Height";
        Parameters[4].Value = height;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "Color";
        Parameters[5].Value = color;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.NVarChar;
        Parameters[6].ParameterName = "WallWidth";
        Parameters[6].Value = wallwidth;

        Parameters[7] = new SqlParameter();
        Parameters[7].SqlDbType = SqlDbType.Int;
        Parameters[7].ParameterName = "DoorTypeID";
        Parameters[7].Value = doortypeid;

        Parameters[8] = new SqlParameter();
        Parameters[8].SqlDbType = SqlDbType.Int;
        Parameters[8].ParameterName = "IsCollection";
        Parameters[8].Value = iscollection;

        Parameters[9] = new SqlParameter();
        Parameters[9].SqlDbType = SqlDbType.Int;
        Parameters[9].ParameterName = "IsFoom";
        Parameters[9].Value = isfoom;

        Parameters[10] = new SqlParameter();
        Parameters[10].SqlDbType = SqlDbType.Int;
        Parameters[10].ParameterName = "IsCuttingForGlass";
        Parameters[10].Value = iscuttingforglass;

        Parameters[11] = new SqlParameter();
        Parameters[11].SqlDbType = SqlDbType.Int;
        Parameters[11].ParameterName = "IsCleaning";
        Parameters[11].Value = iscleaning;

        Parameters[12] = new SqlParameter();
        Parameters[12].SqlDbType = SqlDbType.Int;
        Parameters[12].ParameterName = "IsGlassFitting";
        Parameters[12].Value = isglassfitting;

        Parameters[13] = new SqlParameter();
        Parameters[13].SqlDbType = SqlDbType.Int;
        Parameters[13].ParameterName = "IsPainting";
        Parameters[13].Value = ispainting;

        Parameters[14] = new SqlParameter();
        Parameters[14].SqlDbType = SqlDbType.Int;
        Parameters[14].ParameterName = "IsFrameInside";
        Parameters[14].Value = isframeinside;

        Parameters[15] = new SqlParameter();
        Parameters[15].SqlDbType = SqlDbType.Int;
        Parameters[15].ParameterName = "IsFrameOutside";
        Parameters[15].Value = isframeoutside;

        Parameters[16] = new SqlParameter();
        Parameters[16].SqlDbType = SqlDbType.Int;
        Parameters[16].ParameterName = "IsDoor";
        Parameters[16].Value = isdoor;

        Parameters[17] = new SqlParameter();
        Parameters[17].SqlDbType = SqlDbType.Int;
        Parameters[17].ParameterName = "IsRepair";
        Parameters[17].Value = isrepair;

        Parameters[18] = new SqlParameter();
        Parameters[18].SqlDbType = SqlDbType.Int;
        Parameters[18].ParameterName = "IsSales";
        Parameters[18].Value = issales;

        Parameters[19] = new SqlParameter();
        Parameters[19].SqlDbType = SqlDbType.NVarChar;
        Parameters[19].ParameterName = "Notes";
        Parameters[19].Value = notes;

        Parameters[20] = new SqlParameter();
        Parameters[20].SqlDbType = SqlDbType.Int;
        Parameters[20].ParameterName = "supervisorid";
        Parameters[20].Value = supervisorid;

        Parameters[21] = new SqlParameter();
        Parameters[21].SqlDbType = SqlDbType.NVarChar;
        Parameters[21].ParameterName = "Hours";
        Parameters[21].Value = hours;

        Parameters[22] = new SqlParameter();
        Parameters[22].SqlDbType = SqlDbType.NVarChar;
        Parameters[22].ParameterName = "empname";
        Parameters[22].Value = Session["Admin_Name"];

        Parameters[23] = new SqlParameter();
        Parameters[23].SqlDbType = SqlDbType.Int;
        Parameters[23].ParameterName = "isCompleted";
        Parameters[23].Value = iscompleted;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_WO", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteWO(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "empname";
        Parameters[1].Value = Session["Admin_Name"];

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_WO", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchWOs(int id, string serialno, int doortypeid, string frmdate, string todate)
    {
        SqlParameter[] Parameters = new SqlParameter[5];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "serialnumber";
        Parameters[1].Value = serialno;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "DoorTypeID";
        Parameters[2].Value = doortypeid;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "FROMDATE";
        if (frmdate == "")
        {
            Parameters[3].Value = "1/1/1900";
        }
        else
        {
            Parameters[3].Value = frmdate;
        }

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "TODATE";
        if (todate == "")
        {
            Parameters[4].Value = "1/1/1900";
        }
        else
        {
            Parameters[4].Value = todate;
        }

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Search_WOs", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    #region Work Order Details

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetWODoorDetails(int id, int woid)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "WOID";
        Parameters[1].Value = woid;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_WODetails", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditWODetail(int id, string quantity, string serialno, string width, string height, string color, string wallwidth
        , int doortypeid, int iscollection, int isfoom, int iscuttingforglass, int iscleaning, int isglassfitting, int ispainting
        , int isframeinside, int isframeoutside, int isdoor, int isrepair, int issales, string notes, int supervisorid, string hours, int woid, int iscompleted)
    {
        SqlParameter[] Parameters = new SqlParameter[25];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "serialnumber";
        Parameters[2].Value = serialno;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "Width";
        Parameters[3].Value = width;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "Height";
        Parameters[4].Value = height;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "Color";
        Parameters[5].Value = color;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.NVarChar;
        Parameters[6].ParameterName = "WallWidth";
        Parameters[6].Value = wallwidth;

        Parameters[7] = new SqlParameter();
        Parameters[7].SqlDbType = SqlDbType.Int;
        Parameters[7].ParameterName = "DoorTypeID";
        Parameters[7].Value = doortypeid;

        Parameters[8] = new SqlParameter();
        Parameters[8].SqlDbType = SqlDbType.Int;
        Parameters[8].ParameterName = "IsCollection";
        Parameters[8].Value = iscollection;

        Parameters[9] = new SqlParameter();
        Parameters[9].SqlDbType = SqlDbType.Int;
        Parameters[9].ParameterName = "IsFoom";
        Parameters[9].Value = isfoom;

        Parameters[10] = new SqlParameter();
        Parameters[10].SqlDbType = SqlDbType.Int;
        Parameters[10].ParameterName = "IsCuttingForGlass";
        Parameters[10].Value = iscuttingforglass;

        Parameters[11] = new SqlParameter();
        Parameters[11].SqlDbType = SqlDbType.Int;
        Parameters[11].ParameterName = "IsCleaning";
        Parameters[11].Value = iscleaning;

        Parameters[12] = new SqlParameter();
        Parameters[12].SqlDbType = SqlDbType.Int;
        Parameters[12].ParameterName = "IsGlassFitting";
        Parameters[12].Value = isglassfitting;

        Parameters[13] = new SqlParameter();
        Parameters[13].SqlDbType = SqlDbType.Int;
        Parameters[13].ParameterName = "IsPainting";
        Parameters[13].Value = ispainting;

        Parameters[14] = new SqlParameter();
        Parameters[14].SqlDbType = SqlDbType.Int;
        Parameters[14].ParameterName = "IsFrameInside";
        Parameters[14].Value = isframeinside;

        Parameters[15] = new SqlParameter();
        Parameters[15].SqlDbType = SqlDbType.Int;
        Parameters[15].ParameterName = "IsFrameOutside";
        Parameters[15].Value = isframeoutside;

        Parameters[16] = new SqlParameter();
        Parameters[16].SqlDbType = SqlDbType.Int;
        Parameters[16].ParameterName = "IsDoor";
        Parameters[16].Value = isdoor;

        Parameters[17] = new SqlParameter();
        Parameters[17].SqlDbType = SqlDbType.Int;
        Parameters[17].ParameterName = "IsRepair";
        Parameters[17].Value = isrepair;

        Parameters[18] = new SqlParameter();
        Parameters[18].SqlDbType = SqlDbType.Int;
        Parameters[18].ParameterName = "IsSales";
        Parameters[18].Value = issales;

        Parameters[19] = new SqlParameter();
        Parameters[19].SqlDbType = SqlDbType.NVarChar;
        Parameters[19].ParameterName = "Notes";
        Parameters[19].Value = notes;

        Parameters[20] = new SqlParameter();
        Parameters[20].SqlDbType = SqlDbType.Int;
        Parameters[20].ParameterName = "supervisorid";
        Parameters[20].Value = supervisorid;

        Parameters[21] = new SqlParameter();
        Parameters[21].SqlDbType = SqlDbType.NVarChar;
        Parameters[21].ParameterName = "Hours";
        Parameters[21].Value = hours;

        Parameters[22] = new SqlParameter();
        Parameters[22].SqlDbType = SqlDbType.Int;
        Parameters[22].ParameterName = "WOID";
        Parameters[22].Value = woid;

        Parameters[23] = new SqlParameter();
        Parameters[23].SqlDbType = SqlDbType.NVarChar;
        Parameters[23].ParameterName = "empname";
        Parameters[23].Value = Session["Admin_Name"];

        Parameters[24] = new SqlParameter();
        Parameters[24].SqlDbType = SqlDbType.Int;
        Parameters[24].ParameterName = "isCompleted";
        Parameters[24].Value = iscompleted;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_WO_Detail", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteWODetail(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    #region Tank Types

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetProcessStation(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_ProcessStation", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTankTypes(int id, string productname)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "PorductName";
        Parameters[1].Value = productname;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TankTypes", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditTankType()
    {
        int id = Convert.ToInt32(HttpContext.Current.Request["id"]);
        int processstaionid = Convert.ToInt32(HttpContext.Current.Request["processstaionid"]);
        string machine = HttpContext.Current.Request["machine"];
        string productname = HttpContext.Current.Request["productname"];
        string gallon = HttpContext.Current.Request["gallon"];

        string picture = "", filepath = "";
        if (HttpContext.Current.Request.Files.AllKeys.Any())
        {
            // Get the uploaded image from the Files collection
            var httpPostedFile = HttpContext.Current.Request.Files["picture"];
            if (httpPostedFile != null)
            {
                picture = GetUniqueAlphaNumericKey(15) + ".png";
                // Get the complete file path
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("content/tanktypes"), picture);
                // Save the uploaded file to "UploadedFiles" folder
                httpPostedFile.SaveAs(fileSavePath);
                filepath = domainurl + "/content/tanktypes/" + picture;
            }
        }

        SqlParameter[] Parameters = new SqlParameter[6];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "ProcessStationID";
        Parameters[1].Value = processstaionid;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "MachineNumber";
        Parameters[2].Value = machine;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "ProductName";
        Parameters[3].Value = productname;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "Gallon";
        Parameters[4].Value = gallon;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "picture";
        Parameters[5].Value = filepath;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_TankType", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteTankType(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_TankType", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }


    #endregion

    #region WO Tanks

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchWOTanks(int id, string productname, string frmdate, string todate)
    {
        SqlParameter[] Parameters = new SqlParameter[4];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "ProductName";
        Parameters[1].Value = productname;


        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "FROMDATE";
        if (frmdate == "")
        {
            Parameters[2].Value = "1/1/1900";
        }
        else
        {
            Parameters[2].Value = frmdate;
        }

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "TODATE";
        if (todate == "")
        {
            Parameters[3].Value = "1/1/1900";
        }
        else
        {
            Parameters[3].Value = todate;
        }

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Search_WOTanks", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetWOTanks(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;


        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_WO_Tanks", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditWOTanks(int id, string quantity, int processstaionID, string productname, string machinenumber
        , string gallons, string layers, int isheatbarrel, int isstartmach, int isdoprocess, int isoutput, int isdeliveryhouse
        , int isdeliveryground, int isdeliveryfactory, int isexport, string notes, int isrepair, int issales, int supervisorid, string hours, int tanktypeid, int iscompleted)
    {
        SqlParameter[] Parameters = new SqlParameter[23];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "ProcessStationID";
        Parameters[2].Value = processstaionID;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "ProdcutName";
        Parameters[3].Value = productname;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "MachineNumber";
        Parameters[4].Value = machinenumber;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "Gallons";
        Parameters[5].Value = gallons;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.NVarChar;
        Parameters[6].ParameterName = "Layers";
        Parameters[6].Value = layers;

        Parameters[7] = new SqlParameter();
        Parameters[7].SqlDbType = SqlDbType.Int;
        Parameters[7].ParameterName = "IsHeatBarrel";
        Parameters[7].Value = isheatbarrel;

        Parameters[8] = new SqlParameter();
        Parameters[8].SqlDbType = SqlDbType.Int;
        Parameters[8].ParameterName = "IsStartMach";
        Parameters[8].Value = isstartmach;

        Parameters[9] = new SqlParameter();
        Parameters[9].SqlDbType = SqlDbType.Int;
        Parameters[9].ParameterName = "IsDoProcess";
        Parameters[9].Value = isdoprocess;

        Parameters[10] = new SqlParameter();
        Parameters[10].SqlDbType = SqlDbType.Int;
        Parameters[10].ParameterName = "IsOutput";
        Parameters[10].Value = isoutput;

        Parameters[11] = new SqlParameter();
        Parameters[11].SqlDbType = SqlDbType.Int;
        Parameters[11].ParameterName = "IsDeliveryHouse";
        Parameters[11].Value = isdeliveryhouse;

        Parameters[12] = new SqlParameter();
        Parameters[12].SqlDbType = SqlDbType.Int;
        Parameters[12].ParameterName = "IsDeliveryGround";
        Parameters[12].Value = isdeliveryground;

        Parameters[13] = new SqlParameter();
        Parameters[13].SqlDbType = SqlDbType.Int;
        Parameters[13].ParameterName = "IsDeliveryFactory";
        Parameters[13].Value = isdeliveryfactory;

        Parameters[14] = new SqlParameter();
        Parameters[14].SqlDbType = SqlDbType.NVarChar;
        Parameters[14].ParameterName = "Notes";
        Parameters[14].Value = notes;

        Parameters[15] = new SqlParameter();
        Parameters[15].SqlDbType = SqlDbType.Int;
        Parameters[15].ParameterName = "IsExport";
        Parameters[15].Value = isexport;

        Parameters[16] = new SqlParameter();
        Parameters[16].SqlDbType = SqlDbType.Int;
        Parameters[16].ParameterName = "IsRepair";
        Parameters[16].Value = isrepair;

        Parameters[17] = new SqlParameter();
        Parameters[17].SqlDbType = SqlDbType.Int;
        Parameters[17].ParameterName = "IsSales";
        Parameters[17].Value = issales;

        Parameters[18] = new SqlParameter();
        Parameters[18].SqlDbType = SqlDbType.Int;
        Parameters[18].ParameterName = "supervisorid";
        Parameters[18].Value = supervisorid;

        Parameters[19] = new SqlParameter();
        Parameters[19].SqlDbType = SqlDbType.NVarChar;
        Parameters[19].ParameterName = "Hours";
        Parameters[19].Value = hours;

        Parameters[20] = new SqlParameter();
        Parameters[20].SqlDbType = SqlDbType.NVarChar;
        Parameters[20].ParameterName = "TankTypeID";
        Parameters[20].Value = tanktypeid;

        Parameters[21] = new SqlParameter();
        Parameters[21].SqlDbType = SqlDbType.NVarChar;
        Parameters[21].ParameterName = "empname";
        Parameters[21].Value = Session["Admin_Name"];

        Parameters[22] = new SqlParameter();
        Parameters[22].SqlDbType = SqlDbType.Int;
        Parameters[22].ParameterName = "isCompleted";
        Parameters[22].Value = iscompleted;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_WO_Tanks", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteWOTanks(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "empname";
        Parameters[1].Value = Session["Admin_Name"];

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_WO_Tanks", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    #region WO Tanks Details

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetWOTankDetail(int id, int woid)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "WOID";
        Parameters[1].Value = woid;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_WO_Tanks_Detail", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditWOTanksDetail(int id, string quantity, int processstaionID, string productname, string machinenumber
       , string gallons, string layers, int isheatbarrel, int isstartmach, int isdoprocess, int isoutput, int isdeliveryhouse
       , int isdeliveryground, int isdeliveryfactory, int isexport, string notes, int isrepair, int issales, int supervisorid, string hours, int tanktypeid,
        int woid, int iscompleted)
    {
        SqlParameter[] Parameters = new SqlParameter[24];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "ProcessStationID";
        Parameters[2].Value = processstaionID;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "ProdcutName";
        Parameters[3].Value = productname;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "MachineNumber";
        Parameters[4].Value = machinenumber;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "Gallons";
        Parameters[5].Value = gallons;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.NVarChar;
        Parameters[6].ParameterName = "Layers";
        Parameters[6].Value = layers;

        Parameters[7] = new SqlParameter();
        Parameters[7].SqlDbType = SqlDbType.Int;
        Parameters[7].ParameterName = "IsHeatBarrel";
        Parameters[7].Value = isheatbarrel;

        Parameters[8] = new SqlParameter();
        Parameters[8].SqlDbType = SqlDbType.Int;
        Parameters[8].ParameterName = "IsStartMach";
        Parameters[8].Value = isstartmach;

        Parameters[9] = new SqlParameter();
        Parameters[9].SqlDbType = SqlDbType.Int;
        Parameters[9].ParameterName = "IsDoProcess";
        Parameters[9].Value = isdoprocess;

        Parameters[10] = new SqlParameter();
        Parameters[10].SqlDbType = SqlDbType.Int;
        Parameters[10].ParameterName = "IsOutput";
        Parameters[10].Value = isoutput;

        Parameters[11] = new SqlParameter();
        Parameters[11].SqlDbType = SqlDbType.Int;
        Parameters[11].ParameterName = "IsDeliveryHouse";
        Parameters[11].Value = isdeliveryhouse;

        Parameters[12] = new SqlParameter();
        Parameters[12].SqlDbType = SqlDbType.Int;
        Parameters[12].ParameterName = "IsDeliveryGround";
        Parameters[12].Value = isdeliveryground;

        Parameters[13] = new SqlParameter();
        Parameters[13].SqlDbType = SqlDbType.Int;
        Parameters[13].ParameterName = "IsDeliveryFactory";
        Parameters[13].Value = isdeliveryfactory;

        Parameters[14] = new SqlParameter();
        Parameters[14].SqlDbType = SqlDbType.NVarChar;
        Parameters[14].ParameterName = "Notes";
        Parameters[14].Value = notes;

        Parameters[15] = new SqlParameter();
        Parameters[15].SqlDbType = SqlDbType.Int;
        Parameters[15].ParameterName = "IsExport";
        Parameters[15].Value = isexport;

        Parameters[16] = new SqlParameter();
        Parameters[16].SqlDbType = SqlDbType.Int;
        Parameters[16].ParameterName = "IsRepair";
        Parameters[16].Value = isrepair;

        Parameters[17] = new SqlParameter();
        Parameters[17].SqlDbType = SqlDbType.Int;
        Parameters[17].ParameterName = "IsSales";
        Parameters[17].Value = issales;

        Parameters[18] = new SqlParameter();
        Parameters[18].SqlDbType = SqlDbType.Int;
        Parameters[18].ParameterName = "supervisorid";
        Parameters[18].Value = supervisorid;

        Parameters[19] = new SqlParameter();
        Parameters[19].SqlDbType = SqlDbType.NVarChar;
        Parameters[19].ParameterName = "Hours";
        Parameters[19].Value = hours;

        Parameters[20] = new SqlParameter();
        Parameters[20].SqlDbType = SqlDbType.NVarChar;
        Parameters[20].ParameterName = "TankTypeID";
        Parameters[20].Value = tanktypeid;

        Parameters[21] = new SqlParameter();
        Parameters[21].SqlDbType = SqlDbType.NVarChar;
        Parameters[21].ParameterName = "WOID";
        Parameters[21].Value = woid;

        Parameters[22] = new SqlParameter();
        Parameters[22].SqlDbType = SqlDbType.Int;
        Parameters[22].ParameterName = "isCompleted";
        Parameters[22].Value = iscompleted;

        Parameters[23] = new SqlParameter();
        Parameters[23].SqlDbType = SqlDbType.NVarChar;
        Parameters[23].ParameterName = "empname";
        Parameters[23].Value = Session["Admin_Name"];

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_WO_Tanks_Detail", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    #region GRPSection Types


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetGRPSectionTypes(int id, string productname)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "PorductName";
        Parameters[1].Value = productname;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_GRPSectionTypes", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditGRPSectionType()
    {

        int id = Convert.ToInt32(HttpContext.Current.Request["id"]);
        int processstaionid = Convert.ToInt32(HttpContext.Current.Request["processstaionid"]);
        string machine = HttpContext.Current.Request["machine"];
        string productname = HttpContext.Current.Request["productname"];

        string picture = "", filepath = "";
        if (HttpContext.Current.Request.Files.AllKeys.Any())
        {
            // Get the uploaded image from the Files collection
            var httpPostedFile = HttpContext.Current.Request.Files["picture"];
            if (httpPostedFile != null)
            {
                picture = GetUniqueAlphaNumericKey(15) + ".png";

                // Get the complete file path
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("content/grpsectiontypes"), picture);
                // Save the uploaded file to "UploadedFiles" folder
                httpPostedFile.SaveAs(fileSavePath);
                filepath = domainurl + "/content/grpsectiontypes/" + picture;
            }
        }
        SqlParameter[] Parameters = new SqlParameter[5];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "ProcessStationID";
        Parameters[1].Value = processstaionid;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "MachineNumber";
        Parameters[2].Value = machine;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "ProductName";
        Parameters[3].Value = productname;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "picture";
        Parameters[4].Value = filepath;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_GRPSectionType", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteGRPSectionType(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_GRPSectionType", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }


    #endregion

    #region WO GRPSection

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchWOGRPSections(int id, string productname, string frmdate, string todate)
    {
        SqlParameter[] Parameters = new SqlParameter[4];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "ProductName";
        Parameters[1].Value = productname;


        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "FROMDATE";
        if (frmdate == "")
        {
            Parameters[2].Value = "1/1/1900";
        }
        else
        {
            Parameters[2].Value = frmdate;
        }

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "TODATE";
        if (todate == "")
        {
            Parameters[3].Value = "1/1/1900";
        }
        else
        {
            Parameters[3].Value = todate;
        }

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Search_WOGRPSections", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetWOGRPSections(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;


        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_WO_GRPSections", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditWOGRPSection(int id, string quantity, int processstaionID, string productname, string machinenumber, int ismixingoflmp, int ismixingofwhitepigment, int isstartcompound
        , int isstartsheet, int ischeckoil, int isstartcirculation, int ischeckleakage, int isstartboiler, int iswaittemp, int isstartmachine
        , int isdeliveryfactory, int isdeliverysite, int issteel, int iscompleteassembleonsite,
        int isdeliveryaccessories, string notes, int isrepair, int issales, int supervisorid, string hours, string grptypeid, int iscompleted)
    {
        SqlParameter[] Parameters = new SqlParameter[28];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "ProcessStationID";
        Parameters[2].Value = processstaionID;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "ProdcutName";
        Parameters[3].Value = productname;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "MachineNumber";
        Parameters[4].Value = machinenumber;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.Int;
        Parameters[5].ParameterName = "IsMixingofLMPs";
        Parameters[5].Value = ismixingoflmp;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.Int;
        Parameters[6].ParameterName = "IsMixingofWhitePigment";
        Parameters[6].Value = ismixingofwhitepigment;

        Parameters[7] = new SqlParameter();
        Parameters[7].SqlDbType = SqlDbType.Int;
        Parameters[7].ParameterName = "IsStartCompound";
        Parameters[7].Value = isstartcompound;

        Parameters[8] = new SqlParameter();
        Parameters[8].SqlDbType = SqlDbType.Int;
        Parameters[8].ParameterName = "IsStartSheet";
        Parameters[8].Value = isstartsheet;

        Parameters[9] = new SqlParameter();
        Parameters[9].SqlDbType = SqlDbType.Int;
        Parameters[9].ParameterName = "IsCheckOil";
        Parameters[9].Value = ischeckoil;

        Parameters[10] = new SqlParameter();
        Parameters[10].SqlDbType = SqlDbType.Int;
        Parameters[10].ParameterName = "IsStartCiculation";
        Parameters[10].Value = isstartcirculation;

        Parameters[11] = new SqlParameter();
        Parameters[11].SqlDbType = SqlDbType.Int;
        Parameters[11].ParameterName = "IsCheckLeakage";
        Parameters[11].Value = ischeckleakage;

        Parameters[12] = new SqlParameter();
        Parameters[12].SqlDbType = SqlDbType.Int;
        Parameters[12].ParameterName = "IsStartboiler";
        Parameters[12].Value = isstartboiler;

        Parameters[13] = new SqlParameter();
        Parameters[13].SqlDbType = SqlDbType.Int;
        Parameters[13].ParameterName = "IsWaitTemperature";
        Parameters[13].Value = iswaittemp;

        Parameters[14] = new SqlParameter();
        Parameters[14].SqlDbType = SqlDbType.Int;
        Parameters[14].ParameterName = "IsStartMachine";
        Parameters[14].Value = isstartmachine;

        Parameters[15] = new SqlParameter();
        Parameters[15].SqlDbType = SqlDbType.NVarChar;
        Parameters[15].ParameterName = "Notes";
        Parameters[15].Value = notes;

        Parameters[16] = new SqlParameter();
        Parameters[16].SqlDbType = SqlDbType.Int;
        Parameters[16].ParameterName = "IsDeliveryFatory";
        Parameters[16].Value = isdeliveryfactory;

        Parameters[17] = new SqlParameter();
        Parameters[17].SqlDbType = SqlDbType.Int;
        Parameters[17].ParameterName = "IsDeliverySite";
        Parameters[17].Value = isdeliverysite;

        Parameters[18] = new SqlParameter();
        Parameters[18].SqlDbType = SqlDbType.Int;
        Parameters[18].ParameterName = "IsSteel";
        Parameters[18].Value = issteel;

        Parameters[19] = new SqlParameter();
        Parameters[19].SqlDbType = SqlDbType.Int;
        Parameters[19].ParameterName = "IsCompleteAssembleonSite";
        Parameters[19].Value = iscompleteassembleonsite;

        Parameters[20] = new SqlParameter();
        Parameters[20].SqlDbType = SqlDbType.Int;
        Parameters[20].ParameterName = "IsDeliveryAccessories";
        Parameters[20].Value = isdeliveryaccessories;

        Parameters[21] = new SqlParameter();
        Parameters[21].SqlDbType = SqlDbType.Int;
        Parameters[21].ParameterName = "IsRepair";
        Parameters[21].Value = isrepair;

        Parameters[22] = new SqlParameter();
        Parameters[22].SqlDbType = SqlDbType.Int;
        Parameters[22].ParameterName = "IsSales";
        Parameters[22].Value = issales;

        Parameters[23] = new SqlParameter();
        Parameters[23].SqlDbType = SqlDbType.Int;
        Parameters[23].ParameterName = "supervisorid";
        Parameters[23].Value = supervisorid;

        Parameters[24] = new SqlParameter();
        Parameters[24].SqlDbType = SqlDbType.NVarChar;
        Parameters[24].ParameterName = "Hours";
        Parameters[24].Value = hours;

        Parameters[25] = new SqlParameter();
        Parameters[25].SqlDbType = SqlDbType.NVarChar;
        Parameters[25].ParameterName = "GRPTypeid";
        Parameters[25].Value = grptypeid;

        Parameters[26] = new SqlParameter();
        Parameters[26].SqlDbType = SqlDbType.NVarChar;
        Parameters[26].ParameterName = "empname";
        Parameters[26].Value = Session["Admin_Name"];

        Parameters[27] = new SqlParameter();
        Parameters[27].SqlDbType = SqlDbType.Int;
        Parameters[27].ParameterName = "isCompleted";
        Parameters[27].Value = iscompleted;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_WO_GRPSection", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteWOGRPSection(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "empname";
        Parameters[1].Value = Session["Admin_Name"];

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_WO_GRPSections", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    #region WO GRPSection Detail

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetWOGRPSectionDetail(int id, int woid)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "WOID";
        Parameters[1].Value = woid;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_WO_GRPSections_Detail", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditWOGRPSectionDetail(int id, string quantity, int processstaionID, string productname, string machinenumber, int ismixingoflmp, int ismixingofwhitepigment, int isstartcompound
        , int isstartsheet, int ischeckoil, int isstartcirculation, int ischeckleakage, int isstartboiler, int iswaittemp, int isstartmachine
        , int isdeliveryfactory, int isdeliverysite, int issteel, int iscompleteassembleonsite,
        int isdeliveryaccessories, string notes, int isrepair, int issales, int supervisorid, string hours, string grptypeid, int woid, string iscompleted)
    {
        SqlParameter[] Parameters = new SqlParameter[29];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "ProcessStationID";
        Parameters[2].Value = processstaionID;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "ProdcutName";
        Parameters[3].Value = productname;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "MachineNumber";
        Parameters[4].Value = machinenumber;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.Int;
        Parameters[5].ParameterName = "IsMixingofLMPs";
        Parameters[5].Value = ismixingoflmp;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.Int;
        Parameters[6].ParameterName = "IsMixingofWhitePigment";
        Parameters[6].Value = ismixingofwhitepigment;

        Parameters[7] = new SqlParameter();
        Parameters[7].SqlDbType = SqlDbType.Int;
        Parameters[7].ParameterName = "IsStartCompound";
        Parameters[7].Value = isstartcompound;

        Parameters[8] = new SqlParameter();
        Parameters[8].SqlDbType = SqlDbType.Int;
        Parameters[8].ParameterName = "IsStartSheet";
        Parameters[8].Value = isstartsheet;

        Parameters[9] = new SqlParameter();
        Parameters[9].SqlDbType = SqlDbType.Int;
        Parameters[9].ParameterName = "IsCheckOil";
        Parameters[9].Value = ischeckoil;

        Parameters[10] = new SqlParameter();
        Parameters[10].SqlDbType = SqlDbType.Int;
        Parameters[10].ParameterName = "IsStartCiculation";
        Parameters[10].Value = isstartcirculation;

        Parameters[11] = new SqlParameter();
        Parameters[11].SqlDbType = SqlDbType.Int;
        Parameters[11].ParameterName = "IsCheckLeakage";
        Parameters[11].Value = ischeckleakage;

        Parameters[12] = new SqlParameter();
        Parameters[12].SqlDbType = SqlDbType.Int;
        Parameters[12].ParameterName = "IsStartboiler";
        Parameters[12].Value = isstartboiler;

        Parameters[13] = new SqlParameter();
        Parameters[13].SqlDbType = SqlDbType.Int;
        Parameters[13].ParameterName = "IsWaitTemperature";
        Parameters[13].Value = iswaittemp;

        Parameters[14] = new SqlParameter();
        Parameters[14].SqlDbType = SqlDbType.Int;
        Parameters[14].ParameterName = "IsStartMachine";
        Parameters[14].Value = isstartmachine;

        Parameters[15] = new SqlParameter();
        Parameters[15].SqlDbType = SqlDbType.NVarChar;
        Parameters[15].ParameterName = "Notes";
        Parameters[15].Value = notes;

        Parameters[16] = new SqlParameter();
        Parameters[16].SqlDbType = SqlDbType.Int;
        Parameters[16].ParameterName = "IsDeliveryFatory";
        Parameters[16].Value = isdeliveryfactory;

        Parameters[17] = new SqlParameter();
        Parameters[17].SqlDbType = SqlDbType.Int;
        Parameters[17].ParameterName = "IsDeliverySite";
        Parameters[17].Value = isdeliverysite;

        Parameters[18] = new SqlParameter();
        Parameters[18].SqlDbType = SqlDbType.Int;
        Parameters[18].ParameterName = "IsSteel";
        Parameters[18].Value = issteel;

        Parameters[19] = new SqlParameter();
        Parameters[19].SqlDbType = SqlDbType.Int;
        Parameters[19].ParameterName = "IsCompleteAssembleonSite";
        Parameters[19].Value = iscompleteassembleonsite;

        Parameters[20] = new SqlParameter();
        Parameters[20].SqlDbType = SqlDbType.Int;
        Parameters[20].ParameterName = "IsDeliveryAccessories";
        Parameters[20].Value = isdeliveryaccessories;

        Parameters[21] = new SqlParameter();
        Parameters[21].SqlDbType = SqlDbType.Int;
        Parameters[21].ParameterName = "IsRepair";
        Parameters[21].Value = isrepair;

        Parameters[22] = new SqlParameter();
        Parameters[22].SqlDbType = SqlDbType.Int;
        Parameters[22].ParameterName = "IsSales";
        Parameters[22].Value = issales;

        Parameters[23] = new SqlParameter();
        Parameters[23].SqlDbType = SqlDbType.Int;
        Parameters[23].ParameterName = "supervisorid";
        Parameters[23].Value = supervisorid;

        Parameters[24] = new SqlParameter();
        Parameters[24].SqlDbType = SqlDbType.NVarChar;
        Parameters[24].ParameterName = "Hours";
        Parameters[24].Value = hours;

        Parameters[25] = new SqlParameter();
        Parameters[25].SqlDbType = SqlDbType.NVarChar;
        Parameters[25].ParameterName = "GRPTypeid";
        Parameters[25].Value = grptypeid;

        Parameters[26] = new SqlParameter();
        Parameters[26].SqlDbType = SqlDbType.NVarChar;
        Parameters[26].ParameterName = "empname";
        Parameters[26].Value = Session["Admin_Name"];

        Parameters[27] = new SqlParameter();
        Parameters[27].SqlDbType = SqlDbType.NVarChar;
        Parameters[27].ParameterName = "WOID";
        Parameters[27].Value = woid;

        Parameters[28] = new SqlParameter();
        Parameters[28].SqlDbType = SqlDbType.NVarChar;
        Parameters[28].ParameterName = "isCompleted";
        Parameters[28].Value = iscompleted;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_WO_GRPSection_Detail", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    #region GlassFybre Types


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetGlassFybreTypes(int id, string productname)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "PorductName";
        Parameters[1].Value = productname;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_GlassFybreTypes", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditGlassFybreType()
    {
        int id = Convert.ToInt32(HttpContext.Current.Request["id"]);
        int processstaionid = Convert.ToInt32(HttpContext.Current.Request["processstaionid"]);
        string machine = HttpContext.Current.Request["machine"];
        string productname = HttpContext.Current.Request["productname"];

        string picture = "", filepath = "";
        if (HttpContext.Current.Request.Files.AllKeys.Any())
        {
            // Get the uploaded image from the Files collection
            var httpPostedFile = HttpContext.Current.Request.Files["picture"];
            if (httpPostedFile != null)
            {
                picture = GetUniqueAlphaNumericKey(15) + ".png";

                // Get the complete file path
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("content/glassfybretypes"), picture);
                // Save the uploaded file to "UploadedFiles" folder
                httpPostedFile.SaveAs(fileSavePath);
                filepath = domainurl + "/content/glassfybretypes/" + picture;
            }
        }
        SqlParameter[] Parameters = new SqlParameter[5];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "ProcessStationID";
        Parameters[1].Value = processstaionid;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "MachineNumber";
        Parameters[2].Value = machine;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "ProductName";
        Parameters[3].Value = productname;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "picture";
        Parameters[4].Value = filepath;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_GlassFybreType", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteGlassFybreType(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_GlassFybreType", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }


    #endregion

    #region WO Glass Fybre Hand Layer

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchWOGFHandLayer(int id, string productname, string frmdate, string todate)
    {
        SqlParameter[] Parameters = new SqlParameter[4];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "ProductName";
        Parameters[1].Value = productname;


        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "FROMDATE";
        if (frmdate == "")
        {
            Parameters[2].Value = "1/1/1900";
        }
        else
        {
            Parameters[2].Value = frmdate;
        }

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "TODATE";
        if (todate == "")
        {
            Parameters[3].Value = "1/1/1900";
        }
        else
        {
            Parameters[3].Value = todate;
        }

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Search_WOGFHandLayer", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetWOGFHandLayer(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;


        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_WO_GFHandLayer", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditWOGFHandLayer(int id, string quantity, int processstaionID, string productname, string machinenumber
        , int ishandlayup, int issupplysite, int issupplyfactory, int isfixing, string notes, int isrepair, int issales
        , int supervisorid, string hours, int gfhltypeid, int iscompleted)
    {
        SqlParameter[] Parameters = new SqlParameter[17];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "ProcessStationID";
        Parameters[2].Value = processstaionID;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "ProdcutName";
        Parameters[3].Value = productname;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "MachineNumber";
        Parameters[4].Value = machinenumber;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.Int;
        Parameters[5].ParameterName = "IsHandLayUp";
        Parameters[5].Value = ishandlayup;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.Int;
        Parameters[6].ParameterName = "IsSupplySite";
        Parameters[6].Value = issupplysite;

        Parameters[7] = new SqlParameter();
        Parameters[7].SqlDbType = SqlDbType.Int;
        Parameters[7].ParameterName = "IsSupplyFactory";
        Parameters[7].Value = issupplyfactory;

        Parameters[8] = new SqlParameter();
        Parameters[8].SqlDbType = SqlDbType.Int;
        Parameters[8].ParameterName = "IsFixing";
        Parameters[8].Value = isfixing;

        Parameters[9] = new SqlParameter();
        Parameters[9].SqlDbType = SqlDbType.NVarChar;
        Parameters[9].ParameterName = "Notes";
        Parameters[9].Value = notes;

        Parameters[10] = new SqlParameter();
        Parameters[10].SqlDbType = SqlDbType.Int;
        Parameters[10].ParameterName = "IsRepair";
        Parameters[10].Value = isrepair;

        Parameters[11] = new SqlParameter();
        Parameters[11].SqlDbType = SqlDbType.Int;
        Parameters[11].ParameterName = "IsSales";
        Parameters[11].Value = issales;

        Parameters[12] = new SqlParameter();
        Parameters[12].SqlDbType = SqlDbType.Int;
        Parameters[12].ParameterName = "supervisorid";
        Parameters[12].Value = supervisorid;

        Parameters[13] = new SqlParameter();
        Parameters[13].SqlDbType = SqlDbType.NVarChar;
        Parameters[13].ParameterName = "Hours";
        Parameters[13].Value = hours;

        Parameters[14] = new SqlParameter();
        Parameters[14].SqlDbType = SqlDbType.NVarChar;
        Parameters[14].ParameterName = "GFHLTypeID";
        Parameters[14].Value = gfhltypeid;

        Parameters[15] = new SqlParameter();
        Parameters[15].SqlDbType = SqlDbType.NVarChar;
        Parameters[15].ParameterName = "empname";
        Parameters[15].Value = Session["Admin_Name"];

        Parameters[16] = new SqlParameter();
        Parameters[16].SqlDbType = SqlDbType.Int;
        Parameters[16].ParameterName = "isCompleted";
        Parameters[16].Value = iscompleted;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_WO_GFHandLayer", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteWOGFHandLayer(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "empname";
        Parameters[1].Value = Session["Admin_Name"];

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_WO_GFHandLayer", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    #region WO GFHL Detail

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetWOGFHandLayerDetail(int id, int woid)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "WOID";
        Parameters[1].Value = woid;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_WO_GFHandLayer_Detail", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditWOGFHandLayerDetail(int id, string quantity, int processstaionID, string productname, string machinenumber
       , int ishandlayup, int issupplysite, int issupplyfactory, int isfixing, string notes, int isrepair, int issales
       , int supervisorid, string hours, int gfhltypeid, int woid, string iscompleted)
    {
        SqlParameter[] Parameters = new SqlParameter[18];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "ProcessStationID";
        Parameters[2].Value = processstaionID;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "ProdcutName";
        Parameters[3].Value = productname;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "MachineNumber";
        Parameters[4].Value = machinenumber;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.Int;
        Parameters[5].ParameterName = "IsHandLayUp";
        Parameters[5].Value = ishandlayup;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.Int;
        Parameters[6].ParameterName = "IsSupplySite";
        Parameters[6].Value = issupplysite;

        Parameters[7] = new SqlParameter();
        Parameters[7].SqlDbType = SqlDbType.Int;
        Parameters[7].ParameterName = "IsSupplyFactory";
        Parameters[7].Value = issupplyfactory;

        Parameters[8] = new SqlParameter();
        Parameters[8].SqlDbType = SqlDbType.Int;
        Parameters[8].ParameterName = "IsFixing";
        Parameters[8].Value = isfixing;

        Parameters[9] = new SqlParameter();
        Parameters[9].SqlDbType = SqlDbType.NVarChar;
        Parameters[9].ParameterName = "Notes";
        Parameters[9].Value = notes;

        Parameters[10] = new SqlParameter();
        Parameters[10].SqlDbType = SqlDbType.Int;
        Parameters[10].ParameterName = "IsRepair";
        Parameters[10].Value = isrepair;

        Parameters[11] = new SqlParameter();
        Parameters[11].SqlDbType = SqlDbType.Int;
        Parameters[11].ParameterName = "IsSales";
        Parameters[11].Value = issales;

        Parameters[12] = new SqlParameter();
        Parameters[12].SqlDbType = SqlDbType.Int;
        Parameters[12].ParameterName = "supervisorid";
        Parameters[12].Value = supervisorid;

        Parameters[13] = new SqlParameter();
        Parameters[13].SqlDbType = SqlDbType.NVarChar;
        Parameters[13].ParameterName = "Hours";
        Parameters[13].Value = hours;

        Parameters[14] = new SqlParameter();
        Parameters[14].SqlDbType = SqlDbType.NVarChar;
        Parameters[14].ParameterName = "GFHLTypeID";
        Parameters[14].Value = gfhltypeid;

        Parameters[15] = new SqlParameter();
        Parameters[15].SqlDbType = SqlDbType.NVarChar;
        Parameters[15].ParameterName = "empname";
        Parameters[15].Value = Session["Admin_Name"];

        Parameters[16] = new SqlParameter();
        Parameters[16].SqlDbType = SqlDbType.NVarChar;
        Parameters[16].ParameterName = "WOID";
        Parameters[16].Value = woid;

        Parameters[17] = new SqlParameter();
        Parameters[17].SqlDbType = SqlDbType.NVarChar;
        Parameters[17].ParameterName = "isCompleted";
        Parameters[17].Value = iscompleted;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_WO_GFHandLayer_Detail", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }
    #endregion

    #region AluminiumTypes

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetAluminiumTypes(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_AluminiumTypes", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditAluminiumType()
    {

        int id = Convert.ToInt32(HttpContext.Current.Request["id"]);
        string typename = HttpContext.Current.Request["typename"];
        string serialno = HttpContext.Current.Request["serialno"];

        string picture = "", filepath = "";
        if (HttpContext.Current.Request.Files.AllKeys.Any())
        {
            // Get the uploaded image from the Files collection
            var httpPostedFile = HttpContext.Current.Request.Files["picture"];
            if (httpPostedFile != null)
            {
                picture = GetUniqueAlphaNumericKey(15) + ".png";

                // Get the complete file path
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("content/aluminiumtypes"), picture);
                // Save the uploaded file to "UploadedFiles" folder
                httpPostedFile.SaveAs(fileSavePath);
                filepath = domainurl + "/content/aluminiumtypes/" + picture;
            }
        }
        SqlParameter[] Parameters = new SqlParameter[4];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "TypeName";
        Parameters[1].Value = typename;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "serialnumber";
        Parameters[2].Value = serialno;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "picture";
        Parameters[3].Value = filepath;


        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_AluminiumType", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteAluminiumType(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_AluminiumType", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    #region Work Order Aluminium

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetWOAluminium(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_WO_Aluminium", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditWOAluminium(int id, string quantity, string serialno, string width, string height, string color, string thickness
        , int aluminiumtypeid, int isaluminiumcut, int isglasscut, int isgathering, int isinstallforglass, int isinstallforother, int isrepair,
        int issales, int iscuts, int isoutsideinstall, int issendglass, int issendaluminium, int issendother, int isinstallglass,
        int isinstallaluminium, int isinstallother, string notes, int supervisorid, string hours, int iscompleted)
    {
        SqlParameter[] Parameters = new SqlParameter[28];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "serialnumber";
        Parameters[2].Value = serialno;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "Width";
        Parameters[3].Value = width;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "Height";
        Parameters[4].Value = height;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "Color";
        Parameters[5].Value = color;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.NVarChar;
        Parameters[6].ParameterName = "Thickness";
        Parameters[6].Value = thickness;

        Parameters[7] = new SqlParameter();
        Parameters[7].SqlDbType = SqlDbType.Int;
        Parameters[7].ParameterName = "AluminiumTypeID";
        Parameters[7].Value = aluminiumtypeid;

        Parameters[8] = new SqlParameter();
        Parameters[8].SqlDbType = SqlDbType.Int;
        Parameters[8].ParameterName = "IsAluminiumCut";
        Parameters[8].Value = isaluminiumcut;

        Parameters[9] = new SqlParameter();
        Parameters[9].SqlDbType = SqlDbType.Int;
        Parameters[9].ParameterName = "IsGlassCut";
        Parameters[9].Value = isglasscut;

        Parameters[10] = new SqlParameter();
        Parameters[10].SqlDbType = SqlDbType.Int;
        Parameters[10].ParameterName = "IsGathering";
        Parameters[10].Value = isgathering;

        Parameters[11] = new SqlParameter();
        Parameters[11].SqlDbType = SqlDbType.Int;
        Parameters[11].ParameterName = "IsInstallForGlass";
        Parameters[11].Value = isinstallforglass;

        Parameters[12] = new SqlParameter();
        Parameters[12].SqlDbType = SqlDbType.Int;
        Parameters[12].ParameterName = "IsInstallForOther";
        Parameters[12].Value = isinstallforother;

        Parameters[13] = new SqlParameter();
        Parameters[13].SqlDbType = SqlDbType.Int;
        Parameters[13].ParameterName = "IsRepair";
        Parameters[13].Value = isrepair;

        Parameters[14] = new SqlParameter();
        Parameters[14].SqlDbType = SqlDbType.Int;
        Parameters[14].ParameterName = "IsSales";
        Parameters[14].Value = issales;

        Parameters[15] = new SqlParameter();
        Parameters[15].SqlDbType = SqlDbType.Int;
        Parameters[15].ParameterName = "IsCuts";
        Parameters[15].Value = iscuts;

        Parameters[16] = new SqlParameter();
        Parameters[16].SqlDbType = SqlDbType.Int;
        Parameters[16].ParameterName = "IsInstallOutside";
        Parameters[16].Value = isoutsideinstall;

        Parameters[17] = new SqlParameter();
        Parameters[17].SqlDbType = SqlDbType.Int;
        Parameters[17].ParameterName = "IsSendGlass";
        Parameters[17].Value = issendglass;

        Parameters[18] = new SqlParameter();
        Parameters[18].SqlDbType = SqlDbType.Int;
        Parameters[18].ParameterName = "IsSendAluminium";
        Parameters[18].Value = issendaluminium;

        Parameters[19] = new SqlParameter();
        Parameters[19].SqlDbType = SqlDbType.Int;
        Parameters[19].ParameterName = "IsSendOther";
        Parameters[19].Value = issendother;

        Parameters[20] = new SqlParameter();
        Parameters[20].SqlDbType = SqlDbType.Int;
        Parameters[20].ParameterName = "IsInstallGlass";
        Parameters[20].Value = isinstallglass;

        Parameters[21] = new SqlParameter();
        Parameters[21].SqlDbType = SqlDbType.Int;
        Parameters[21].ParameterName = "IsInstallAluminium";
        Parameters[21].Value = isinstallaluminium;

        Parameters[22] = new SqlParameter();
        Parameters[22].SqlDbType = SqlDbType.Int;
        Parameters[22].ParameterName = "IsInstallOther";
        Parameters[22].Value = isinstallother;

        Parameters[23] = new SqlParameter();
        Parameters[23].SqlDbType = SqlDbType.NVarChar;
        Parameters[23].ParameterName = "Notes";
        Parameters[23].Value = notes;

        Parameters[24] = new SqlParameter();
        Parameters[24].SqlDbType = SqlDbType.Int;
        Parameters[24].ParameterName = "supervisorid";
        Parameters[24].Value = supervisorid;

        Parameters[25] = new SqlParameter();
        Parameters[25].SqlDbType = SqlDbType.NVarChar;
        Parameters[25].ParameterName = "Hours";
        Parameters[25].Value = hours;

        Parameters[26] = new SqlParameter();
        Parameters[26].SqlDbType = SqlDbType.NVarChar;
        Parameters[26].ParameterName = "empname";
        Parameters[26].Value = Session["Admin_Name"];

        Parameters[27] = new SqlParameter();
        Parameters[27].SqlDbType = SqlDbType.Int;
        Parameters[27].ParameterName = "isCompleted";
        Parameters[27].Value = iscompleted;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_WO_Aluminium", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteWOAluminium(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "empname";
        Parameters[1].Value = Session["Admin_Name"];

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_WO_Aluminium", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchWOAluminium(int id, string serialno, int aluminiumtypeid, string frmdate, string todate)
    {
        SqlParameter[] Parameters = new SqlParameter[5];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "serialnumber";
        Parameters[1].Value = serialno;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "AluminiumTypeID";
        Parameters[2].Value = aluminiumtypeid;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "FROMDATE";
        if (frmdate == "")
        {
            Parameters[3].Value = "1/1/1900";
        }
        else
        {
            Parameters[3].Value = frmdate;
        }

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "TODATE";
        if (todate == "")
        {
            Parameters[4].Value = "1/1/1900";
        }
        else
        {
            Parameters[4].Value = todate;
        }

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Search_WO_Aluminium", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    #region WO Aluminium Detail

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetWOAluminiumDetail(int id, int woid)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "WOID";
        Parameters[1].Value = woid;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_WO_Aluminium_Detail", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditWOAluminiumDetail(int id, string quantity, string serialno, string width, string height, string color, string thickness
        , int aluminiumtypeid, int isaluminiumcut, int isglasscut, int isgathering, int isinstallforglass, int isinstallforother, int isrepair,
        int issales, int iscuts, int isoutsideinstall, int issendglass, int issendaluminium, int issendother, int isinstallglass,
        int isinstallaluminium, int isinstallother, string notes, int supervisorid, string hours, int woid, string iscompleted)
    {
        SqlParameter[] Parameters = new SqlParameter[29];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "serialnumber";
        Parameters[2].Value = serialno;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "Width";
        Parameters[3].Value = width;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "Height";
        Parameters[4].Value = height;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "Color";
        Parameters[5].Value = color;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.NVarChar;
        Parameters[6].ParameterName = "Thickness";
        Parameters[6].Value = thickness;

        Parameters[7] = new SqlParameter();
        Parameters[7].SqlDbType = SqlDbType.Int;
        Parameters[7].ParameterName = "AluminiumTypeID";
        Parameters[7].Value = aluminiumtypeid;

        Parameters[8] = new SqlParameter();
        Parameters[8].SqlDbType = SqlDbType.Int;
        Parameters[8].ParameterName = "IsAluminiumCut";
        Parameters[8].Value = isaluminiumcut;

        Parameters[9] = new SqlParameter();
        Parameters[9].SqlDbType = SqlDbType.Int;
        Parameters[9].ParameterName = "IsGlassCut";
        Parameters[9].Value = isglasscut;

        Parameters[10] = new SqlParameter();
        Parameters[10].SqlDbType = SqlDbType.Int;
        Parameters[10].ParameterName = "IsGathering";
        Parameters[10].Value = isgathering;

        Parameters[11] = new SqlParameter();
        Parameters[11].SqlDbType = SqlDbType.Int;
        Parameters[11].ParameterName = "IsInstallForGlass";
        Parameters[11].Value = isinstallforglass;

        Parameters[12] = new SqlParameter();
        Parameters[12].SqlDbType = SqlDbType.Int;
        Parameters[12].ParameterName = "IsInstallForOther";
        Parameters[12].Value = isinstallforother;

        Parameters[13] = new SqlParameter();
        Parameters[13].SqlDbType = SqlDbType.Int;
        Parameters[13].ParameterName = "IsRepair";
        Parameters[13].Value = isrepair;

        Parameters[14] = new SqlParameter();
        Parameters[14].SqlDbType = SqlDbType.Int;
        Parameters[14].ParameterName = "IsSales";
        Parameters[14].Value = issales;

        Parameters[15] = new SqlParameter();
        Parameters[15].SqlDbType = SqlDbType.Int;
        Parameters[15].ParameterName = "IsCuts";
        Parameters[15].Value = iscuts;

        Parameters[16] = new SqlParameter();
        Parameters[16].SqlDbType = SqlDbType.Int;
        Parameters[16].ParameterName = "IsInstallOutside";
        Parameters[16].Value = isoutsideinstall;

        Parameters[17] = new SqlParameter();
        Parameters[17].SqlDbType = SqlDbType.Int;
        Parameters[17].ParameterName = "IsSendGlass";
        Parameters[17].Value = issendglass;

        Parameters[18] = new SqlParameter();
        Parameters[18].SqlDbType = SqlDbType.Int;
        Parameters[18].ParameterName = "IsSendAluminium";
        Parameters[18].Value = issendaluminium;

        Parameters[19] = new SqlParameter();
        Parameters[19].SqlDbType = SqlDbType.Int;
        Parameters[19].ParameterName = "IsSendOther";
        Parameters[19].Value = issendother;

        Parameters[20] = new SqlParameter();
        Parameters[20].SqlDbType = SqlDbType.Int;
        Parameters[20].ParameterName = "IsInstallGlass";
        Parameters[20].Value = isinstallglass;

        Parameters[21] = new SqlParameter();
        Parameters[21].SqlDbType = SqlDbType.Int;
        Parameters[21].ParameterName = "IsInstallAluminium";
        Parameters[21].Value = isinstallaluminium;

        Parameters[22] = new SqlParameter();
        Parameters[22].SqlDbType = SqlDbType.Int;
        Parameters[22].ParameterName = "IsInstallOther";
        Parameters[22].Value = isinstallother;

        Parameters[23] = new SqlParameter();
        Parameters[23].SqlDbType = SqlDbType.NVarChar;
        Parameters[23].ParameterName = "Notes";
        Parameters[23].Value = notes;

        Parameters[24] = new SqlParameter();
        Parameters[24].SqlDbType = SqlDbType.Int;
        Parameters[24].ParameterName = "supervisorid";
        Parameters[24].Value = supervisorid;

        Parameters[25] = new SqlParameter();
        Parameters[25].SqlDbType = SqlDbType.NVarChar;
        Parameters[25].ParameterName = "Hours";
        Parameters[25].Value = hours;

        Parameters[26] = new SqlParameter();
        Parameters[26].SqlDbType = SqlDbType.NVarChar;
        Parameters[26].ParameterName = "empname";
        Parameters[26].Value = Session["Admin_Name"];

        Parameters[27] = new SqlParameter();
        Parameters[27].SqlDbType = SqlDbType.NVarChar;
        Parameters[27].ParameterName = "WOID";
        Parameters[27].Value = woid;

        Parameters[28] = new SqlParameter();
        Parameters[28].SqlDbType = SqlDbType.NVarChar;
        Parameters[28].ParameterName = "isCompleted";
        Parameters[28].Value = iscompleted;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_WO_Aluminium_Detail", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }
    #endregion

    #region User
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditUser(int id, string username, string password, string name, string email, int roleid, string active)
    {
        SqlParameter[] Parameters = new SqlParameter[7];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.VarChar;
        Parameters[1].ParameterName = "Username";
        Parameters[1].Value = username.Trim();

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.VarChar;
        Parameters[2].ParameterName = "password";
        Parameters[2].Value = password.Trim();

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.VarChar;
        Parameters[3].ParameterName = "Name";
        Parameters[3].Value = name;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.VarChar;
        Parameters[4].ParameterName = "Email";
        Parameters[4].Value = email;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.Int;
        Parameters[5].ParameterName = "roleid";
        Parameters[5].Value = roleid;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.VarChar;
        Parameters[6].ParameterName = "active";
        Parameters[6].Value = active;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_USER", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteUser(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_User", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetUsers(int user)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "UserID";
        Parameters[0].Value = user;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_Users", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void ChangeUserProfile(int id, string username, string email, string password, string newpassword, int count)
    {
        SqlParameter[] Parameters = new SqlParameter[6];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "UserName";
        Parameters[1].Value = username.Trim();

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "EMAIL";
        Parameters[2].Value = email;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "PASSWORD";
        Parameters[3].Value = password.Trim();

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "NEWPASSWORD";
        Parameters[4].Value = newpassword.Trim();

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.Int;
        Parameters[5].ParameterName = "COUNT";
        Parameters[5].Value = count;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Change_UserProfile", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void LoginAdmin(string username, string password)
    {

        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.NVarChar;
        Parameters[0].ParameterName = "@Username";
        Parameters[0].Value = username.Trim();

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "@Password";
        Parameters[1].Value = password.Trim();

        string result = "invalid";

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Login_Admin", Parameters);
        if (dtResult.Rows.Count > 0)
        {
            result = "success";

            if (dtResult.Rows[0][0].ToString() != "-1")
            {
                Session["Admin_ID"] = dtResult.Rows[0]["TB_ID"].ToString();
                Session["Admin_Username"] = dtResult.Rows[0]["TB_USERNAME"].ToString();
                Session["Admin_Name"] = dtResult.Rows[0]["TB_NAME"].ToString();
                Session["Admin_Role"] = dtResult.Rows[0]["TB_ROLE_ID"].ToString();
                Session["User_Type_ID"] = dtResult.Rows[0]["USER_TYPE_ID"].ToString();
            }
        }

        Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.NVarChar;
        Parameters[0].ParameterName = "@RoleID";
        Parameters[0].Value = Session["Admin_Role"];

        dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_Role_Permissions", Parameters);
        if (dtResult.Rows.Count > 0)
        {
            Session["Admin_Permissions"] = dtResult;
        }

        HttpContext.Current.Response.Write(result);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void UserNameExist(string username, int id)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.NVarChar;
        Parameters[0].ParameterName = "username";
        Parameters[0].Value = username;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "ID";
        Parameters[1].Value = id;

        string result = "true";

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Check_Username", Parameters);
        if (dtResult.Rows.Count > 0)
        {
            result = "false";
        }
        HttpContext.Current.Response.Write(result);
    }

    #endregion

    #region Tablet User
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditTabletUser(int id, string username, string password, string name, string mobile, int roleid, string active)
    {
        SqlParameter[] Parameters = new SqlParameter[7];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.VarChar;
        Parameters[1].ParameterName = "Username";
        Parameters[1].Value = username;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.VarChar;
        Parameters[2].ParameterName = "password";
        Parameters[2].Value = password;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.VarChar;
        Parameters[3].ParameterName = "Name";
        Parameters[3].Value = name;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.VarChar;
        Parameters[4].ParameterName = "mobile";
        Parameters[4].Value = mobile;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.Int;
        Parameters[5].ParameterName = "TabletRoleID";
        Parameters[5].Value = roleid;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.VarChar;
        Parameters[6].ParameterName = "active";
        Parameters[6].Value = active;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_TabletUser", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteTabletUser(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_User", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTabletUsers(int user, int tabletroleid)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "UserID";
        Parameters[0].Value = user;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "TabletRoleID";
        Parameters[1].Value = tabletroleid;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TabletUser", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void TabletUserNameExist(string username, int id)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.NVarChar;
        Parameters[0].ParameterName = "username";
        Parameters[0].Value = username;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "ID";
        Parameters[1].Value = id;

        string result = "true";

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Check_TabletUsername", Parameters);
        if (dtResult.Rows.Count > 0)
        {
            result = "false";
        }
        HttpContext.Current.Response.Write(result);
    }

    #endregion

    #region Tablet Roles

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveTabletRoles(int id, string name)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "Name";
        Parameters[1].Value = name;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_TabletRole", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTabletRoles(int role)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "RoleID";
        Parameters[0].Value = role;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TabletRoles", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteTabletRoles(int role)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "RoleID";
        Parameters[0].Value = role;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_TabletRole", Parameters);

        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    #region Roles

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveRoles(int id, string name, string permissions, int allowdoortype, int allowalutype,
        int allowtanktype, int allowgrptype, int allowgfhtype, int usertypeid)
    {
        SqlParameter[] Parameters = new SqlParameter[9];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "Name";
        Parameters[1].Value = name;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "Permissions";
        Parameters[2].Value = permissions;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.Int;
        Parameters[3].ParameterName = "AllowDoorType";
        Parameters[3].Value = allowdoortype;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.Int;
        Parameters[4].ParameterName = "AllowAluminiumType";
        Parameters[4].Value = allowalutype;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.Int;
        Parameters[5].ParameterName = "AllowTankType";
        Parameters[5].Value = allowtanktype;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.Int;
        Parameters[6].ParameterName = "AllowGRPType";
        Parameters[6].Value = allowgrptype;

        Parameters[7] = new SqlParameter();
        Parameters[7].SqlDbType = SqlDbType.Int;
        Parameters[7].ParameterName = "AllowGFHType";
        Parameters[7].Value = allowgfhtype;

        Parameters[8] = new SqlParameter();
        Parameters[8].SqlDbType = SqlDbType.Int;
        Parameters[8].ParameterName = "Usertypeid";
        Parameters[8].Value = usertypeid;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_Role", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRoles(int role, int showadmin)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "RoleID";
        Parameters[0].Value = role;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "showadmin";
        Parameters[1].Value = showadmin;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_Roles", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteRoles(int role)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "RoleID";
        Parameters[0].Value = role;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_Role", Parameters);

        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetUserTypes(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_UserTypes", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    #region Permissions

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPermissionTypes()
    {

        SqlParameter[] Parameters = new SqlParameter[0];

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_Permission_Types", Parameters);

        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPermissionPages()
    {

        SqlParameter[] Parameters = new SqlParameter[0];

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_Permission_Pages", Parameters);

        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }



    #endregion

    #region Dashboard

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDashboardStatistics()
    {

        SqlParameter[] Parameters = new SqlParameter[0];


        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_DashboardStatistics", Parameters);

        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    #region Task Sales

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditTaskSales(int id, int typeid, string date, string custname, string desc, string rmk, int tskstatus, int taskfor)
    {
        SqlParameter[] Parameters = new SqlParameter[9];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "TypeID";
        Parameters[1].Value = typeid;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "date";
        Parameters[2].Value = date;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "custname";
        Parameters[3].Value = custname;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "desc";
        Parameters[4].Value = desc;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "rmk";
        Parameters[5].Value = rmk;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.Int;
        Parameters[6].ParameterName = "tskstatus";
        Parameters[6].Value = tskstatus;

        Parameters[7] = new SqlParameter();
        Parameters[7].SqlDbType = SqlDbType.NVarChar;
        Parameters[7].ParameterName = "empname";
        Parameters[7].Value = Session["Admin_Name"];

        Parameters[8] = new SqlParameter();
        Parameters[8].SqlDbType = SqlDbType.Int;
        Parameters[8].ParameterName = "TypeFor";
        Parameters[8].Value = taskfor;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_TaskSales", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));

        if (dtResult.Rows.Count > 0)
        {
            if (id == 0)
            {
                Int32 salestaskid = Convert.ToInt32(dtResult.Rows[0].ItemArray[0].ToString());

                string title = "Task Assigned";
                string Body = "Please check the application for new Task";

                #region DeviceID

                string deviceid = "";

                SqlParameter[] parm = new SqlParameter[2];

                parm[0] = new SqlParameter();
                parm[0].SqlDbType = SqlDbType.Int;
                parm[0].ParameterName = "UserID";
                parm[0].Value = taskfor;

                parm[1] = new SqlParameter();
                parm[1].SqlDbType = SqlDbType.Int;
                parm[1].ParameterName = "TabletRoleID";
                parm[1].Value = 4;

                DataTable userrslt = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TabletUser", parm);
                if (userrslt.Rows.Count > 0)
                {
                    deviceid = userrslt.Rows[0]["DeviceID"].ToString();
                }
                #endregion

                GetSalesTaskByUserIDDTO gst = new GetSalesTaskByUserIDDTO();

                List<SalesTask> stlst = new List<SalesTask>();

                SalesTask st = new SalesTask();
                st.id = salestaskid.ToString();
                st.remark = rmk;
                st.taskforuserid = taskfor.ToString();
                st.customername = custname;
                st.datetime = date;
                st.description = desc;

                #region Images

                List<Images> imglst = new List<Images>();


                SqlParameter[] pm = new SqlParameter[1];

                pm[0] = new SqlParameter();
                pm[0].SqlDbType = SqlDbType.Int;
                pm[0].ParameterName = "TaskSalesID";
                pm[0].Value = salestaskid;

                DataTable imgrslt = DataAccess.GetDataTableFromStoredProcedure("sp_Get_ImagesTaskSales", pm);
                if (imgrslt.Rows.Count > 0)
                {
                    for (int i = 0; i < imgrslt.Rows.Count; i++)
                    {
                        Images img = new Images();
                        img.imgpath = imgrslt.Rows[i].ItemArray[2].ToString();
                        imglst.Add(img);
                    }


                }


                #endregion

                st.images = imglst;

                #region Task status

                TaskStatus ts = new TaskStatus();

                SqlParameter[] Para = new SqlParameter[1];

                Para[0] = new SqlParameter();
                Para[0].SqlDbType = SqlDbType.Int;
                Para[0].ParameterName = "ID";
                Para[0].Value = tskstatus;

                DataTable tsrslt = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TaskStatus", Para);
                if (tsrslt.Rows.Count > 0)
                {
                    for (int i = 0; i < tsrslt.Rows.Count; i++)
                    {
                        ts.id = tsrslt.Rows[i].ItemArray[0].ToString();
                        ts.taskstatusname = tsrslt.Rows[i].ItemArray[1].ToString();
                    }
                }

                #endregion

                st.taskstatus = ts;

                #region Task Type

                TaskTypes tt = new TaskTypes();

                SqlParameter[] prm = new SqlParameter[1];
                prm[0] = new SqlParameter();
                prm[0].SqlDbType = SqlDbType.NVarChar;
                prm[0].ParameterName = "IDs";
                prm[0].Value = typeid;

                DataTable ttrslt = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TaskType", prm);
                if (ttrslt.Rows.Count > 0)
                {
                    for (int i = 0; i < ttrslt.Rows.Count; i++)
                    {
                        tt.id = ttrslt.Rows[i].ItemArray[0].ToString();
                        tt.typename = ttrslt.Rows[i].ItemArray[1].ToString();
                    }
                }
                #endregion

                st.tasktype = tt;
                stlst.Add(st);

                gst.salestasks = stlst;

                dynamic dygst = gst;


                FireBase.SendPushNotification(deviceid, title, Body, dygst);
            }
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTaskSales(int id, int tasktypeid, int taskstatus, string frmdate, string todate)
    {
        string typeids = "";
        int roleid = Convert.ToInt32(Session["Admin_Role"]);

        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "RoleID";
        Parameters[0].Value = roleid;

        if (tasktypeid == 0)
        {
            if (roleid != 1)
            {
                DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TaskTypeRoles", Parameters);
                if (dtResult.Rows.Count > 0)
                {
                    if (dtResult.Rows[0]["AllowDoorType"].ToString() == "True")
                        typeids = typeids + "1,";
                    if (dtResult.Rows[0]["AllowAluminiumType"].ToString() == "True")
                        typeids = typeids + "2,";
                    if (dtResult.Rows[0]["AllowTankType"].ToString() == "True")
                        typeids = typeids + "3,";
                    if (dtResult.Rows[0]["AllowGRPType"].ToString() == "True")
                        typeids = typeids + "4,";
                    if (dtResult.Rows[0]["AllowGFHType"].ToString() == "True")
                        typeids = typeids + "5,";
                }
                else
                {
                    typeids = "-1";
                }
            }
        }
        else
        {
            typeids = tasktypeid.ToString();
        }
        Parameters = new SqlParameter[5];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "TYPEIDs";
        if (typeids == "")
        {
            Parameters[1].Value = "1,2,3,4,5";
        }
        else
        {
            Parameters[1].Value = typeids;
        }

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "TaskStatus";
        Parameters[2].Value = taskstatus;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "FROMDATE";
        if (frmdate == "")
        {
            Parameters[3].Value = "1/1/1900";
        }
        else
        {
            Parameters[3].Value = frmdate;
        }

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "TODATE";
        if (todate == "")
        {
            Parameters[4].Value = "1/1/1900";
        }
        else
        {
            Parameters[4].Value = todate;
        }

        DataTable dtblResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TaskSales", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtblResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTaskTypes()
    {
        int roleid = Convert.ToInt32(Session["Admin_Role"]);
        string ids = "";

        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "RoleID";
        Parameters[0].Value = roleid;

        if (roleid != 1)
        {
            DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TaskTypeRoles", Parameters);
            if (dtResult.Rows.Count > 0)
            {
                if (dtResult.Rows[0]["AllowDoorType"].ToString() == "True")
                    ids = ids + "1,";
                if (dtResult.Rows[0]["AllowAluminiumType"].ToString() == "True")
                    ids = ids + "2,";
                if (dtResult.Rows[0]["AllowTankType"].ToString() == "True")
                    ids = ids + "3,";
                if (dtResult.Rows[0]["AllowGRPType"].ToString() == "True")
                    ids = ids + "4,";
                if (dtResult.Rows[0]["AllowGFHType"].ToString() == "True")
                    ids = ids + "5,";
            }
            else
            {
                ids = "-1";
            }
        }
        Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.NVarChar;
        Parameters[0].ParameterName = "IDs";
        if (ids == "")
        {
            Parameters[0].Value = "1,2,3,4,5";
        }
        else
        {
            Parameters[0].Value = ids;
        }

        DataTable dtblResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TaskType", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtblResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTaskStatus(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TaskStatus", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteTaskSales(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "empname";
        Parameters[1].Value = Session["Admin_Name"];

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_TaskSales", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSalesImages(int tasksalesid)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "TaskSalesID";
        Parameters[0].Value = tasksalesid;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_ImagesTaskSales", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }
    #endregion

    #region Task Mandoop

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddEditTaskMandoop(int id, int typeid, string date, string custname, string desc, string rmk, int tskstatus, int taskfor)
    {
        SqlParameter[] Parameters = new SqlParameter[9];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "TypeID";
        Parameters[1].Value = typeid;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.NVarChar;
        Parameters[2].ParameterName = "date";
        Parameters[2].Value = date;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "custname";
        Parameters[3].Value = custname;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "desc";
        Parameters[4].Value = desc;

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "rmk";
        Parameters[5].Value = rmk;

        Parameters[6] = new SqlParameter();
        Parameters[6].SqlDbType = SqlDbType.Int;
        Parameters[6].ParameterName = "tskstatus";
        Parameters[6].Value = tskstatus;

        Parameters[7] = new SqlParameter();
        Parameters[7].SqlDbType = SqlDbType.NVarChar;
        Parameters[7].ParameterName = "empname";
        Parameters[7].Value = Session["Admin_Name"];

        Parameters[8] = new SqlParameter();
        Parameters[8].SqlDbType = SqlDbType.Int;
        Parameters[8].ParameterName = "TypeFor";
        Parameters[8].Value = taskfor;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Save_TaskMandoop", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));

        if (dtResult.Rows.Count > 0)
        {
            if (id == 0)
            {
                Int32 mandooptaskid = Convert.ToInt32(dtResult.Rows[0].ItemArray[0].ToString());

                string title = "Task Assigned";
                string Body = "Please check the application for new Task";

                #region DeviceID

                string deviceid = "";

                SqlParameter[] parm = new SqlParameter[2];

                parm[0] = new SqlParameter();
                parm[0].SqlDbType = SqlDbType.Int;
                parm[0].ParameterName = "UserID";
                parm[0].Value = taskfor;

                parm[1] = new SqlParameter();
                parm[1].SqlDbType = SqlDbType.Int;
                parm[1].ParameterName = "TabletRoleID";
                parm[1].Value = 5;

                DataTable userrslt = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TabletUser", parm);
                if (userrslt.Rows.Count > 0)
                {
                    deviceid = userrslt.Rows[0]["DeviceID"].ToString();
                }
                #endregion

                GetMandoopTaskByUserIDDTO mtusid = new GetMandoopTaskByUserIDDTO();

                List<MandoopTask> mtlst = new List<MandoopTask>();

                MandoopTask mt = new MandoopTask();
                mt.id = mandooptaskid.ToString();
                mt.remark = rmk;
                mt.taskforuserid = taskfor.ToString();
                mt.customername = custname;
                mt.datetime = date;
                mt.description = desc;

                #region Images

                List<Images> imglst = new List<Images>();


                SqlParameter[] pm = new SqlParameter[1];

                pm[0] = new SqlParameter();
                pm[0].SqlDbType = SqlDbType.Int;
                pm[0].ParameterName = "TaskSalesID";
                pm[0].Value = mandooptaskid;

                DataTable imgrslt = DataAccess.GetDataTableFromStoredProcedure("sp_Get_ImagesTaskMandoop", pm);
                if (imgrslt.Rows.Count > 0)
                {
                    for (int i = 0; i < imgrslt.Rows.Count; i++)
                    {
                        Images img = new Images();
                        img.imgpath = imgrslt.Rows[i].ItemArray[2].ToString();
                        imglst.Add(img);
                    }


                }


                #endregion

                mt.images = imglst;

                #region Task status

                TaskStatus ts = new TaskStatus();

                SqlParameter[] Para = new SqlParameter[1];

                Para[0] = new SqlParameter();
                Para[0].SqlDbType = SqlDbType.Int;
                Para[0].ParameterName = "ID";
                Para[0].Value = tskstatus;

                DataTable tsrslt = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TaskStatus", Para);
                if (tsrslt.Rows.Count > 0)
                {
                    for (int i = 0; i < tsrslt.Rows.Count; i++)
                    {
                        ts.id = tsrslt.Rows[i].ItemArray[0].ToString();
                        ts.taskstatusname = tsrslt.Rows[i].ItemArray[1].ToString();
                    }
                }

                #endregion

                mt.taskstatus = ts;

                #region Task Type

                TaskTypes tt = new TaskTypes();

                SqlParameter[] prm = new SqlParameter[1];
                prm[0] = new SqlParameter();
                prm[0].SqlDbType = SqlDbType.NVarChar;
                prm[0].ParameterName = "IDs";
                prm[0].Value = typeid;

                DataTable ttrslt = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TaskType", prm);
                if (ttrslt.Rows.Count > 0)
                {
                    for (int i = 0; i < ttrslt.Rows.Count; i++)
                    {
                        tt.id = ttrslt.Rows[i].ItemArray[0].ToString();
                        tt.typename = ttrslt.Rows[i].ItemArray[1].ToString();
                    }
                }
                #endregion

                mt.tasktype = tt;
                mtlst.Add(mt);

                mtusid.mandooptasks = mtlst;

                dynamic dygst = mtusid;


                FireBase.SendPushNotification(deviceid, title, Body, dygst);
            }
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTaskMandoop(int id, int tasktypeid, int taskstatus, string frmdate, string todate)
    {
        string typeids = "";
        int roleid = Convert.ToInt32(Session["Admin_Role"]);

        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "RoleID";
        Parameters[0].Value = roleid;

        if (tasktypeid == 0)
        {
            if (roleid != 1)
            {
                DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TaskTypeRoles", Parameters);
                if (dtResult.Rows.Count > 0)
                {
                    if (dtResult.Rows[0]["AllowDoorType"].ToString() == "True")
                        typeids = typeids + "1,";
                    if (dtResult.Rows[0]["AllowAluminiumType"].ToString() == "True")
                        typeids = typeids + "2,";
                    if (dtResult.Rows[0]["AllowTankType"].ToString() == "True")
                        typeids = typeids + "3,";
                    if (dtResult.Rows[0]["AllowGRPType"].ToString() == "True")
                        typeids = typeids + "4,";
                    if (dtResult.Rows[0]["AllowGFHType"].ToString() == "True")
                        typeids = typeids + "5,";
                }
                else
                {
                    typeids = "-1";
                }
            }
        }
        else
        {
            typeids = tasktypeid.ToString();
        }
        Parameters = new SqlParameter[5];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "TYPEIDs";
        if (typeids == "")
        {
            Parameters[1].Value = "1,2,3,4,5";
        }
        else
        {
            Parameters[1].Value = typeids;
        }

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "TaskStatus";
        Parameters[2].Value = taskstatus;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "FROMDATE";
        if (frmdate == "")
        {
            Parameters[3].Value = "1/1/1900";
        }
        else
        {
            Parameters[3].Value = frmdate;
        }

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "TODATE";
        if (todate == "")
        {
            Parameters[4].Value = "1/1/1900";
        }
        else
        {
            Parameters[4].Value = todate;
        }

        DataTable dtblResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TaskMandoop", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtblResult));
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteTaskMandoop(int id)
    {
        SqlParameter[] Parameters = new SqlParameter[2];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = id;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.NVarChar;
        Parameters[1].ParameterName = "empname";
        Parameters[1].Value = Session["Admin_Name"];

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Delete_TaskMandoop", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetMandoopImages(int taskmandoopid)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "TaskMandoopID";
        Parameters[0].Value = taskmandoopid;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_ImagesTaskMandoop", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }
    #endregion

    #region Reports

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReportDoor(int woid, int quantity, int supid, int techid, string frmdate, string todate)
    {
        SqlParameter[] Parameters = new SqlParameter[6];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "WOID";
        Parameters[0].Value = woid;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "SupervisorID";
        Parameters[2].Value = supid;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.Int;
        Parameters[3].ParameterName = "TechnicianID";
        Parameters[3].Value = techid;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "FROMDATE";
        if (frmdate == "")
        {
            Parameters[4].Value = "1/1/1900";
        }
        else
        {
            Parameters[4].Value = frmdate;
        }

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "TODATE";
        if (todate == "")
        {
            Parameters[5].Value = "1/1/1900";
        }
        else
        {
            Parameters[5].Value = todate;
        }

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_Report_wodoor", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReportAluminium(int woid, int quantity, int supid, int techid, string frmdate, string todate)
    {
        SqlParameter[] Parameters = new SqlParameter[6];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "WOID";
        Parameters[0].Value = woid;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "SupervisorID";
        Parameters[2].Value = supid;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.Int;
        Parameters[3].ParameterName = "TechnicianID";
        Parameters[3].Value = techid;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "FROMDATE";
        if (frmdate == "")
        {
            Parameters[4].Value = "1/1/1900";
        }
        else
        {
            Parameters[4].Value = frmdate;
        }

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "TODATE";
        if (todate == "")
        {
            Parameters[5].Value = "1/1/1900";
        }
        else
        {
            Parameters[5].Value = todate;
        }

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_Report_woaluminium", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReportTank(int woid, int quantity, int supid, int techid, string frmdate, string todate)
    {
        SqlParameter[] Parameters = new SqlParameter[6];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "WOID";
        Parameters[0].Value = woid;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "SupervisorID";
        Parameters[2].Value = supid;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.Int;
        Parameters[3].ParameterName = "TechnicianID";
        Parameters[3].Value = techid;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "FROMDATE";
        if (frmdate == "")
        {
            Parameters[4].Value = "1/1/1900";
        }
        else
        {
            Parameters[4].Value = frmdate;
        }

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "TODATE";
        if (todate == "")
        {
            Parameters[5].Value = "1/1/1900";
        }
        else
        {
            Parameters[5].Value = todate;
        }

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_Report_wotank", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReportGRP(int woid, int quantity, int supid, int techid, string frmdate, string todate)
    {
        SqlParameter[] Parameters = new SqlParameter[6];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "WOID";
        Parameters[0].Value = woid;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "SupervisorID";
        Parameters[2].Value = supid;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.Int;
        Parameters[3].ParameterName = "TechnicianID";
        Parameters[3].Value = techid;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "FROMDATE";
        if (frmdate == "")
        {
            Parameters[4].Value = "1/1/1900";
        }
        else
        {
            Parameters[4].Value = frmdate;
        }

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "TODATE";
        if (todate == "")
        {
            Parameters[5].Value = "1/1/1900";
        }
        else
        {
            Parameters[5].Value = todate;
        }

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_Report_wogrp", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReportGFHL(int woid, int quantity, int supid, int techid, string frmdate, string todate)
    {
        SqlParameter[] Parameters = new SqlParameter[6];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "WOID";
        Parameters[0].Value = woid;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "Quantity";
        Parameters[1].Value = quantity;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "SupervisorID";
        Parameters[2].Value = supid;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.Int;
        Parameters[3].ParameterName = "TechnicianID";
        Parameters[3].Value = techid;

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "FROMDATE";
        if (frmdate == "")
        {
            Parameters[4].Value = "1/1/1900";
        }
        else
        {
            Parameters[4].Value = frmdate;
        }

        Parameters[5] = new SqlParameter();
        Parameters[5].SqlDbType = SqlDbType.NVarChar;
        Parameters[5].ParameterName = "TODATE";
        if (todate == "")
        {
            Parameters[5].Value = "1/1/1900";
        }
        else
        {
            Parameters[5].Value = todate;
        }

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_Report_wogfhl", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReportAttendance(int tracktypeid, int roleid, int userid, string frmdate, string todate)
    {
        SqlParameter[] Parameters = new SqlParameter[5];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "checktype";
        Parameters[0].Value = tracktypeid;

        Parameters[1] = new SqlParameter();
        Parameters[1].SqlDbType = SqlDbType.Int;
        Parameters[1].ParameterName = "tabletroleid";
        Parameters[1].Value = roleid;

        Parameters[2] = new SqlParameter();
        Parameters[2].SqlDbType = SqlDbType.Int;
        Parameters[2].ParameterName = "tabletuserid";
        Parameters[2].Value = userid;

        Parameters[3] = new SqlParameter();
        Parameters[3].SqlDbType = SqlDbType.NVarChar;
        Parameters[3].ParameterName = "FROMDATE";
        if (frmdate == "")
        {
            Parameters[3].Value = "1/1/1900";
        }
        else
        {
            Parameters[3].Value = frmdate;
        }

        Parameters[4] = new SqlParameter();
        Parameters[4].SqlDbType = SqlDbType.NVarChar;
        Parameters[4].ParameterName = "TODATE";
        if (todate == "")
        {
            Parameters[4].Value = "1/1/1900";
        }
        else
        {
            Parameters[4].Value = todate;
        }

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_Report_dailylog", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTracktype(int tracktypeid)
    {
        SqlParameter[] Parameters = new SqlParameter[1];

        Parameters[0] = new SqlParameter();
        Parameters[0].SqlDbType = SqlDbType.Int;
        Parameters[0].ParameterName = "ID";
        Parameters[0].Value = tracktypeid;

        DataTable dtResult = DataAccess.GetDataTableFromStoredProcedure("sp_Get_TrackType", Parameters);
        HttpContext.Current.Response.Write(DataAccess.GetJsonFromDataTable(dtResult));
    }

    #endregion

    public static string GetUniqueAlphaNumericKey(int KeyLength)
    {
        String a = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789abcdefghijklmnopqrstuvwxyz";
        Char[] chars = new Char[(a.Length)];
        chars = a.ToCharArray();
        byte[] data = new byte[(KeyLength)];
        RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
        crypto.GetNonZeroBytes(data);
        StringBuilder result = new StringBuilder(KeyLength);
        foreach (byte b in data)
        {
            result.Append(chars[b % (chars.Length)]);
        }
        return result.ToString();
    }



    #region API class
    public class GetSalesTaskByUserIDDTO
    {
        public List<SalesTask> salestasks { get; set; }
    }
    public class SalesTask
    {
        public string id { get; set; }
        public TaskTypes tasktype { get; set; }
        public string taskforuserid { get; set; }
        public string datetime { get; set; }
        public string customername { get; set; }
        public string description { get; set; }
        public string remark { get; set; }
        public TaskStatus taskstatus { get; set; }
        public List<Images> images { get; set; }
    }
    public class Images
    {
        public string imgpath { get; set; }
    }
    public class TaskStatus
    {
        public string id { get; set; }
        public string taskstatusname { get; set; }
    }
    public class TaskTypes
    {
        public string id { get; set; }
        public string typename { get; set; }
    }

    public class GetMandoopTaskByUserIDDTO
    {
        public List<MandoopTask> mandooptasks { get; set; }
    }

    public class MandoopTask
    {
        public string id { get; set; }
        public TaskTypes tasktype { get; set; }
        public string taskforuserid { get; set; }
        public string datetime { get; set; }
        public string customername { get; set; }
        public string description { get; set; }
        public string remark { get; set; }
        public TaskStatus taskstatus { get; set; }
        public List<Images> images { get; set; }
    }

    #endregion

}
