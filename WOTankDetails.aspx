﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="WOTankDetails.aspx.cs" Inherits="WOTankDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Plugin Css-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

    <link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- Plugins css-->
    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />


    <!-- Mutliple css-->
    <link href="assets/plugins/select2/select2.css" rel="stylesheet" type="text/css" />

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

    <link href="assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">WO Tanks Details</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li>
                                <a href="WOTank.aspx">WO Tanks</a>
                            </li>
                            <li class="active">WO Tanks Details
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="m-b-30">
                                    <button class="btn btn-primary waves-effect waves-light" onclick="backtowopage()">Back</button>
                                </div>
                            </div>
                        </div>

                        <div class="" style='overflow: scroll; overflow-y: hidden;'>
                            <table id="tablelist" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>WO No.</th>
                                        <th>Quantity</th>
                                        <th>Supervisor</th>
                                        <th>ProcessStation</th>
                                        <th>Product</th>
                                        <th>Machine Number</th>
                                        <th>Gallons</th>
                                        <th>Layers</th>
                                        <th>Created Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->

        <!-- MODAL -->
        <div class="modal fade" id="modal-delete-wo" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Confirm</h4>
                    </div>

                    <div class="modal-body">
                        Are you sure, you want to delete current record!
                    </div>

                    <div class="modal-footer">
                        <input type="hidden" id="hidDelID" name="hidDelID" value="0" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info" id="btnDeleteWorkOrder" onclick="DeleteWO()">Continue</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end Modal -->

    </div>
    <!-- sample modal content -->
    <div class="modal fade bs-example-modal-lg" id="modal-AddEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="frmAddEdit" name="frmAddEdit" method="post">
                    <input type="hidden" id="hidEditID" name="hidEditID" value="0" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Work Order Tanks Detail</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">WO No.</label>
                                    <input type="text" class="form-control" readonly="readonly" id="txtwo" name="txtwo" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Quantity</label>
                                    <input type="text" class="form-control" readonly="readonly" onkeypress="return isNumberKey(event)" maxlength="4" id="txtquantity" name="txtquantity" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Products</label>
                                    <select class="selectpicker" disabled="disabled" id="lstproduct" name="lstproduct" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Supervisor</label>
                                    <select class="selectpicker" id="lstsupr" disabled="disabled" name="lstsupr" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Hours</label>
                                    <input type="text" class="form-control" id="txthrs" onkeypress="return allowdotnnumeric(event)" maxlength="5" name="txthrs" placeholder="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Process Station</label>
                                    <input type="text" class="form-control" id="txtprocessstation" readonly="" name="txtprocessstation" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Machine</label>
                                    <input type="text" class="form-control" id="txtmachine" readonly="" name="txtmachine" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Gallons</label>
                                    <input type="text" class="form-control" id="txtgallon" readonly="" name="txtgallon" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Layers</label>
                                    <select id="lstlayer" name="lstlayer" class="selectpicker" data-style="btn-white">
                                        <option value="3">3 Layers</option>
                                        <option value="4">4 Layers</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" style="color: red" class="control-label">Status</label>
                                    <select class="selectpicker" id="lstwost" name="lstwost" data-live-search="true" data-style="btn-white">
                                        <option value="0">Not Completed</option>
                                        <option value="1">Completed</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <h4 class="modal-header" id="lblproduction">Production</h4>

                        <div class="row">
                            <div class="col-md-2" id="dvheatbarrel">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxheatbarrel" type="checkbox" />
                                        <label for="chkbxheatbarrel">
                                            Heat Barrel
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="dvstartmach">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxstartmach" type="checkbox" />
                                        <label for="chkbxstartmach">
                                            Start Machine
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="dvdoprocess">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxdoprocess" type="checkbox" />
                                        <label for="chkbxdoprocess">
                                            Do Process
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="dvoutbags">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxoutbags" type="checkbox" />
                                        <label for="chkbxoutbags">
                                            Output Bags
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="dvrepair">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxrepair" type="checkbox" />
                                        <label for="chkbxrepair">Repair</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="dvsales">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxsales" type="checkbox" />
                                        <label for="chkbxsales">Sales</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h4 class="modal-header" id="lblinstallation">Installation / Delivery</h4>

                        <div class="row">
                            <div class="col-md-2" id="dvdeliveryhouse">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxdeliveryhouse" type="checkbox" />
                                        <label for="chkbxdeliveryhouse">
                                            Delivery House
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="dvdeliveryground">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxdeliveryground" type="checkbox" />
                                        <label for="chkbxdeliveryground">
                                            Delivery Ground
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="dvdeliveryfactory">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxdeliveryfactory" type="checkbox" />
                                        <label for="chkbxdeliveryfactory">
                                            Delivery Factory
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="dvexport">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxexport" type="checkbox" />
                                        <label for="chkbxexport">
                                            Export
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                    <label for="field-7" class="control-label">Note</label>
                                    <textarea class="form-control autogrow" id="txtnote" name="txtnote" placeholder="Write something about Work Order"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <button type="button" class="btn btn-default waves-effect" id="btncloseworkorder" data-dismiss="modal" onclick="resetForm()">Close</button>
                            <button class="btn btn-primary waves-effect waves-light" id="btneditworkorder" type="submit" hidden="hidden">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

    <%--Multiple Sript--%>
    <script src="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script src="assets/plugins/switchery/dist/switchery.min.js"></script>
    <script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
    <script src="assets/plugins/select2/select2.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>



    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/moment.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.js"></script>
    <script src="Scripts/filedownload.js"></script>

    <script>
        var woid = '<%= Request["wo"] %>';

        $(document).ready(function () {
            var resizefunc = [];

            var currentDate = new Date();
            var processstationid = 0, tanktypeid = 0;
            // Date Picker

            $('#txtfrmdate').datetimepicker({ format: 'DD/MM/YYYY' });
            $('#txttodate').datetimepicker({ format: 'DD/MM/YYYY' });
            $('#txtstarttime').datetimepicker({ format: 'LT' });
            $('#txtendtime').datetimepicker({ format: 'LT' });

            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#frmAddEdit").validate({
                rules: {
                    "txtnote": "required",
                    "txtquantity": "required",
                },
                messages: {
                    "txtquantity": "Quantity is required",
                    "txtnote": "Note is required",
                },
                submitHandler: function (form) {
                    if ($("#hidEditID").val() > 0) {
                        EditWO();
                    }
                    else {
                        AddWO();
                    }
                    return false;
                }
            });
            bindProducts();
            bindTabletUsers();
            if (woid != null && woid != "") {
                getHistoryData(woid);
            }
            else {
                getHistoryData(-1);
            }

        });

        function allowdotnnumeric(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /^[0-9.,]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function resetForm() {
            $("#hidEditID").val("0");
            $("#hidDelID").val("0");
            $("#txtprocessstation").val("");
            $("#txtmachine").val("");
            $("#txtwo").val("");
            $("#txtquantity").val("");
            $("#txtgallon").val("");
            $("#txthrs").val("");

            $("#lstlayer").val("");
            $('#lstlayer').selectpicker('refresh');

            $("#txtnote").val("");
            processstationid = 0;
            tanktypeid = 0;
            bindProducts();
            bindTabletUsers();


            $("#chkbxheatbarrel").prop("checked", false);
            $("#chkbxstartmach").prop("checked", false);
            $("#chkbxdoprocess").prop("checked", false);
            $("#chkbxoutbags").prop("checked", false);
            $("#chkbxdeliveryhouse").prop("checked", false);
            $("#chkbxdeliveryground").prop("checked", false);
            $("#chkbxdeliveryfactory").prop("checked", false);
            $("#chkbxexport").prop("checked", false);
            $("#chkbxrepair").prop("checked", false);
            $("#chkbxsales").prop("checked", false);

            var validator = $("#frmAddEdit").validate();
            validator.resetForm();
        }

        function backtowopage() {
            window.location.href = 'WOTank.aspx';
        }

        function getHistoryData(woid) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWOTankDetail',
                data: 'id=0&woid=' + woid,
                async: true,
                success: function (data) {

                    $('#tablelist').dataTable().fnClearTable();
                    $('#tablelist').dataTable().fnDestroy();

                    $.each(data, function (i, item) {
                        var trHTML = '';

                        var date = new Date(parseInt(item.CreatedDate.replace("/Date(", "").replace(")/", ""), 10));
                        var ds = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + ' ' + formatAMPM(date);

                        trHTML += '<tr><td>' + item.WOID + '</td><td>' + item.Quantity + '</td><td>' + item.SupervisorName + '</td><td>' + item.ProcessStationName + '</td><td>' + item.ProductName + '</td><td>' + item.MachineNumber + '</td><td>' + item.Gallons + '</td><td>' + item.Layers + '</td><td>' + ds + '</td><td>' +
                            '<button type="button" class="btn btn-info waves-effect btnEdit" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="FillWO(' + item.ID + ')"><i class="fa fa-pencil-square-o"></i></button>'
                            //+ '<button class="btn btn-danger waves-effect waves-light btnDelete" onclick="confirmWODelete(' + item.ID + ')"><i class="fa fa-times"></i></button>'
                            + '</td></tr>';
                        $('#tablelist').append(trHTML);
                    });
                    $('#tablelist').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "bSort": false
                        //order: [[0, 'desc']]
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function FillWO(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWOTankDetail',
                data: 'id=' + id + '&woid=0',
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        $("#hidEditID").val(id);
                        $("#txtwo").val(item.WOID);
                        FillWOSteps(item.WOID);
                        $("#txtquantity").val(item.Quantity);
                        $("#lstproduct").val(item.ProductName);
                        $('#lstproduct').selectpicker('refresh');
                        $("#txtmachine").val(item.MachineNumber);
                        $("#txtgallon").val(item.Gallons);
                        $("#txtnote").val(item.Notes);
                        $("#txthrs").val(item.Hours);
                        $("#txtprocessstation").val(item.ProcessStationName);
                        $("#lstlayer").val(item.Layers);
                        $('#lstlayer').selectpicker('refresh');

                        $('#lstsupr').val(item.SupervisorID);
                        $('#lstsupr').selectpicker('refresh');

                        $('#lstwost').val(item.isCompleted == 0 ? 0 : 1);
                        $('#lstwost').selectpicker('refresh');

                        processstationid = item.ProcessStationID;
                        tanktypeid = item.TankTypeID;

                        if (item.isHeatBarrel == 0 ? $("#chkbxheatbarrel").prop("checked", false) : $("#chkbxheatbarrel").prop("checked", true));
                        if (item.isStartMachine == 0 ? $("#chkbxstartmach").prop("checked", false) : $("#chkbxstartmach").prop("checked", true));
                        if (item.isDoProcess == 0 ? $("#chkbxdoprocess").prop("checked", false) : $("#chkbxdoprocess").prop("checked", true));
                        if (item.isOutput == 0 ? $("#chkbxoutbags").prop("checked", false) : $("#chkbxoutbags").prop("checked", true));
                        if (item.isDeliveryInHouse == 0 ? $("#chkbxdeliveryhouse").prop("checked", false) : $("#chkbxdeliveryhouse").prop("checked", true));
                        if (item.isDeliveryInGround == 0 ? $("#chkbxdeliveryground").prop("checked", false) : $("#chkbxdeliveryground").prop("checked", true));
                        if (item.isDeliveryInFactory == 0 ? $("#chkbxdeliveryfactory").prop("checked", false) : $("#chkbxdeliveryfactory").prop("checked", true));
                        if (item.isExport == 0 ? $("#chkbxexport").prop("checked", false) : $("#chkbxexport").prop("checked", true));
                        if (item.isRepair == 0 ? $("#chkbxrepair").prop("checked", false) : $("#chkbxrepair").prop("checked", true));
                        if (item.isSales == 0 ? $("#chkbxsales").prop("checked", false) : $("#chkbxsales").prop("checked", true));
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function FillWOSteps(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWOTanks',
                data: 'id=' + id,
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        if (item.isHeatBerral == 0 ? $("#dvheatbarrel").hide() : $("#dvheatbarrel").show());
                        if (item.isStartMachine == 0 ? $("#dvstartmach").hide() : $("#dvstartmach").show());
                        if (item.isDoProcess == 0 ? $("#dvdoprocess").hide() : $("#dvdoprocess").show());
                        if (item.isOutput == 0 ? $("#dvoutbags").hide() : $("#dvoutbags").show());
                        if (item.isDeliveryInHouse == 0 ? $("#dvdeliveryhouse").hide() : $("#dvdeliveryhouse").show());
                        if (item.isDeliveryInGround == 0 ? $("#dvdeliveryground").hide() : $("#dvdeliveryground").show());
                        if (item.isDeliveryInFactory == 0 ? $("#dvdeliveryfactory").hide() : $("#dvdeliveryfactory").show());
                        if (item.isExport == 0 ? $("#dvexport").hide() : $("#dvexport").show());
                        if (item.isRepair == 0 ? $("#dvrepair").hide() : $("#dvrepair").show());
                        if (item.isSales == 0 ? $("#dvsales").hide() : $("#dvsales").show());
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function AddWO() {
            var isheatberral = $(chkbxheatbarrel).prop("checked") == false ? "0" : "1";
            var isstartmach = $(chkbxstartmach).prop("checked") == false ? "0" : "1";
            var isdoprocess = $(chkbxdoprocess).prop("checked") == false ? "0" : "1";
            var isoutbags = $(chkbxoutbags).prop("checked") == false ? "0" : "1";
            var isdeliveryhouse = $(chkbxdeliveryhouse).prop("checked") == false ? "0" : "1";
            var isdeliveryground = $(chkbxdeliveryground).prop("checked") == false ? "0" : "1";
            var isdeliveryfactory = $(chkbxdeliveryfactory).prop("checked") == false ? "0" : "1";
            var isexport = $(chkbxexport).prop("checked") == false ? "0" : "1";
            var isrepair = $(chkbxrepair).prop("checked") == false ? "0" : "1";
            var issales = $(chkbxsales).prop("checked") == false ? "0" : "1";

            //var data2 = 'id=0&quantity=' + $("#txtquantity").val() + '&processstaionID=' + processstationid + '&productname=' + $("#lstproduct").val() + '&machinenumber=' + $("#txtmachine").val() + '&gallons=' + $("#txtgallon").val() + '&layers=' + $("#lstlayer").val() + '&isheatbarrel=' + isheatberral + '&isstartmach=' + isstartmach + '&isdoprocess=' + isdoprocess + '&isoutput=' + isoutbags + '&isframeinside=' + isframeinside + '&isframeoutside=' + isframeoutside + '&istanks=' + istank + '&notes=' + $("#txtnote").val();
            //alert(data2);

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditWOTanksDetail',
                data: 'id=0&quantity=' + $("#txtquantity").val() + '&processstaionID=' + processstationid + '&productname=' + $("#lstproduct").val() + '&machinenumber='
                + $("#txtmachine").val() + '&gallons=' + $("#txtgallon").val() + '&layers=' + $("#lstlayer").val() + '&isheatbarrel=' + isheatberral + '&isstartmach='
                + isstartmach + '&isdoprocess=' + isdoprocess + '&isoutput=' + isoutbags + '&isdeliveryhouse=' + isdeliveryhouse + '&isdeliveryground='
                + isdeliveryground + '&isdeliveryfactory=' + isdeliveryfactory + '&isexport=' + isexport + '&notes=' + $("#txtnote").val()
                + '&isrepair=' + isrepair + '&issales=' + issales + '&supervisorid=' + $("#lstsupr").val() + '&hours=' + $("#txthrs").val() + '&tanktypeid=' + tanktypeid
                 + '&woid=' + $("#txtwo").val() + '&iscompleted=' + $("#lstwost").val(),
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "Work Order has been saved successfully");
                        resetForm();
                        getSearchWorkOrder();
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function EditWO() {
            var id = $("#hidEditID").val();

            var isheatberral = $(chkbxheatbarrel).prop("checked") == false ? "0" : "1";
            var isstartmach = $(chkbxstartmach).prop("checked") == false ? "0" : "1";
            var isdoprocess = $(chkbxdoprocess).prop("checked") == false ? "0" : "1";
            var isoutbags = $(chkbxoutbags).prop("checked") == false ? "0" : "1";
            var isdeliveryhouse = $(chkbxdeliveryhouse).prop("checked") == false ? "0" : "1";
            var isdeliveryground = $(chkbxdeliveryground).prop("checked") == false ? "0" : "1";
            var isdeliveryfactory = $(chkbxdeliveryfactory).prop("checked") == false ? "0" : "1";
            var isexport = $(chkbxexport).prop("checked") == false ? "0" : "1";
            var isrepair = $(chkbxrepair).prop("checked") == false ? "0" : "1";
            var issales = $(chkbxsales).prop("checked") == false ? "0" : "1";

            //data2 = 'id=' + id + '&quantity=' + $("#txtquantity").val() + '&processstaionID=' + processstationid + '&productname=' + $("#lstproduct").val() + '&machinenumber='
            //         + $("#txtmachine").val() + '&gallons=' + $("#txtgallon").val() + '&layers=' + $("#lstlayer").val() + '&isheatbarrel=' + isheatberral + '&isstartmach='
            //         + isstartmach + '&isdoprocess=' + isdoprocess + '&isoutput=' + isoutbags + '&isdeliveryhouse=' + isdeliveryhouse + '&isdeliveryground='
            //         + isdeliveryground + '&isdeliveryfactory=' + isdeliveryfactory + '&isexport=' + isexport + '&notes=' + $("#txtnote").val()
            //         + '&isrepair=' + isrepair + '&issales=' + issales;

            //alert(data2);

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditWOTanksDetail',
                data: 'id=' + id + '&quantity=' + $("#txtquantity").val() + '&processstaionID=' + processstationid + '&productname=' + $("#lstproduct").val() + '&machinenumber='
                + $("#txtmachine").val() + '&gallons=' + $("#txtgallon").val() + '&layers=' + $("#lstlayer").val() + '&isheatbarrel=' + isheatberral + '&isstartmach='
                + isstartmach + '&isdoprocess=' + isdoprocess + '&isoutput=' + isoutbags + '&isdeliveryhouse=' + isdeliveryhouse + '&isdeliveryground='
                + isdeliveryground + '&isdeliveryfactory=' + isdeliveryfactory + '&isexport=' + isexport + '&notes=' + $("#txtnote").val()
                + '&isrepair=' + isrepair + '&issales=' + issales + '&supervisorid=' + $("#lstsupr").val() + '&hours=' + $("#txthrs").val() + '&tanktypeid=' + tanktypeid
                 + '&woid=' + $("#txtwo").val() + '&iscompleted=' + $("#lstwost").val(),
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "Work Order has been updated successfully");
                        resetForm();
                        getHistoryData(woid);
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function formatAMPM(date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        }

        function confirmWODelete(id) {
            $("#hidDelID").val(id);
            jQuery('#modal-delete-wo').modal('show', { backdrop: 'static' });
        }

        function DeleteWO() {
            var id = $('input#hidDelID').val();

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/DeleteWOTanks',
                data: "id=" + id,
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('warning', 'top right', 'Deleted', 'Work Order Tanks deleted successfully.');
                        getSearchWorkOrder();
                        $('#modal-delete-wo').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function bindProducts() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTankTypes',
                data: 'id=0&productname=',
                async: true,
                success: function (data) {
                    $("#lstproduct").append("").val("").html("");
                    //$("#lstproduct").append($("<option></option>").val("-1").html("Select"));

                    $("#lstsearchproduct").append("").val("").html("");
                    $("#lstsearchproduct").append($("<option></option>").val("-1").html("Select"));

                    $.each(data, function (key, val) {
                        $("#lstproduct").append($("<option></option>").val(val.ProductName).html(val.ProductName));

                        $("#lstsearchproduct").append($("<option></option>").val(val.ProductName).html(val.ProductName));
                    });
                    $('#lstproduct').selectpicker('refresh');

                    $('#lstsearchproduct').selectpicker('refresh');
                    getproductinfo($('#lstproduct').val());

                }
            });
        }

        $('#lstproduct').change(function () {
            getproductinfo($('#lstproduct').val());
        });

        function getproductinfo(pname) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTankTypes',
                data: 'id=0&productname=' + pname,
                async: true,
                success: function (data) {
                    $("#txtprocessstation").val("");
                    $("#txtmachine").val("");

                    $.each(data, function (key, val) {
                        $("#txtprocessstation").val(val.ProcessStationName);
                        $("#txtmachine").val(val.MachineNumber);
                        $("#txtgallon").val(val.Gallon);
                        processstationid = val.ProcessStationID;
                        tanktypeid = val.ID;
                    });
                }
            });
        }

        function bindTabletUsers() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTabletUsers',
                data: "user=0&tabletroleid=1",
                async: true,
                success: function (data) {
                    $("#lstsupr").append("").val("").html("");

                    $.each(data, function (key, val) {
                        $("#lstsupr").append($("<option></option>").val(val.ID).html(val.Name));
                    });
                    $('#lstsupr').selectpicker('refresh');
                }
            });
        }

    </script>
</asp:Content>



