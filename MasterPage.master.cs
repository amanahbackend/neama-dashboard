﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    public string viwPermission = "none", addPermission = "none", edtPermission = "none", delPermission = "none";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Admin_ID"] == null)
        {
            Response.Redirect("Login.aspx");
        }

        string pageName = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
        if (Request["type"] != null)
        {
            pageName += "?type=" + Request["type"];
        }

        if (Session["Admin_ID"].ToString() == "1")
        {
            viwPermission = "display";
            addPermission = "display";
            edtPermission = "display";
            delPermission = "display";
        }
        else
        {
            DataTable dtPermissions = (DataTable)Session["Admin_Permissions"];
            DataRow[] rows = dtPermissions.Select("TB_PAGE_NAME='" + pageName + "'");
            foreach (DataRow row in rows)
            {
                viwPermission = "display";
                if (row["TB_TYPE_ID"].ToString() == "1" && row["TB_ALLOW"].ToString() == "True")
                {
                    addPermission = "inline-block";
                }
                if (row["TB_TYPE_ID"].ToString() == "2" && row["TB_ALLOW"].ToString() == "True")
                {
                    edtPermission = "inline-block";
                }
                if (row["TB_TYPE_ID"].ToString() == "3" && row["TB_ALLOW"].ToString() == "True")
                {
                    delPermission = "inline-block";
                }
                //if (row["TB_TYPE_ID"].ToString() == "4" && row["TB_ALLOW"].ToString() == "True")
                //{
                //    delPermission = "inline-block";
                //}
            }

            //if (viwPermission == "none" || rows.Count() == 0)
            //{
            //    Response.Redirect("Default.aspx");
            //}
        }
    }
}
