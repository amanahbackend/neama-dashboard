﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="WorkOrderDoorDetails.aspx.cs" Inherits="WorkOrderDoorDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Plugin Css-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

    <link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- Plugins css-->
    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />

    <link href="assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">WorkOrder Door Details</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li>
                                <a href="WorkOrder.aspx">WorkOrder Door</a>
                            </li>
                            <li class="active">WorkOrder Door Details
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="m-b-30">
                                    <button class="btn btn-primary waves-effect waves-light" onclick="backtowopage()">Back</button>
                                </div>
                            </div>
                        </div>

                        <div class="" style='overflow: scroll; overflow-y: hidden;'>
                            <table id="tablelist" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>WO No.</th>
                                        <th>Quantity</th>
                                        <th>Supervisor</th>
                                        <th>Serial No.</th>
                                        <th>Door Type</th>
                                        <th>Width</th>
                                        <th>Height</th>
                                        <th>Color</th>
                                        <th>Created Date</th>
                                        <th style="width: 100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->

        <!-- MODAL -->
        <div class="modal fade" id="modal-delete-wo" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Confirm</h4>
                    </div>

                    <div class="modal-body">
                        Are you sure, you want to delete current record!
                    </div>

                    <div class="modal-footer">
                        <input type="hidden" id="hidDelID" name="hidDelID" value="0" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info" id="btnDeleteWorkOrder" onclick="DeleteWorkOrder()">Continue</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end Modal -->

    </div>
    <!-- sample modal content -->
    <div class="modal fade bs-example-modal-lg" id="modal-AddEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="frmAddEdit" name="frmAddEdit" method="post">
                    <input type="hidden" id="hidEditID" name="hidEditID" value="0" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Work Order Door Detail</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">WO No.</label>
                                    <input type="text" class="form-control" readonly="readonly" id="txtwo" name="txtwo" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Quantity</label>
                                    <input type="text" class="form-control" readonly="readonly" id="txtquantity" onkeypress="return isNumberKey(event)" maxlength="4" name="txtquantity" placeholder="" />
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Door Types</label>
                                    <select class="selectpicker" disabled="disabled" id="lstdoortype" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Supervisor</label>
                                    <select class="selectpicker" disabled="disabled" id="lstsupr" name="lstsupr" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Hours</label>
                                    <input type="text" class="form-control" id="txthrs" onkeypress="return allowdotnnumeric(event)" readonly="" maxlength="10" name="txthrs" placeholder="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Serial No.  </label>
                                    <input type="text" class="form-control" id="txtserialno" readonly="readonly" name="txtserialno" placeholder="" />
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Width</label>
                                    <input type="text" class="form-control" id="txtwidth" maxlength="8" name="txtwidth" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Height</label>
                                    <input type="text" class="form-control" id="txtheight" maxlength="8" name="txtheight" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Color</label>
                                    <input type="text" class="form-control" id="txtcolor" name="txtcolor" placeholder="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Wall Width</label>
                                    <input type="text" class="form-control" id="txtwallwidth" maxlength="25" name="txtwallwidth" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" style="color: red" class="control-label">Status</label>
                                    <select class="selectpicker" id="lstwoddst" name="lstwoddst" data-live-search="true" data-style="btn-white">
                                        <option value="0">Not Completed</option>
                                        <option value="1">Completed</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h4 class="modal-header" id="lblproduction">Production</h4>

                        <div class="row">
                            <div class="col-md-2" id="dvcollection">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxcollection" type="checkbox" />
                                        <label for="chkbxcollection">
                                            Collection
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-1" id="dvfoom">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxfoom" type="checkbox" />
                                        <label for="chkbxfoom">
                                            Foom
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="dvcfglass">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxcfglass" type="checkbox" />
                                        <label for="chkbxcfglass">
                                            Cutting for Glass
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="dvcleaning">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxcleaning" type="checkbox" />
                                        <label for="chkbxcleaning">
                                            Cleaning
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="dvglassfitting">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxglassfitting" type="checkbox" />
                                        <label for="chkbxglassfitting">
                                            Glass Fitting
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="dvpainting">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxpainting" type="checkbox" />
                                        <label for="chkbxpainting">
                                            Painting
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2" id="dvrepair">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxrepair" type="checkbox" />
                                        <label for="chkbxrepair">Repair</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="dvsales">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxsales" type="checkbox" />
                                        <label for="chkbxsales">Sales</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="modal-header" id="lblinstallation">Installation / Delivery</h4>

                        <div class="row">
                            <div class="col-md-2" id="dvframeinside">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxframeinside" type="checkbox" />
                                        <label for="chkbxframeinside">
                                            Frame Inside
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="dvframeoutside">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxframeoutside" type="checkbox" />
                                        <label for="chkbxframeoutside">
                                            FrameOutside
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="dvdoor">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxdoor" type="checkbox" />
                                        <label for="chkbxdoor">
                                            Door
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                    <label for="field-7" class="control-label">Note</label>
                                    <textarea class="form-control autogrow" id="txtnote" name="txtnote" placeholder="Write something about Work Order"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <button type="button" class="btn btn-default waves-effect" id="btncloseworkorder" data-dismiss="modal" onclick="resetForm()">Close</button>
                            <button class="btn btn-primary waves-effect waves-light" id="btneditworkorder" type="submit" hidden="hidden">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>

    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/moment.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.js"></script>
    <script src="Scripts/filedownload.js"></script>

    <script>

        $(document).ready(function () {
            var resizefunc = [];
            var currentDate = new Date();


            $("#frmAddEdit").validate({
                rules: {
                    "txtserialno": "required",
                },
                messages: {
                    "txtserialno": "Serial Number is required",
                },
                submitHandler: function (form) {
                    if ($("#hidEditID").val() > 0) {
                        EditWO();
                    }
                    else {
                        AddWO();
                    }
                    return false;
                }
            });

            var woid = '<%= Request["wo"] %>';
            if (woid != null && woid != "") {
                getHistoryData(woid);
            }
            else {
                getHistoryData(-1);
            }

            binddoortypes();
            bindTabletUsers();
        });

        function resetForm() {
            $("#hidEditID").val("0");
            $("#hidDelID").val("0");
            $("#txtserialno").val("");
            $("#txtquantity").val("");
            $("#txtwo").val("");
            $("#txtwidth").val("");
            $("#txtheight").val("");
            $("#txtcolor").val("");
            $("#txtnote").val("");
            $("#txthrs").val("");
            $("#txtwallwidth").val("");
            binddoortypes();
            bindTabletUsers();

            $("#chkbxcollection").prop("checked", false);
            $("#chkbxfoom").prop("checked", false);
            $("#chkbxcfglass").prop("checked", false);
            $("#chkbxcleaning").prop("checked", false);
            $("#chkbxglassfitting").prop("checked", false);
            $("#chkbxpainting").prop("checked", false);
            $("#chkbxframeinside").prop("checked", false);
            $("#chkbxframeoutside").prop("checked", false);
            $("#chkbxdoor").prop("checked", false);
            $("#chkbxrepair").prop("checked", false);
            $("#chkbxsales").prop("checked", false);

            var validator = $("#frmAddEdit").validate();
            validator.resetForm();
        }

        function allowdotnnumeric(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /^[0-9.,]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function backtowopage() {
            window.location.href = 'WorkOrder.aspx';
        }

        function getHistoryData(woid) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWODoorDetails',
                data: 'id=0&woid=' + woid,
                async: true,
                success: function (data) {

                    $('#tablelist').dataTable().fnClearTable();
                    $('#tablelist').dataTable().fnDestroy();

                    $.each(data, function (i, item) {
                        var trHTML = '';

                        var date = new Date(parseInt(item.CreatedDate.replace("/Date(", "").replace(")/", ""), 10));
                        var ds = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + ' ' + formatAMPM(date);

                        trHTML += '<tr><td>' + item.WOID + '</td><td>' + item.Quantity + '</td><td>' + item.SupervisorName + '</td><td>' + item.SerialNumber + '</td><td>' + item.DoorTypeName + '</td><td>' + item.Width + '</td><td>' + item.Height + '</td><td>' + item.Color + '</td><td>' + ds + '</td><td>' +
                            '<button type="button" class="btn btn-info waves-effect btnEdit" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="FillWO(' + item.ID + ')"><i class="fa fa-pencil-square-o"></i></button>'
                            //+ '<button class="btn btn-danger waves-effect waves-light btnDelete" onclick="confirmWODelete(' + item.ID + ')"><i class="fa fa-times"></i></button>'
                            + '</td></tr>';
                        $('#tablelist').append(trHTML);
                    });
                    $('#tablelist').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "bSort": false
                        //order: [[0, 'desc']]
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function FillWO(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWODoorDetails',
                data: 'id=' + id + '&woid=0',
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        $("#hidEditID").val(id);
                        $("#txtserialno").val(item.SerialNumber);
                        $("#txtquantity").val(item.Quantity);
                        $("#txtwo").val(item.WOID);
                        FillWOSteps(item.WOID);
                        $("#txtwidth").val(item.Width);
                        $("#txtheight").val(item.Height);
                        $("#txtcolor").val(item.Color);
                        $("#txtnote").val(item.Notes);
                        $("#txtwallwidth").val(item.WallWidth);
                        $("#txthrs").val(item.Hours);

                        $('#lstsupr').val(item.SupervisorID);
                        $('#lstsupr').selectpicker('refresh');

                        $('#lstwoddst').val(item.isCompleted == 0 ? 0 : 1);
                        $('#lstwoddst').selectpicker('refresh');

                        $('#lstdoortype').val(item.DoorTypeID);
                        $('#lstdoortype').selectpicker('refresh');
                        if (item.IsCollection == 0 ? $("#chkbxcollection").prop("checked", false) : $("#chkbxcollection").prop("checked", true));
                        if (item.IsFoom == 0 ? $("#chkbxfoom").prop("checked", false) : $("#chkbxfoom").prop("checked", true));
                        if (item.IsCuttingForGlass == 0 ? $("#chkbxcfglass").prop("checked", false) : $("#chkbxcfglass").prop("checked", true));
                        if (item.IsCleaning == 0 ? $("#chkbxcleaning").prop("checked", false) : $("#chkbxcleaning").prop("checked", true));
                        if (item.IsGlassFitting == 0 ? $("#chkbxglassfitting").prop("checked", false) : $("#chkbxglassfitting").prop("checked", true));
                        if (item.IsPainting == 0 ? $("#chkbxpainting").prop("checked", false) : $("#chkbxpainting").prop("checked", true));
                        if (item.IsFrameInside == 0 ? $("#chkbxframeinside").prop("checked", false) : $("#chkbxframeinside").prop("checked", true));
                        if (item.IsFrameOutside == 0 ? $("#chkbxframeoutside").prop("checked", false) : $("#chkbxframeoutside").prop("checked", true));
                        if (item.IsDoor == 0 ? $("#chkbxdoor").prop("checked", false) : $("#chkbxdoor").prop("checked", true));
                        if (item.isRepair == 0 ? $("#chkbxrepair").prop("checked", false) : $("#chkbxrepair").prop("checked", true));
                        if (item.isSales == 0 ? $("#chkbxsales").prop("checked", false) : $("#chkbxsales").prop("checked", true));
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function FillWOSteps(id) {
            debugger;
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWOs',
                data: 'id=' + id,
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        if (item.IsCollection == 0 ? $("#dvcollection").hide() : $("#dvcollection").show());
                        if (item.IsFoom == 0 ? $("#dvfoom").hide() : $("#dvfoom").show());
                        if (item.IsCuttingForGlass == 0 ? $("#dvcfglass").hide() : $("#dvcfglass").show());
                        if (item.IsCleaning == 0 ? $("#dvcleaning").hide() : $("#dvcleaning").show());
                        if (item.IsGlassFitting == 0 ? $("#dvglassfitting").hide() : $("#dvglassfitting").show());
                        if (item.IsPainting == 0 ? $("#dvpainting").hide() : $("#dvpainting").show());
                        if (item.IsFrameInside == 0 ? $("#dvframeinside").hide() : $("#dvframeinside").show());
                        if (item.IsFrameOutside == 0 ? $("#dvframeoutside").hide() : $("#dvframeoutside").show());
                        if (item.IsDoor == 0 ? $("#dvdoor").hide() : $("#dvdoor").show());
                        if (item.isRepair == 0 ? $("#dvrepair").hide() : $("#dvrepair").show());
                        if (item.isSales == 0 ? $("#dvsales").hide() : $("#dvsales").show());
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function AddWO() {
            var iscollection = $(chkbxcollection).prop("checked") == false ? "0" : "1";
            var isfoom = $(chkbxfoom).prop("checked") == false ? "0" : "1";
            var iscuttingforglass = $(chkbxcfglass).prop("checked") == false ? "0" : "1";
            var iscleaning = $(chkbxcleaning).prop("checked") == false ? "0" : "1";
            var isglassfitting = $(chkbxglassfitting).prop("checked") == false ? "0" : "1";
            var ispainting = $(chkbxpainting).prop("checked") == false ? "0" : "1";
            var isframeinside = $(chkbxframeinside).prop("checked") == false ? "0" : "1";
            var isframeoutside = $(chkbxframeoutside).prop("checked") == false ? "0" : "1";
            var isdoor = $(chkbxdoor).prop("checked") == false ? "0" : "1";
            var isrepair = $(chkbxrepair).prop("checked") == false ? "0" : "1";
            var issales = $(chkbxsales).prop("checked") == false ? "0" : "1";

            //var dat2a = 'id=0&quantity=' + $("#txtquantity").val() + '&serialno=' + $("#txtserialno").val() + '&width=' + $("#txtwidth").val() + '&height=' + $("#txtheight").val() + '&color=' + $("#txtcolor").val() + '&doortypeid=' + $("#lstdoortype").val() + '&iscollection=' + iscollection + '&isfoom=' + isfoom + '&iscuttingforglass=' + iscuttingforglass + '&iscleaning=' + iscleaning + '&isglassfitting=' + isglassfitting + '&ispainting=' + ispainting + '&isframeinside=' + isframeinside + '&isframeoutside=' + isframeoutside + '&isdoor=' + isdoor + '&notes=' + $("#txtnote").val();
            //alert(dat2a);
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditWODetail',
                data: 'id=0&quantity=' + $("#txtquantity").val() + '&serialno=' + $("#txtserialno").val() + '&width=' + $("#txtwidth").val()
                + '&height=' + $("#txtheight").val() + '&color=' + $("#txtcolor").val() + '&wallwidth=' + $("#txtwallwidth").val() + '&doortypeid=' + $("#lstdoortype").val()
                + '&iscollection=' + iscollection + '&isfoom=' + isfoom + '&iscuttingforglass=' + iscuttingforglass + '&iscleaning=' + iscleaning + '&isglassfitting=' + isglassfitting
                + '&ispainting=' + ispainting + '&isframeinside=' + isframeinside + '&isframeoutside=' + isframeoutside + '&isdoor=' + isdoor + '&isrepair=' + isrepair
                + '&issales=' + issales + '&notes=' + $("#txtnote").val() + '&supervisorid=' + $("#lstsupr").val() + '&hours=' + $("#txthrs").val()
                + '&woid=' + $("#txtwo").val() + '&iscompleted=' + $("#lstwoddst").val(),
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "Work Order has been saved successfully");
                        resetForm();
                        getHistoryData();
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function EditWO() {
            var id = $("#hidEditID").val();

            var iscollection = $(chkbxcollection).prop("checked") == false ? "0" : "1";
            var isfoom = $(chkbxfoom).prop("checked") == false ? "0" : "1";
            var iscuttingforglass = $(chkbxcfglass).prop("checked") == false ? "0" : "1";
            var iscleaning = $(chkbxcleaning).prop("checked") == false ? "0" : "1";
            var isglassfitting = $(chkbxglassfitting).prop("checked") == false ? "0" : "1";
            var ispainting = $(chkbxpainting).prop("checked") == false ? "0" : "1";
            var isframeinside = $(chkbxframeinside).prop("checked") == false ? "0" : "1";
            var isframeoutside = $(chkbxframeoutside).prop("checked") == false ? "0" : "1";
            var isdoor = $(chkbxdoor).prop("checked") == false ? "0" : "1";
            var isrepair = $(chkbxrepair).prop("checked") == false ? "0" : "1";
            var issales = $(chkbxsales).prop("checked") == false ? "0" : "1";

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditWODetail',
                data: 'id=' + id + '&quantity=' + $("#txtquantity").val() + '&serialno=' + $("#txtserialno").val() + '&width=' + $("#txtwidth").val()
                + '&height=' + $("#txtheight").val() + '&color=' + $("#txtcolor").val() + '&wallwidth=' + $("#txtwallwidth").val() + '&doortypeid=' + $("#lstdoortype").val()
                + '&iscollection=' + iscollection + '&isfoom=' + isfoom + '&iscuttingforglass=' + iscuttingforglass + '&iscleaning=' + iscleaning
                + '&isglassfitting=' + isglassfitting + '&ispainting=' + ispainting + '&isframeinside=' + isframeinside + '&isframeoutside=' + isframeoutside + '&isdoor=' + isdoor
                + '&isrepair=' + isrepair + '&issales=' + issales + '&notes=' + $("#txtnote").val() + '&supervisorid=' + $("#lstsupr").val() + '&hours=' + $("#txthrs").val()
                 + '&woid=' + $("#txtwo").val() + '&iscompleted=' + $("#lstwoddst").val(),
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "Work Order has been updated successfully");
                        resetForm();
                        getHistoryData();
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function formatAMPM(date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        }

        function confirmWODelete(id) {
            $("#hidDelID").val(id);
            jQuery('#modal-delete-wo').modal('show', { backdrop: 'static' });
        }

        function DeleteWO() {
            var id = $('input#hidDelID').val();

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/DeleteWO',
                data: "id=" + id,
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('warning', 'top right', 'Deleted', 'Work Order deleted successfully.');
                        getHistoryData();
                        $('#modal-delete-doortype').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function binddoortypes() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetDoorTypes',
                data: 'id=0',
                async: true,
                success: function (data) {
                    $("#lstdoortype").append("").val("").html("");
                    $("#lstdoortype").append($("<option></option>").val("-1").html("Select"));

                    $("#lstsearchdtypes").append("").val("").html("");
                    $("#lstsearchdtypes").append($("<option></option>").val("-1").html("Select"));

                    $.each(data, function (key, val) {
                        $("#lstdoortype").append($("<option></option>").val(val.ID).html(val.DoorTypeName));

                        $("#lstsearchdtypes").append($("<option></option>").val(val.ID).html(val.DoorTypeName));
                    });
                    $('#lstdoortype').selectpicker('refresh');

                    $('#lstsearchdtypes').selectpicker('refresh');
                }
            });
        }

        $('#lstdoortype').change(function () {
            getserialno($('#lstdoortype').val());
        });

        function getserialno(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetDoorTypes',
                data: 'id=' + id,
                async: true,
                success: function (data) {
                    $("#txtserialno").val("");
                    $.each(data, function (key, val) {
                        $("#txtserialno").val(val.SerialNumber);
                    });
                }
            });
        }

        function bindTabletUsers() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTabletUsers',
                data: "user=0&tabletroleid=1",
                async: true,
                success: function (data) {
                    $("#lstsupr").append("").val("").html("");

                    $.each(data, function (key, val) {
                        $("#lstsupr").append($("<option></option>").val(val.ID).html(val.Name));
                    });
                    $('#lstsupr').selectpicker('refresh');
                }
            });
        }

    </script>
</asp:Content>



