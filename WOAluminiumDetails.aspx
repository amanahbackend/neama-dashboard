﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="WOAluminiumDetails.aspx.cs" Inherits="WOAluminiumDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Plugin Css-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

    <link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- Plugins css-->
    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />

    <link href="assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">WO Aluminium Details</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li>
                                <a href="WOAluminium.aspx">WO Aluminium</a>
                            </li>
                            <li class="active">WO Aluminium Details
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="m-b-30">
                                    <button class="btn btn-primary waves-effect waves-light" onclick="backtowopage()">Back</button>
                                </div>
                            </div>
                        </div>
                        <div class="" style='overflow: scroll; overflow-y: hidden;'>
                            <table id="tablelist" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>WO No.</th>
                                        <th>Quantity</th>
                                        <th>Supervisor</th>
                                        <th>Serial No.</th>
                                        <th>Aluminium Type</th>
                                        <th>Width</th>
                                        <th>Height</th>
                                        <th>Color</th>
                                        <th>Thickness</th>
                                        <th>Created Date</th>
                                        <th style="width: 100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->

        <!-- MODAL -->
        <div class="modal fade" id="modal-delete-wo" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Confirm</h4>
                    </div>

                    <div class="modal-body">
                        Are you sure, you want to delete current record!
                    </div>

                    <div class="modal-footer">
                        <input type="hidden" id="hidDelID" name="hidDelID" value="0" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info" id="btnDeleteWorkOrder" onclick="DeleteWO()">Continue</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end Modal -->

    </div>
    <!-- sample modal content -->
    <div class="modal fade bs-example-modal-lg" id="modal-AddEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="frmAddEdit" name="frmAddEdit" method="post">
                    <input type="hidden" id="hidEditID" name="hidEditID" value="0" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Work Order Aluminium Detail</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">WO No.</label>
                                    <input type="text" class="form-control" readonly="readonly" id="txtwo" name="txtwo" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Quantity</label>
                                    <input type="text" class="form-control" readonly="readonly" onkeypress="return isNumberKey(event)" maxlength="4" id="txtquantity" name="txtquantity" placeholder="" />
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Aluminium Types</label>
                                    <select class="selectpicker" disabled="disabled" id="lstaluminiumtype" name="lstaluminiumtype" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Serial No.  </label>
                                    <input type="text" class="form-control" id="txtserialno" readonly="" name="txtserialno" placeholder="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Supervisor</label>
                                    <select class="selectpicker" id="lstsupr" disabled="disabled" name="lstsupr" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Hours</label>
                                    <input type="text" class="form-control" id="txthrs" readonly="" name="txthrs" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Width</label>
                                    <input type="text" class="form-control" id="txtwidth" name="txtwidth" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Height</label>
                                    <input type="text" class="form-control" id="txtheight" name="txtheight" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Color</label>
                                    <input type="text" class="form-control" id="txtcolor" name="txtcolor" placeholder="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Thickness</label>
                                    <input type="text" class="form-control" id="txtthickness" name="txtthickness" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" style="color: red" class="control-label">Status</label>
                                    <select class="selectpicker" id="lstwost" name="lstwost" data-live-search="true" data-style="btn-white">
                                        <option value="0">Not Completed</option>
                                        <option value="1">Completed</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h4 class="modal-header" id="lblproduction">Production</h4>

                        <div class="row">
                            <div class="col-md-2" id="dvaluminiumcut">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxaluminiumcut" type="checkbox" />
                                        <label for="chkbxaluminiumcut">Aluminium Cut</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="dvglasscut">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxglasscut" type="checkbox" />
                                        <label for="chkbxglasscut">Glass Cut</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="dvgathering">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxgathering" type="checkbox" />
                                        <label for="chkbxgathering">Gathering</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="dvinstallforglass">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxinstallforglass" type="checkbox" />
                                        <label for="chkbxinstallforglass">Installation for Glass</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="dvinstallforother">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxinstallforother" type="checkbox" />
                                        <label for="chkbxinstallforother">Installation Other</label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-2" id="dvrepair">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxrepair" type="checkbox" />
                                        <label for="chkbxrepair">Repair</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="dvsales">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxsales" type="checkbox" />
                                        <label for="chkbxsales">Sales</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="modal-header" id="lblinstallation">Installation / Delivery</h4>

                        <div class="row">
                            <div class="col-md-2" id="dvcuts">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxcuts" type="checkbox" />
                                        <label for="chkbxcuts">Cuts</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="dvoutsideinstall">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxoutsideinstall" type="checkbox" />
                                        <label for="chkbxoutsideinstall">Outside Installation</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2" id="dvsendglass">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxsendglass" type="checkbox" />
                                        <label for="chkbxsendglass">Send Glass</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" id="dvsendaluminium">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxsendaluminium" type="checkbox" />
                                        <label for="chkbxsendaluminium">Send Aluminium</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="dvsendother">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxsendother" type="checkbox" />
                                        <label for="chkbxsendother">Send Other</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2" id="dvinstallglass">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxinstallglass" type="checkbox" />
                                        <label for="chkbxinstallglass">Install Glass</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" id="dvinstallaluminium">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxinstallaluminium" type="checkbox" />
                                        <label for="chkbxinstallaluminium">Install Aluminium</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="dvinstallother">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxinstallother" type="checkbox" />
                                        <label for="chkbxinstallother">Install Other</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                    <label for="field-7" class="control-label">Note</label>
                                    <textarea class="form-control autogrow" id="txtnote" name="txtnote" placeholder="Write something about Work Order"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <button type="button" class="btn btn-default waves-effect" id="btncloseworkorder" data-dismiss="modal" onclick="resetForm()">Close</button>
                            <button class="btn btn-primary waves-effect waves-light" id="btneditworkorder" type="submit" hidden="hidden">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>

    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/moment.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.js"></script>
    <script src="Scripts/filedownload.js"></script>

    <script>
        var woid = '<%= Request["wo"] %>';
        $(document).ready(function () {
            var resizefunc = [];
            var currentDate = new Date();

            // Date Picker

            $('#txtfrmdate').datetimepicker({ format: 'DD/MM/YYYY' });
            $('#txttodate').datetimepicker({ format: 'DD/MM/YYYY' });

            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#frmAddEdit").validate({
                rules: {
                    "txtwo": "required",
                    "txtquantity": "required",
                    "txtserialno": "required",
                },
                messages: {
                    "txtserialno": "Serial Number is required",
                    "txtwo": "wono is required",
                    "txtquantity": "Quantity is required",
                },
                submitHandler: function (form) {
                    if ($("#hidEditID").val() > 0) {
                        EditWO();
                    }
                    else {
                        AddWO();
                    }
                    return false;
                }
            });
            bindaluminiumtypes();
            bindTabletUsers();
            if (woid != null && woid != "") {
                getHistoryData(woid);
            }
            else {
                getHistoryData(-1);
            }
        });

        function backtowopage() {
            window.location.href = 'WOAluminium.aspx';
        }

        function resetForm() {
            $("#hidEditID").val("0");
            $("#hidDelID").val("0");
            $("#txtserialno").val("");
            $("#txtquantity").val("");
            $("#txtwidth").val("");
            $("#txtheight").val("");
            $("#txtcolor").val("");
            $("#txtnote").val("");
            $("#txtthickness").val("");
            $("#txthrs").val("");
            $("#txtwo").val("");
            bindaluminiumtypes();
            bindTabletUsers();

            $("#chkbxaluminiumcut").prop("checked", false);
            $("#chkbxglasscut").prop("checked", false);
            $("#chkbxgathering").prop("checked", false);
            $("#chkbxinstallforglass").prop("checked", false);
            $("#chkbxinstallforother").prop("checked", false);
            $("#chkbxrepair").prop("checked", false);
            $("#chkbxsales").prop("checked", false);
            $("#chkbxcuts").prop("checked", false);
            $("#chkbxoutsideinstall").prop("checked", false);
            $("#chkbxsendglass").prop("checked", false);
            $("#chkbxsendaluminium").prop("checked", false);
            $("#chkbxsendother").prop("checked", false);
            $("#chkbxinstallglass").prop("checked", false);
            $("#chkbxinstallaluminium").prop("checked", false);
            $("#chkbxinstallother").prop("checked", false);

            var validator = $("#frmAddEdit").validate();
            validator.resetForm();
        }

        function getHistoryData(woid) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWOAluminiumDetail',
                data: 'id=0&woid=' + woid,
                async: true,
                success: function (data) {

                    $('#tablelist').dataTable().fnClearTable();
                    $('#tablelist').dataTable().fnDestroy();

                    $.each(data, function (i, item) {
                        var trHTML = '';


                        var date = new Date(parseInt(item.CreatedDate.replace("/Date(", "").replace(")/", ""), 10));
                        var ds = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + ' ' + formatAMPM(date);

                        trHTML += '<tr><td>' + item.WOID + '</td><td>' + item.Quantity + '</td><td>' + item.SupervisorName + '</td><td>' + item.SerialNumber + '</td><td>' + item.AluminiumTypeName + '</td><td>' + item.Width + '</td><td>' + item.Height + '</td><td>' + item.Color + '</td><td>' + item.Thickness + '</td><td>' + ds + '</td><td>' +
                            '<button type="button" class="btn btn-info waves-effect btnEdit" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="FillWO(' + item.ID + ')"><i class="fa fa-pencil-square-o"></i></button>'
                            //+ '<button class="btn btn-danger waves-effect waves-light btnDelete" onclick="confirmWODelete(' + item.ID + ')"><i class="fa fa-times"></i></button>'
                        + '</td></tr>';
                        $('#tablelist').append(trHTML);
                    });
                    $('#tablelist').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "bSort": false
                        //order: [[0, 'desc']]
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function FillWO(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWOAluminiumDetail',
                data: 'id=' + id + '&woid=0',
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        $("#hidEditID").val(id);
                        $("#txtwo").val(item.WOID);
                        FillWOSteps(item.WOID);
                        $("#txtserialno").val(item.SerialNumber);
                        $("#txtquantity").val(item.Quantity);
                        $("#txtwidth").val(item.Width);
                        $("#txtheight").val(item.Height);
                        $("#txtcolor").val(item.Color);
                        $("#txtnote").val(item.Notes);
                        $("#txthrs").val(item.Hours);
                        $("#txtthickness").val(item.Thickness);

                        $('#lstsupr').val(item.SupervisorID);
                        $('#lstsupr').selectpicker('refresh');

                        $('#lstwost').val(item.isCompleted == 0 ? 0 : 1);
                        $('#lstwost').selectpicker('refresh');

                        $('#lstaluminiumtype').val(item.AluminiumTypeID);
                        $('#lstaluminiumtype').selectpicker('refresh');

                        if (item.isAluminiumCut == 0 ? $("#chkbxaluminiumcut").prop("checked", false) : $("#chkbxaluminiumcut").prop("checked", true));
                        if (item.isGlassCut == 0 ? $("#chkbxglasscut").prop("checked", false) : $("#chkbxglasscut").prop("checked", true));
                        if (item.isGathering == 0 ? $("#chkbxgathering").prop("checked", false) : $("#chkbxgathering").prop("checked", true));
                        if (item.isInstallationGlass == 0 ? $("#chkbxinstallforglass").prop("checked", false) : $("#chkbxinstallforglass").prop("checked", true));
                        if (item.isInstallationOther == 0 ? $("#chkbxinstallforother").prop("checked", false) : $("#chkbxinstallforother").prop("checked", true));
                        if (item.isRepair == 0 ? $("#chkbxrepair").prop("checked", false) : $("#chkbxrepair").prop("checked", true));
                        if (item.isSales == 0 ? $("#chkbxsales").prop("checked", false) : $("#chkbxsales").prop("checked", true));
                        if (item.isCuts == 0 ? $("#chkbxcuts").prop("checked", false) : $("#chkbxcuts").prop("checked", true));
                        if (item.isOutSideInstallation == 0 ? $("#chkbxoutsideinstall").prop("checked", false) : $("#chkbxoutsideinstall").prop("checked", true));
                        if (item.isSendGlass == 0 ? $("#chkbxsendglass").prop("checked", false) : $("#chkbxsendglass").prop("checked", true));
                        if (item.isSendAluminium == 0 ? $("#chkbxsendaluminium").prop("checked", false) : $("#chkbxsendaluminium").prop("checked", true));
                        if (item.isSendOther == 0 ? $("#chkbxsendother").prop("checked", false) : $("#chkbxsendother").prop("checked", true));
                        if (item.isInstallGlass == 0 ? $("#chkbxinstallglass").prop("checked", false) : $("#chkbxinstallglass").prop("checked", true));
                        if (item.isInstallAluminium == 0 ? $("#chkbxinstallaluminium").prop("checked", false) : $("#chkbxinstallaluminium").prop("checked", true));
                        if (item.isInstallOther == 0 ? $("#chkbxinstallother").prop("checked", false) : $("#chkbxinstallother").prop("checked", true));
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function FillWOSteps(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWOAluminium',
                data: 'id=' + id,
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        if (item.isAluminiumCut == 0 ? $("#dvaluminiumcut").hide() : $("#dvaluminiumcut").show());
                        if (item.isGlassCut == 0 ? $("#dvglasscut").hide() : $("#dvglasscut").show());
                        if (item.isGathering == 0 ? $("#dvgathering").hide() : $("#dvgathering").show());
                        if (item.isInstallationGlass == 0 ? $("#dvinstallforglass").hide() : $("#dvinstallforglass").show());
                        if (item.isInstallationOther == 0 ? $("#dvinstallforother").hide() : $("#dvinstallforother").show());
                        if (item.isRepair == 0 ? $("#dvrepair").hide() : $("#dvrepair").show());
                        if (item.isSales == 0 ? $("#dvsales").hide() : $("#dvsales").show());
                        if (item.isCuts == 0 ? $("#dvcuts").hide() : $("#dvcuts").show());
                        if (item.isOutSideInstallation == 0 ? $("#dvoutsideinstall").hide() : $("#dvoutsideinstall").show());
                        if (item.isSendGlass == 0 ? $("#dvsendglass").hide() : $("#dvsendglass").show());
                        if (item.isSendAluminium == 0 ? $("#dvsendaluminium").hide() : $("#dvsendaluminium").show());
                        if (item.isSendOther == 0 ? $("#dvsendother").hide() : $("#dvsendother").show());
                        if (item.isInstallGlass == 0 ? $("#dvinstallglass").hide() : $("#dvinstallglass").show());
                        if (item.isInstallAluminium == 0 ? $("#dvinstallaluminium").hide() : $("#dvinstallaluminium").show());
                        if (item.isInstallOther == 0 ? $("#dvinstallother").hide() : $("#dvinstallother").show());
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function AddWO() {
            var isaluminiumcut = $(chkbxaluminiumcut).prop("checked") == false ? "0" : "1";
            var isglasscut = $(chkbxglasscut).prop("checked") == false ? "0" : "1";
            var isgathering = $(chkbxgathering).prop("checked") == false ? "0" : "1";
            var isinstallforglass = $(chkbxinstallforglass).prop("checked") == false ? "0" : "1";
            var isinstallforother = $(chkbxinstallforother).prop("checked") == false ? "0" : "1";
            var isrepair = $(chkbxrepair).prop("checked") == false ? "0" : "1";
            var issales = $(chkbxsales).prop("checked") == false ? "0" : "1";
            var iscuts = $(chkbxcuts).prop("checked") == false ? "0" : "1";
            var isoutsideinstall = $(chkbxoutsideinstall).prop("checked") == false ? "0" : "1";
            var issendglass = $(chkbxsendglass).prop("checked") == false ? "0" : "1";
            var issendaluminium = $(chkbxsendaluminium).prop("checked") == false ? "0" : "1";
            var issendother = $(chkbxsendother).prop("checked") == false ? "0" : "1";
            var isinstallglass = $(chkbxinstallglass).prop("checked") == false ? "0" : "1";
            var isinstallaluminium = $(chkbxinstallaluminium).prop("checked") == false ? "0" : "1";
            var isinstallother = $(chkbxinstallother).prop("checked") == false ? "0" : "1";

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditWOAluminiumDetail',
                data: 'id=0&quantity=' + $("#txtquantity").val() + '&serialno=' + $("#txtserialno").val() + '&width=' + $("#txtwidth").val()
                + '&height=' + $("#txtheight").val() + '&color=' + $("#txtcolor").val() + '&thickness=' + $("#txtthickness").val()
                + '&aluminiumtypeid=' + $('#lstaluminiumtype').val() + '&isaluminiumcut=' + isaluminiumcut + '&isglasscut=' + isglasscut + '&isgathering=' + isgathering
                + '&isinstallforglass=' + isinstallforglass + '&isinstallforother=' + isinstallforother + '&isrepair=' + isrepair
                + '&issales=' + issales + '&iscuts=' + iscuts + '&isoutsideinstall=' + isoutsideinstall + '&issendglass=' + issendglass
                + '&issendaluminium=' + issendaluminium + '&issendother=' + issendother + '&isinstallglass=' + isinstallglass + '&isinstallaluminium=' + isinstallaluminium
                + '&isinstallother=' + isinstallother + '&notes=' + $("#txtnote").val() + '&supervisorid=' + $("#lstsupr").val() + '&hours=' + $("#txthrs").val()
                + '&woid=' + $("#txtwo").val() + '&iscompleted=' + $("#lstwost").val(),
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "Work Order has been saved successfully");
                        resetForm();
                        getHistoryData(woid);
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function EditWO() {
            var id = $("#hidEditID").val();

            var isaluminiumcut = $(chkbxaluminiumcut).prop("checked") == false ? "0" : "1";
            var isglasscut = $(chkbxglasscut).prop("checked") == false ? "0" : "1";
            var isgathering = $(chkbxgathering).prop("checked") == false ? "0" : "1";
            var isinstallforglass = $(chkbxinstallforglass).prop("checked") == false ? "0" : "1";
            var isinstallforother = $(chkbxinstallforother).prop("checked") == false ? "0" : "1";
            var isrepair = $(chkbxrepair).prop("checked") == false ? "0" : "1";
            var issales = $(chkbxsales).prop("checked") == false ? "0" : "1";
            var iscuts = $(chkbxcuts).prop("checked") == false ? "0" : "1";
            var isoutsideinstall = $(chkbxoutsideinstall).prop("checked") == false ? "0" : "1";
            var issendglass = $(chkbxsendglass).prop("checked") == false ? "0" : "1";
            var issendaluminium = $(chkbxsendaluminium).prop("checked") == false ? "0" : "1";
            var issendother = $(chkbxsendother).prop("checked") == false ? "0" : "1";
            var isinstallglass = $(chkbxinstallglass).prop("checked") == false ? "0" : "1";
            var isinstallaluminium = $(chkbxinstallaluminium).prop("checked") == false ? "0" : "1";
            var isinstallother = $(chkbxinstallother).prop("checked") == false ? "0" : "1";

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditWOAluminiumDetail',
                data: 'id=' + id + '&quantity=' + $("#txtquantity").val() + '&serialno=' + $("#txtserialno").val() + '&width=' + $("#txtwidth").val()
                + '&height=' + $("#txtheight").val() + '&color=' + $("#txtcolor").val() + '&thickness=' + $("#txtthickness").val()
                + '&aluminiumtypeid=' + $('#lstaluminiumtype').val() + '&isaluminiumcut=' + isaluminiumcut + '&isglasscut=' + isglasscut + '&isgathering=' + isgathering
                + '&isinstallforglass=' + isinstallforglass + '&isinstallforother=' + isinstallforother + '&isrepair=' + isrepair
                + '&issales=' + issales + '&iscuts=' + iscuts + '&isoutsideinstall=' + isoutsideinstall + '&issendglass=' + issendglass
                + '&issendaluminium=' + issendaluminium + '&issendother=' + issendother + '&isinstallglass=' + isinstallglass + '&isinstallaluminium=' + isinstallaluminium
                + '&isinstallother=' + isinstallother + '&notes=' + $("#txtnote").val() + '&supervisorid=' + $("#lstsupr").val() + '&hours=' + $("#txthrs").val()
                + '&woid=' + $("#txtwo").val() + '&iscompleted=' + $("#lstwost").val(),
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "Work Order has been updated successfully");
                        resetForm();
                        getHistoryData(woid);
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function formatAMPM(date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        }

        function confirmWODelete(id) {
            $("#hidDelID").val(id);
            jQuery('#modal-delete-wo').modal('show', { backdrop: 'static' });
        }

        function DeleteWO() {
            var id = $('input#hidDelID').val();

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/DeleteWOAluminium',
                data: "id=" + id,
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('warning', 'top right', 'Deleted', 'Work Order deleted successfully.');
                        getSearchWorkOrder();
                        $('#modal-delete-wo').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function bindaluminiumtypes() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetAluminiumTypes',
                data: 'id=0',
                async: true,
                success: function (data) {
                    $("#lstaluminiumtype").append("").val("").html("");
                    $("#lstaluminiumtype").append($("<option></option>").val("-1").html("Select"));

                    $("#lstsearchdtypes").append("").val("").html("");
                    $("#lstsearchdtypes").append($("<option></option>").val("-1").html("Select"));

                    $.each(data, function (key, val) {
                        $("#lstaluminiumtype").append($("<option></option>").val(val.ID).html(val.AluminiumTypeName));

                        $("#lstsearchdtypes").append($("<option></option>").val(val.ID).html(val.AluminiumTypeName));
                    });
                    $('#lstaluminiumtype').selectpicker('refresh');

                    $('#lstsearchdtypes').selectpicker('refresh');
                }
            });
        }

        $('#lstaluminiumtype').change(function () {
            getserialno($('#lstaluminiumtype').val());
        });

        function getserialno(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetAluminiumTypes',
                data: 'id=' + id,
                async: true,
                success: function (data) {
                    $("#txtserialno").val("");
                    $.each(data, function (key, val) {
                        $("#txtserialno").val(val.SerialNumber);
                    });
                }
            });
        }

        function bindTabletUsers() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTabletUsers',
                data: "user=0&tabletroleid=1",
                async: true,
                success: function (data) {
                    $("#lstsupr").append("").val("").html("");

                    $.each(data, function (key, val) {
                        $("#lstsupr").append($("<option></option>").val(val.ID).html(val.Name));
                    });
                    $('#lstsupr').selectpicker('refresh');
                }
            });
        }
    </script>
</asp:Content>




