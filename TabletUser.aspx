﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TabletUser.aspx.cs" Inherits="TabletUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Plugin Css-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

    <link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- Plugins css-->
    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="assets/plugins/switchery/dist/switchery.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Tablet User</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li>
                                <a href="#">Tablet Users</a>
                            </li>
                            <li class="active">Tablet User
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="m-b-30">
                                    <button class="btn btn-primary waves-effect waves-light btnAdd" data-toggle="modal" onclick="resetUserForm()" data-target=".bs-example-modal-lg">Add New</button>
                                </div>
                            </div>
                        </div>

                        <div class="" style='overflow: scroll; overflow-y: hidden;'>
                            <table id="usertable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>UserName</th>
                                        <th>Name</th>
                                        <th>Mobile</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
    </div>

    <div class="modal fade" id="modal-delete-confirm" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Confirm</h4>
                </div>

                <div class="modal-body">
                    Are you sure, you want to delete current record!
                </div>

                <div class="modal-footer">
                    <input type="hidden" id="hidDeleteID" name="deleteID" value="0" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info" id="btnDelete" onclick="DeleteUser()">Continue</button>
                </div>
            </div>
        </div>
    </div>

    <!-- sample modal content -->
    <div class="modal fade bs-example-modal-lg" id="modal-AddEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form name="frmAddEdit" id="frmAddEdit" method="post">
                    <input type="hidden" id="hidEditID" name="hidEditID" value="0" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">User Detail</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">User Name</label>
                                    <input type="text" class="form-control" id="txtusername" name="username" maxlength="15" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Password</label>
                                    <input type="password" class="form-control" id="txtpassword" maxlength="15" name="password" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Name</label>
                                    <input type="text" class="form-control" id="txtname" name="name" maxlength="40" placeholder="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Mobile</label>
                                    <input type="text" class="form-control" id="txtmobile" onkeypress="return isNumberKey(event)" name="txtmobile" maxlength="8" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Role</label>
                                    <select id="lstrole" class="selectpicker" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Status</label>
                                    <select id="lststatus" name="lststatus" class="selectpicker" data-style="btn-primary btn-custom">
                                        <option value="1">Active</option>
                                        <option value="0">InActive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary waves-effect waves-light" type="submit" id="btnadduser">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>
    <script src="assets/plugins/switchery/dist/switchery.min.js"></script>

    <script>
        var resizefunc = [];

        $(document).ready(function () {
            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $.validator.addMethod("noSpace", function (value, element) {
                return value.indexOf(" ") < 0 && value != "";
            }, "Space are not allowed")

            $("#frmAddEdit").validate({
                rules: {
                    "username": {
                        required: true,
                        noSpace: true,
                        remote: {
                            url: "AdminWeb.asmx/TabletUserNameExist",
                            type: "post",
                            data: {
                                username: function () { return $("#txtusername").val(); },
                                id: function () { return $("#hidEditID").val(); }
                            }
                        },
                    },
                    "password": { required: true, noSpace: true },
                    "name": { required: true },
                },
                messages: {
                    "username": { required: "Username is required", remote: "username already exist" },
                    "password": { required: 'Password is required' },
                    "name": { required: 'Name is required' },
                },
                submitHandler: function (form) {
                    if ($("#hidEditID").val() > 0) {
                        EditUser();
                    }
                    else {
                        AddUser();
                    }
                    return false;
                }
            });
            bindrole();
            getUser();
            resetUserForm();
        });

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function bindrole() {
            var data = "role=0";
            $.post("AdminWeb.asmx/GetTabletRoles", data, function (data) {
                $("#lstrole").append("").val("").html("");
                //$("#lstcustomertype").append($("<option></option>").val(0).html("All"));
                $.each(JSON.parse(data), function (key, val) {
                    $("#lstrole").append($("<option></option>").val(val.ID).html(val.Name));
                });
                $('#lstrole').selectpicker('refresh');
            });
        }

        function getUser() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTabletUsers',
                data: "user=0&tabletroleid=0",
                async: true,
                success: function (data) {
                    //alert(data.response.customers[0].customerID);
                    //$(this).html($("#tableListTemplate").render(data.response.customers));

                    $('#usertable').dataTable().fnClearTable();
                    $('#usertable').dataTable().fnDestroy();
                    $.each(data, function (i, item) {
                        var active = "";
                        if (item.isActive == "1") { active = "Active" } else { active = "InActive" }
                        var trHTML = '';

                        trHTML += '<tr><td>' + item.Username + '</td><td>' + item.Name + '</td><td>' + item.Mobile + '</td><td>' + item.TableRoleName + '</td><td>' + active + '</td><td>' +
                            '<button type="button" class="btn btn-info waves-effect btnEdit" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="filluserdata(' + item.ID + ')"><i class="fa fa-pencil-square-o"></i></button><button class="btn btn-danger waves-effect waves-light btnDelete" onclick="confirmUserDelete(' + item.ID + ')"><i class="fa fa-times"></i></button>' + '</td></tr>';

                        //'<a href="#" class="on-default edit-row" onclick="fillcustomerdata(' + item.customerID + ')" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-pencil"></i> Edit</a><br /> <a href="javascript:;"  onclick="confirmCustomerDelete(' + item.customerID + ')" class="on-default remove-row"><i class="fa fa-trash-o"></i>Delete</a>' + '</td><td>' + '<a href="WorkOrder.aspx?customerID=' + item.customerID + '"><i class="fa fa-file-text-o"></i> WorkOrder</a>' + '</td></tr>';
                        $('#usertable').append(trHTML);

                    });

                    $('#usertable').dataTable({
                        "bPaginate": true,
                        "bLengthChange": true,
                        "bFilter": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "bSort": false
                        //order: [[0, 'desc']]
                    });

                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function filluserdata(userid) {
            //$("#btnadduser").hide();
            //$("#btnedituser").show();
            $('input#hidEditID').val(userid);

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTabletUsers',
                data: "user=" + userid + '&tabletroleid=0',
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        $("#txtusername").val(item.Username);
                        $("#txtpassword").val(item.Password);

                        if (item.TabletRoleID > 0) {
                            $("#lstrole").val(item.TabletRoleID);
                            $('#lstrole').selectpicker('refresh');
                        }
                        else { $("#lstrole").val(""); $('#lstrole').selectpicker('refresh'); }
                        $("#txtmobile").val(item.Mobile);
                        $("#txtname").val(item.Name);
                        $("#lststatus").val(item.isActive);
                        $('#lststatus').selectpicker('refresh');
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function confirmUserDelete(id) {
            $("#hidDeleteID").val(id);
            jQuery('#modal-delete-confirm').modal('show', { backdrop: 'static' });
        }

        function DeleteUser() {
            var userid = $('input#hidDeleteID').val();

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/DeleteUser',
                data: 'id=' + userid,
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('warning', 'top right', 'Deleted', 'User deleted successfully.');

                        //alert("User deleted successfully");
                        getUser();
                        $('#modal-delete-confirm').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function EditUser() {

            var userid = $('input#hidEditID').val();

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditTabletUser',
                data: "id=" + userid + "&username=" + $("#txtusername").val() + "&password=" + $("#txtpassword").val() + "&name=" + $("#txtname").val() + "&mobile=" + $("#txtmobile").val() + "&roleid=" + $("#lstrole").val() + "&active=" + $("#lststatus").val(),
                async: false,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', 'User has been updated successfully.');

                        //alert("User has been updated successfully");
                        resetUserForm();
                        $('#modal-AddEdit').modal('toggle');
                        getUser();
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function AddUser() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditTabletUser',
                data: "id=0&username=" + $("#txtusername").val() + "&password=" + $("#txtpassword").val() + "&name=" + $("#txtname").val() + "&mobile=" + $("#txtmobile").val() + "&roleid=" + $("#lstrole").val() + "&active=" + $("#lststatus").val(),
                async: false,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', 'User has been saved successfully.');
                        //alert("User has been saved successfully");
                        resetUserForm();
                        $('#modal-AddEdit').modal('toggle');
                        getUser();
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function resetUserForm() {
            $("#hidEditID").val("0");
            $("#hidDeleteID").val("0");
            $("#txtusername").val("");
            $("#txtpassword").val("");
            $("#txtname").val("");
            $("#txtmobile").val("");
            $("#lstrole").val("0");
            $('#lstrole').selectpicker('refresh');
            $("#lststatus").val("1");
            $('#lststatus').selectpicker('refresh');
            //$("#btnadduser").show();
            //$("#btnedituser").hide();

            var validator = $("#frmAddEdit").validate();
            validator.resetForm();
        }
    </script>
</asp:Content>

