﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Neama login Page">
    <meta name="author" content="Neama">

    <link rel="shortcut icon" href="assets/images/favicon_1.ico">

    <title>Neama | Login</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    <script src="assets/js/modernizr.min.js"></script>

</head>
<body>

    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
        <div class=" card-box">
            <div class="panel-heading">
                <h3 class="text-center">Sign In to
                     <strong class="text-primary">Neama</strong>
                </h3>
            </div>


            <div class="panel-body">
                <form id="formlogin" class="form-horizontal m-t-20">

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" id="txtusername" maxlength="15" name="username" type="text" placeholder="Username">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" id="txtpassword" maxlength="15" name="password" type="password" placeholder="Password">
                        </div>
                    </div>

                    <%-- <div class="form-group ">
                        <div class="col-xs-12">
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup">
                                    Remember me
                                </label>
                            </div>

                        </div>
                    </div>--%>

                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-block text-uppercase waves-effect waves-light" type="submit">log In</button>
                        </div>
                    </div>
                    <%-- <div class="form-group m-t-30 m-b-0">
                        <div class="col-sm-12">
                            <a href="page-recoverpw.html" class="text-dark"><i class="fa fa-lock m-r-5"></i>Forgot your password?</a>
                        </div>
                    </div>--%>
                </form>

            </div>

        </div>
        <%-- <div class="row">
            <div class="col-sm-12 text-center">
                <p>Don't have an account? <a href="page-register.html" class="text-primary m-l-5"><b>Sign Up</b></a></p>

            </div>
        </div>--%>
    </div>
    <footer class="footer text-right">
        <%= DateTime.Now.Year.ToString()  %> © Neama Powered by <a href="http://www.e-amanah.com/">Amanah Teknologia</a>
    </footer>





    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/jquery.validate.min.js"></script>

    <script src="assets/plugins/notifyjs/dist/notify.min.js"></script>
    <script src="assets/plugins/notifications/notify-metro.js"></script>

    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>
    <script type="text/javascript">
        resetform();
        var resizefunc = [];
        var baseurl = 'Default.aspx';


        function disableBack() { window.history.forward() }

        window.onload = disableBack();
        window.onpageshow = function (evt) { if (evt.persisted) disableBack() }

        $.validator.addMethod("noSpace", function (value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "Space are not allowed")

        $("#formlogin").validate({
            rules: {
                "username": { required: true, noSpace: true },
                "password": { required: true, noSpace: true },
            },
            messages: {
                "username": { required: "UserName is required" },
                "password": { required: "Password is required" },
            },
            submitHandler: function (form) {
                login();
                return false;
            }
        });

        function resetform() {
            $("#txtusername").val("");
            $("#txtpassword").val("");
        }

        function login() {
            $.ajax({
                url: 'AdminWeb.asmx/LoginAdmin',
                method: 'POST',
                dataType: 'text',
                data: "username=" + $("#txtusername").val() + "&password=" + $("#txtpassword").val(),
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("textStatus->" + textStatus);
                    alert("errorThrown->" + errorThrown);
                    //if (textStatus == "timeout") {
                    //    alert("Request Timeout");
                    //}
                },
                success: function (response) {
                    //alert(response);
                    // Login status [success|invalid]
                    var login_status = response;


                    // If login is invalid, we store the 
                    if (login_status == 'invalid') {
                        resetform();
                        $.Notification.autoHideNotify('error', 'top right', 'Invalid Login', 'Invalid Username and/or Password.');
                        //$.Notification.autoHideNotify('info', 'top right', 'Info', 'Username and Password are Case Sensative.');
                    }
                    else
                        if (login_status == 'success') {
                            var redirect_url = baseurl;

                            if (response.redirect_url && response.redirect_url.length) {
                                redirect_url = response.redirect_url;
                            }
                            window.location.href = redirect_url;
                        }
                }
            });

        }
    </script>
</body>
</html>

