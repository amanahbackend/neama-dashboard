﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Clear();
        //Session.Clear();

        //Response.Redirect("Login.aspx");

        //Response.Write("Logged Out");
        //Response.End();

        Session["Admin_ID"] = null;
        Session["Admin_Username"] = null;
        Session["Admin_Name"] = null;
        Session["Admin_Role"] = null;
        Session["User_Type_ID"] = null;

        Session.Abandon();
        Session.Clear();
        Response.Redirect("Login.aspx");
    }
}