﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="WorkOrder.aspx.cs" Inherits="WorkOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Plugin Css-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

    <link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- Plugins css-->
    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />

    <link href="assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Work Order Door</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li>
                                <a href="#">Create Work Order</a>
                            </li>
                            <li class="active">Work Order Door
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Work order No.</label>
                                    <input type="text" class="form-control" id="txtsearchwono" name="txtsearchwono" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Serial No.</label>
                                    <input type="text" class="form-control" id="txtsearchserialno" name="txtsearchserialno" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Door Types</label>
                                    <select id="lstsearchdtypes" name="lstsearchdtypes" class="selectpicker" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label for="field-1" class="control-label">From Date</label>

                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="From Date" id="txtfrmdate" />
                                    <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label for="field-1" class="control-label">To Date</label>

                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="To Date" id="txttodate" />
                                    <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <button class="btn btn-primary waves-effect waves-light" onclick="clearsearch()">Clear</button>
                                <button class="btn btn-primary waves-effect waves-light" onclick="getSearchWorkOrder()">Search</button>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary waves-effect waves-light btnAdd" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="resetForm()">Add New</button>
                            </div>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->

                <div class="panel">
                    <div class="panel-body">
                        <div class="" style='overflow-x: scroll; overflow-y: hidden;'>
                            <table id="tablelist" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>WO No.</th>
                                        <th>Supervisor</th>
                                        <th>Serial No.</th>
                                        <th>Door Type</th>
                                        <th>Width</th>
                                        <th>Height</th>
                                        <th>Color</th>
                                        <th>Quantity</th>
                                        <th>Created Date</th>
                                        <th>Action</th>
                                        <th>Quantity Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->

        <!-- MODAL -->
        <div class="modal fade" id="modal-delete-wo" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Confirm</h4>
                    </div>

                    <div class="modal-body">
                        Are you sure, you want to delete current record!
                    </div>

                    <div class="modal-footer">
                        <input type="hidden" id="hidDelID" name="hidDelID" value="0" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info" id="btnDeleteWorkOrder" onclick="DeleteWorkOrder()">Continue</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end Modal -->

    </div>
    <!-- sample modal content -->
    <div class="modal fade bs-example-modal-lg" id="modal-AddEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="frmAddEdit" name="frmAddEdit" method="post">
                    <input type="hidden" id="hidEditID" name="hidEditID" value="0" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Work Order Door Detail</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">


                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Door Types</label>
                                    <select class="selectpicker" id="lstdoortype" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Supervisor</label>
                                    <select class="selectpicker" id="lstsupr" name="lstsupr" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Hours</label>
                                    <input type="text" class="form-control" id="txthrs" onkeypress="return allowdotnnumeric(event)" maxlength="10" name="txthrs" placeholder="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Serial No.  </label>
                                    <input type="text" class="form-control" id="txtserialno" readonly="" name="txtserialno" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Quantity</label>
                                    <input type="text" class="form-control" id="txtquantity" onkeypress="return isNumberKey(event)" maxlength="5" name="txtquantity" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Width</label>
                                    <input type="text" class="form-control" id="txtwidth" name="txtwidth" maxlength="10" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Height</label>
                                    <input type="text" class="form-control" id="txtheight" name="txtheight" maxlength="10" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Color</label>
                                    <input type="text" class="form-control" id="txtcolor" maxlength="40" name="txtcolor" placeholder="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Wall Width</label>
                                    <input type="text" class="form-control" id="txtwallwidth" maxlength="25" name="txtwallwidth" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" style="color: red" class="control-label">Status</label>
                                    <select class="selectpicker" id="lstwoddst" name="lstwoddst" data-live-search="true" data-style="btn-white">
                                        <option value="0">Not Completed</option>
                                        <option value="1">Completed</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h4 class="modal-header" id="lblproduction">Production</h4>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxcollection" type="checkbox" />
                                        <label for="chkbxcollection">
                                            Collection
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-1">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxfoom" type="checkbox" />
                                        <label for="chkbxfoom">
                                            Foom
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxcfglass" type="checkbox" />
                                        <label for="chkbxcfglass">
                                            Cutting for Glass
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxcleaning" type="checkbox" />
                                        <label for="chkbxcleaning">
                                            Cleaning
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxglassfitting" type="checkbox" />
                                        <label for="chkbxglassfitting">
                                            Glass Fitting
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxpainting" type="checkbox" />
                                        <label for="chkbxpainting">
                                            Painting
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxrepair" type="checkbox" />
                                        <label for="chkbxrepair">Repair</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxsales" type="checkbox" />
                                        <label for="chkbxsales">Sales</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="modal-header" id="lblinstallation">Installation / Delivery</h4>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxframeinside" type="checkbox" />
                                        <label for="chkbxframeinside">
                                            Frame Inside
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxframeoutside" type="checkbox" />
                                        <label for="chkbxframeoutside">
                                            FrameOutside
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxdoor" type="checkbox" />
                                        <label for="chkbxdoor">
                                            Door
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                    <label for="field-7" class="control-label">Note</label>
                                    <textarea class="form-control autogrow" id="txtnote" name="txtnote" placeholder="Write something about Work Order"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <button type="button" class="btn btn-default waves-effect" id="btncloseworkorder" data-dismiss="modal" onclick="resetForm()">Close</button>
                            <button class="btn btn-primary waves-effect waves-light" id="btneditworkorder" type="submit" hidden="hidden">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>

    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/moment.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.js"></script>
    <script src="Scripts/filedownload.js"></script>

    <script>

        $(document).ready(function () {
            var resizefunc = [];
            var currentDate = new Date();

            // Date Picker

            $('#txtfrmdate').datetimepicker({ format: 'DD/MM/YYYY' });
            $('#txttodate').datetimepicker({ format: 'DD/MM/YYYY' });

            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#frmAddEdit").validate({
                rules: {
                    "txtquantity": "required",
                    "txtserialno": "required",
                },
                messages: {
                    "txtserialno": "Serial Number is required",
                    "txtquantity": "Quantity is required",
                },
                submitHandler: function (form) {
                    if ($("#hidEditID").val() > 0) {
                        EditWO();
                    }
                    else {
                        AddWO();
                    }
                    return false;
                }
            });
            binddoortypes();
            bindTabletUsers();
            getSearchWorkOrder();
        });


        function allowdotnnumeric(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /^[0-9.,]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function clearsearch() {
            $('#txtfrmdate').val("");
            $('#txttodate').val("");
            $('#txtsearchwono').val("");
            $('#txtsearchserialno').val("");
            $('#lstsearchdtypes').val("-1");
            $('#lstsearchdtypes').selectpicker('refresh');
            getSearchWorkOrder();
        }

        function getSearchWorkOrder() {
            debugger;
            var frmdate = $('#txtfrmdate').val();
            var todate = $('#txttodate').val();

            var fd = "", td = "";
            if (frmdate == null || frmdate == "") {

            } else {
                frmdate = $("#txtfrmdate").val().split('/');
                fd = frmdate[1] + '/' + frmdate[0] + '/' + frmdate[2] + ' 00:00:00';

            }
            if (todate == null || todate == "") {

            }
            else {
                todate = $("#txttodate").val().split('/');
                td = todate[1] + '/' + todate[0] + '/' + todate[2] + ' 23:59:59';
            }
            //alert($('#txtsearchwono').val());
            var id = 0;

            if ($('#txtsearchwono').val() == null || $('#txtsearchwono').val() == "") {
            } else {
                id = $('#txtsearchwono').val();
            }

            var typeid = $("#lstsearchdtypes").val();
            if ($('#lstsearchdtypes').val() == null || $('#lstsearchdtypes').val() == "") {
                typeid = 0;
            } else {
                typeid = $('#lstsearchdtypes').val();
            }
            //var data2 = 'id=' + id + '&serialno=' + $("#txtsearchserialno").val() + '&doortypeid=' + typeid + '&frmdate=' + fd + '&todate=' + td;
            //alert(data2);
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetSearchWOs',
                data: 'id=' + id + '&serialno=' + $("#txtsearchserialno").val() + '&doortypeid=' + typeid + '&frmdate=' + fd + '&todate=' + td,
                async: true,
                success: function (data) {

                    $('#tablelist').dataTable().fnClearTable();
                    $('#tablelist').dataTable().fnDestroy();

                    $.each(data, function (i, item) {
                        var trHTML = '';


                        var date = new Date(parseInt(item.CreatedDate.replace("/Date(", "").replace(")/", ""), 10));
                        var ds = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + ' ' + formatAMPM(date);

                        trHTML += '<tr><td>' + item.ID + '</td><td>' + item.SupervisorName + '</td><td>' + item.SerialNumber + '</td><td>' + item.DoorTypeName + '</td><td>' + item.Width + '</td><td>' + item.Height + '</td><td>' + item.Color + '</td><td>' + item.Quantity + '</td><td>' + ds + '</td><td>' +
                            '<button type="button" class="btn btn-info waves-effect btnEdit" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="FillWO(' + item.ID + ')"><i class="fa fa-pencil-square-o"></i></button>'
                            + '&nbsp; <button class="btn btn-danger waves-effect waves-light btnDelete" onclick="confirmWODelete(' + item.ID + ')"><i class="fa fa-times"></i></button>' + '</td><td>'
                            + '<button type="button" class="btn btn-success waves-effect btnEdit" onclick="Redirecttodetial(' + item.ID + ')"> <span class="btn-label"><i class="fa fa-exclamation"></i></span >Details' + '</td></tr>';
                        $('#tablelist').append(trHTML);
                    });
                    $('#tablelist').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "bSort": false
                        //order: [[0, 'desc']]
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }


        function resetForm() {
            $("#hidEditID").val("0");
            $("#hidDelID").val("0");
            $("#txtserialno").val("");
            $("#txtquantity").val("");
            $("#txtwidth").val("");
            $("#txtheight").val("");
            $("#txtcolor").val("");
            $("#txtnote").val("");
            $("#txthrs").val("");
            $("#txtwallwidth").val("");
            binddoortypes();
            //bindTabletUsers();

            $('#lstsupr').val(0);
            $('#lstsupr').selectpicker('refresh');

            $('#lstwoddst').val(0);
            $('#lstwoddst').selectpicker('refresh');

            $("#chkbxcollection").prop("checked", false);
            $("#chkbxfoom").prop("checked", false);
            $("#chkbxcfglass").prop("checked", false);
            $("#chkbxcleaning").prop("checked", false);
            $("#chkbxglassfitting").prop("checked", false);
            $("#chkbxpainting").prop("checked", false);
            $("#chkbxframeinside").prop("checked", false);
            $("#chkbxframeoutside").prop("checked", false);
            $("#chkbxdoor").prop("checked", false);
            $("#chkbxrepair").prop("checked", false);
            $("#chkbxsales").prop("checked", false);

            var validator = $("#frmAddEdit").validate();
            validator.resetForm();
        }

        function getHistoryData() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWOs',
                data: 'id=0',
                async: true,
                success: function (data) {

                    $('#tablelist').dataTable().fnClearTable();
                    $('#tablelist').dataTable().fnDestroy();

                    $.each(data, function (i, item) {
                        var trHTML = '';


                        var date = new Date(parseInt(item.CreatedDate.replace("/Date(", "").replace(")/", ""), 10));
                        var ds = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + ' ' + formatAMPM(date);

                        trHTML += '<tr><td>' + item.ID + '</td><td>' + item.SupervisorName + '</td><td>' + item.SerialNumber + '</td><td>' + item.DoorTypeName + '</td><td>' + item.Width + '</td><td>' + item.Height + '</td><td>' + item.Color + '</td><td>' + item.Quantity + '</td><td>' + ds + '</td><td>' +
                            '<button type="button" class="btn btn-info waves-effect btnEdit" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="FillWO(' + item.ID + ')"><i class="fa fa-pencil-square-o"></i></button>'
                            + '&nbsp; <button class="btn btn-danger waves-effect waves-light btnDelete" onclick="confirmWODelete(' + item.ID + ')"><i class="fa fa-times"></i></button>' + '</td><td>'
                            + '<button type="button" class="btn btn-success waves-effect btnEdit" onclick="Redirecttodetial(' + item.ID + ')"> <span class="btn-label"><i class="fa fa-exclamation"></i></span >Details' + '</td></tr>';
                        $('#tablelist').append(trHTML);
                    });
                    $('#tablelist').dataTable({
                        //order: [[0, 'desc']]
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function FillWO(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetWOs',
                data: 'id=' + id,
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        $("#hidEditID").val(id);
                        $("#txtserialno").val(item.SerialNumber);
                        $("#txtquantity").val(item.Quantity);
                        $("#txtwidth").val(item.Width);
                        $("#txtheight").val(item.Height);
                        $("#txtcolor").val(item.Color);
                        $("#txtnote").val(item.Notes);
                        $("#txtwallwidth").val(item.WallWidth);
                        $("#txthrs").val(item.Hours);

                        $('#lstwoddst').val(item.isCompleted == 0 ? 0 : 1);
                        $('#lstwoddst').selectpicker('refresh');

                        $('#lstsupr').val(item.SupervisorID);
                        $('#lstsupr').selectpicker('refresh');

                        $('#lstdoortype').val(item.DoorTypeID);
                        $('#lstdoortype').selectpicker('refresh');
                        if (item.IsCollection == 0 ? $("#chkbxcollection").prop("checked", false) : $("#chkbxcollection").prop("checked", true));
                        if (item.IsFoom == 0 ? $("#chkbxfoom").prop("checked", false) : $("#chkbxfoom").prop("checked", true));
                        if (item.IsCuttingForGlass == 0 ? $("#chkbxcfglass").prop("checked", false) : $("#chkbxcfglass").prop("checked", true));
                        if (item.IsCleaning == 0 ? $("#chkbxcleaning").prop("checked", false) : $("#chkbxcleaning").prop("checked", true));
                        if (item.IsGlassFitting == 0 ? $("#chkbxglassfitting").prop("checked", false) : $("#chkbxglassfitting").prop("checked", true));
                        if (item.IsPainting == 0 ? $("#chkbxpainting").prop("checked", false) : $("#chkbxpainting").prop("checked", true));
                        if (item.IsFrameInside == 0 ? $("#chkbxframeinside").prop("checked", false) : $("#chkbxframeinside").prop("checked", true));
                        if (item.IsFrameOutside == 0 ? $("#chkbxframeoutside").prop("checked", false) : $("#chkbxframeoutside").prop("checked", true));
                        if (item.IsDoor == 0 ? $("#chkbxdoor").prop("checked", false) : $("#chkbxdoor").prop("checked", true));
                        if (item.isRepair == 0 ? $("#chkbxrepair").prop("checked", false) : $("#chkbxrepair").prop("checked", true));
                        if (item.isSales == 0 ? $("#chkbxsales").prop("checked", false) : $("#chkbxsales").prop("checked", true));
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function AddWO() {
            var iscollection = $(chkbxcollection).prop("checked") == false ? "0" : "1";
            var isfoom = $(chkbxfoom).prop("checked") == false ? "0" : "1";
            var iscuttingforglass = $(chkbxcfglass).prop("checked") == false ? "0" : "1";
            var iscleaning = $(chkbxcleaning).prop("checked") == false ? "0" : "1";
            var isglassfitting = $(chkbxglassfitting).prop("checked") == false ? "0" : "1";
            var ispainting = $(chkbxpainting).prop("checked") == false ? "0" : "1";
            var isframeinside = $(chkbxframeinside).prop("checked") == false ? "0" : "1";
            var isframeoutside = $(chkbxframeoutside).prop("checked") == false ? "0" : "1";
            var isdoor = $(chkbxdoor).prop("checked") == false ? "0" : "1";
            var isrepair = $(chkbxrepair).prop("checked") == false ? "0" : "1";
            var issales = $(chkbxsales).prop("checked") == false ? "0" : "1";

            //var dat2a = 'id=0&quantity=' + $("#txtquantity").val() + '&serialno=' + $("#txtserialno").val() + '&width=' + $("#txtwidth").val() + '&height=' + $("#txtheight").val() + '&color=' + $("#txtcolor").val() + '&doortypeid=' + $("#lstdoortype").val() + '&iscollection=' + iscollection + '&isfoom=' + isfoom + '&iscuttingforglass=' + iscuttingforglass + '&iscleaning=' + iscleaning + '&isglassfitting=' + isglassfitting + '&ispainting=' + ispainting + '&isframeinside=' + isframeinside + '&isframeoutside=' + isframeoutside + '&isdoor=' + isdoor + '&notes=' + $("#txtnote").val();
            //alert(dat2a);
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditWO',
                data: 'id=0&quantity=' + $("#txtquantity").val() + '&serialno=' + $("#txtserialno").val() + '&width=' + $("#txtwidth").val()
                + '&height=' + $("#txtheight").val() + '&color=' + $("#txtcolor").val() + '&wallwidth=' + $("#txtwallwidth").val() + '&doortypeid=' + $("#lstdoortype").val()
                + '&iscollection=' + iscollection + '&isfoom=' + isfoom + '&iscuttingforglass=' + iscuttingforglass + '&iscleaning=' + iscleaning + '&isglassfitting=' + isglassfitting
                + '&ispainting=' + ispainting + '&isframeinside=' + isframeinside + '&isframeoutside=' + isframeoutside + '&isdoor=' + isdoor + '&isrepair=' + isrepair
                + '&issales=' + issales + '&notes=' + $("#txtnote").val() + '&supervisorid=' + $("#lstsupr").val() + '&hours=' + $("#txthrs").val()
                + '&iscompleted=' + $("#lstwoddst").val(),
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "Work Order has been saved successfully");
                        resetForm();
                        getSearchWorkOrder();
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function EditWO() {
            var id = $("#hidEditID").val();

            var iscollection = $(chkbxcollection).prop("checked") == false ? "0" : "1";
            var isfoom = $(chkbxfoom).prop("checked") == false ? "0" : "1";
            var iscuttingforglass = $(chkbxcfglass).prop("checked") == false ? "0" : "1";
            var iscleaning = $(chkbxcleaning).prop("checked") == false ? "0" : "1";
            var isglassfitting = $(chkbxglassfitting).prop("checked") == false ? "0" : "1";
            var ispainting = $(chkbxpainting).prop("checked") == false ? "0" : "1";
            var isframeinside = $(chkbxframeinside).prop("checked") == false ? "0" : "1";
            var isframeoutside = $(chkbxframeoutside).prop("checked") == false ? "0" : "1";
            var isdoor = $(chkbxdoor).prop("checked") == false ? "0" : "1";
            var isrepair = $(chkbxrepair).prop("checked") == false ? "0" : "1";
            var issales = $(chkbxsales).prop("checked") == false ? "0" : "1";

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditWO',
                data: 'id=' + id + '&quantity=' + $("#txtquantity").val() + '&serialno=' + $("#txtserialno").val() + '&width=' + $("#txtwidth").val()
                + '&height=' + $("#txtheight").val() + '&color=' + $("#txtcolor").val() + '&wallwidth=' + $("#txtwallwidth").val() + '&doortypeid=' + $("#lstdoortype").val()
                + '&iscollection=' + iscollection + '&isfoom=' + isfoom + '&iscuttingforglass=' + iscuttingforglass + '&iscleaning=' + iscleaning
                + '&isglassfitting=' + isglassfitting + '&ispainting=' + ispainting + '&isframeinside=' + isframeinside + '&isframeoutside=' + isframeoutside + '&isdoor=' + isdoor
                + '&isrepair=' + isrepair + '&issales=' + issales + '&notes=' + $("#txtnote").val() + '&supervisorid=' + $("#lstsupr").val() + '&hours=' + $("#txthrs").val()
                + '&iscompleted=' + $("#lstwoddst").val(),
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "Work Order has been updated successfully");
                        resetForm();
                        getSearchWorkOrder();
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function Redirecttodetial(woid) {
            window.location.href = 'WorkOrderDoorDetails.aspx?wo=' + woid;
        }

        function formatAMPM(date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        }

        function confirmWODelete(id) {
            $("#hidDelID").val(id);
            jQuery('#modal-delete-wo').modal('show', { backdrop: 'static' });
        }

        function DeleteWO() {
            var id = $('input#hidDelID').val();

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/DeleteWO',
                data: "id=" + id,
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('warning', 'top right', 'Deleted', 'Work Order deleted successfully.');
                        getSearchWorkOrder();
                        $('#modal-delete-doortype').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function binddoortypes() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetDoorTypes',
                data: 'id=0',
                async: true,
                success: function (data) {
                    $("#lstdoortype").append("").val("").html("");
                    $("#lstdoortype").append($("<option></option>").val("-1").html("Select"));

                    $("#lstsearchdtypes").append("").val("").html("");
                    $("#lstsearchdtypes").append($("<option></option>").val("-1").html("Select"));

                    $.each(data, function (key, val) {
                        $("#lstdoortype").append($("<option></option>").val(val.ID).html(val.DoorTypeName));

                        $("#lstsearchdtypes").append($("<option></option>").val(val.ID).html(val.DoorTypeName));
                    });
                    $('#lstdoortype').selectpicker('refresh');

                    $('#lstsearchdtypes').selectpicker('refresh');
                }
            });
        }

        $('#lstdoortype').change(function () {
            getserialno($('#lstdoortype').val());
        });

        function getserialno(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetDoorTypes',
                data: 'id=' + id,
                async: true,
                success: function (data) {
                    $("#txtserialno").val("");
                    $.each(data, function (key, val) {
                        $("#txtserialno").val(val.SerialNumber);
                    });
                }
            });
        }

        function bindTabletUsers() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTabletUsers',
                data: "user=0&tabletroleid=1",
                async: true,
                success: function (data) {
                    $("#lstsupr").append("").val("").html("");

                    $.each(data, function (key, val) {
                        $("#lstsupr").append($("<option></option>").val(val.ID).html(val.Name));
                    });
                    $('#lstsupr').selectpicker('refresh');
                }
            });
        }

    </script>
</asp:Content>


