﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Lockscreen.aspx.cs" Inherits="Lockscreen" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Locked screen for neama application">
    <meta name="author" content="Neama">

    <link rel="shortcut icon" href="assets/images/favicon_1.ico">

    <title>Neama | Lock Screen</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    <script src="assets/js/modernizr.min.js"></script>
</head>
<body>
    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
        <div class=" card-box">

            <div class="panel-body">
                <form id="form1" class="text-center" runat="server">
                    <div class="user-thumb">
                        <img src="assets/images/users/8912.jpg" class="img-responsive img-circle img-thumbnail" alt="thumbnail">
                    </div>
                    <div class="form-group">
                        <h3><%= Session["Admin_Name"] %></h3>
                        <p class="text-muted">
                            Enter your password to access the admin.
                        </p>
                        <div class="input-group m-t-30">
                            <input type="password" id="txtpassword" name="password" class="form-control" placeholder="Password" required="" runat="server" />
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-pink w-sm waves-effect waves-light">
                                    Log In
                                </button>
                            </span>
                        </div>
                    </div>

                </form>


            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 text-center">
                <p>
                    Not <%= Session["Admin_Name"] %>?<a href="Login.aspx" class="text-primary m-l-5"><b>Sign In</b></a>
                </p>
            </div>
        </div>

    </div>
    <footer class="footer text-center">
        <%= DateTime.Now.Year.ToString()  %> © Neama Powered by <a href="http://www.e-amanah.com/">Amanah Teknologia</a>
    </footer>
    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/jquery.validate.min.js"></script>

    <script src="assets/plugins/notifyjs/dist/notify.min.js"></script>
    <script src="assets/plugins/notifications/notify-metro.js"></script>

    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>


    <script type="text/javascript">

        function disableBack() { window.history.forward() }

        window.onload = disableBack();
        window.onpageshow = function (evt) { if (evt.persisted) disableBack() }


        resetform();
        var resizefunc = [];
        var baseurl = 'Default.aspx';

        $("#form1").validate({
            rules: {
                "password": "required",
            },
            messages: {
                "password": "Password is required",
            },
            submitHandler: function (form) {
                login();
                return false;
            }
        });

        function resetform() {
            $("#txtpassword").val("");
        }

        function login() {
            var username = '<%= Session["Admin_Username"] %>';
            $.ajax({
                url: 'AdminWeb.asmx/LoginAdmin',
                method: 'POST',
                dataType: 'text',
                data: "username=" + username + "&password=" + $("#txtpassword").val(),
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("textStatus->" + textStatus);
                    alert("errorThrown->" + errorThrown);
                    //if (textStatus == "timeout") {
                    //    alert("Request Timeout");
                    //}
                },
                success: function (response) {
                    //alert(response);
                    // Login status [success|invalid]
                    var login_status = response;


                    // If login is invalid, we store the 
                    if (login_status == 'invalid') {
                        resetform();
                        $.Notification.autoHideNotify('error', 'top right', 'Invalid Login', 'Invalid Username / Password.');
                    }
                    else
                        if (login_status == 'success') {
                            $.Notification.autoHideNotify('success', 'top right', 'Success', 'You have been Logged In SuccessFully');
                            var redirect_url = baseurl;

                            if (response.redirect_url && response.redirect_url.length) {
                                redirect_url = response.redirect_url;
                            }
                            window.location.href = redirect_url;
                        }
                }
            });

        }
    </script>
</body>
</html>
