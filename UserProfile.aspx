﻿<%@ Page Language="C#" AutoEventWireup="false" CodeFile="UserProfile.aspx.cs" Inherits="UserProfile" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="assets/images/favicon_1.ico">

    <title>Neama | UserProfile</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    <script src="assets/js/modernizr.min.js"></script>

</head>
<body>

    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
        <div class=" card-box">
            <div class="panel-heading">
                <h3 class="text-center">Change Profile <strong class="text-primary">Neama</strong> </h3>
            </div>

            <div class="panel-body">
                <form id="frmuserprofile" name="frmuserprofile" class="form-horizontal m-t-20">

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <label for="field-3" class="control-label">Email</label>
                            <input class="form-control" type="email" id="txtemail" name="txtemail" placeholder="Email">
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <label for="field-3" class="control-label">Username</label>
                            <input class="form-control" type="text" readonly="readonly" id="txtusername" maxlength="15" name="txtusername" placeholder="Username">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <label for="field-3" class="control-label">Password</label>
                            <input class="form-control" type="password" id="txtpass" maxlength="15" name="txtpass" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label for="field-3" class="control-label">New Password</label>
                            <input class="form-control" type="password" id="txtnewpass" maxlength="15" name="txtnewpass" placeholder="Password">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <button type="button" class="btn btn-default waves-effect" id="btnback">Back</button>
                            <button class="btn btn-primary waves-effect waves-light" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
    <footer class="footer text-right">
        <%= DateTime.Now.Year.ToString()  %> © Neama Powered by <a href="http://www.e-amanah.com/">Amanah Teknologia</a>
    </footer>


    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/jquery.validate.min.js"></script>

    <script src="assets/plugins/notifyjs/dist/notify.min.js"></script>
    <script src="assets/plugins/notifications/notify-metro.js"></script>

    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>
    <script>
        var resizefunc = [];
        var baseurl = 'Default.aspx';
        resetform();
        $.validator.addMethod("noSpace", function (value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "Space are not allowed")

        getuserdetails();

        $("#frmuserprofile").validate({
            rules: {
                "txtemail": {
                    required: true,
                    email: true,
                },
                "txtnewpass": "required",
                "txtpass": "required",
                "txtusername": "required",
            },
            messages: {
                "txtnewpass": { required: true, noSpace: true },
                "txtpass": { required: true, noSpace: true },
                "txtusername": "Username is required",
                "txtemail": {
                    required: "Email is required",
                    email: "Please enter proper Email"
                },
            },
            submitHandler: function (form) {
                changeuserprofile()
                return false;
            }
        });

        $("#btnback").click(function () {
            window.location.href = baseurl;
        });

        function resetform() {
            $("#txtusername").val("");
            $("#txtpass").val("");
            $("#txtnewpass").val("");
            $("#txtemail").val("");
        }

        function changeuserprofile() {
            var userid = '<%= Session["Admin_ID"] %>';

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/ChangeUserProfile',
                data: "id=" + userid + "&username=" + $("#txtusername").val() + "&email=" + $("#txtemail").val() + "&password=" + $("#txtpass").val() + "&newpassword=" + $("#txtnewpass").val() + "&count=0",
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "User Profile has been reset successfully");
                        $("#txtpass").val("");
                        $("#txtnewpass").val("");
                    }
                    else {
                        $.Notification.autoHideNotify('white', 'top right', 'Password', 'Please enter Correct Password.!!');
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function getuserdetails() {
            var userid = '<%= Session["Admin_ID"] %>';

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetUsers',
                data: 'user=' + userid,
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        $("#txtusername").val(item.TB_USERNAME);
                        $("#txtemail").val(item.TB_EMAIL);
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');

                }
            });
        }

    </script>
</body>
</html>
