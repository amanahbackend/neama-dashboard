﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportTank.aspx.cs" Inherits="ReportTank" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Plugin Css-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <%--<link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />--%>
    <link href="assets/plugins/datatables/newdatatables/buttons.bootstrap4.min.css" rel="stylesheet" />
    <link href="assets/plugins/datatables/newdatatables/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <link href="assets/plugins/datatables/newdatatables/responsive.bootstrap4.min.css" rel="stylesheet" />

    <link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- Plugins css-->
    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />

    <link href="assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Report Tank</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li>
                                <a href="#">Reports</a>
                            </li>
                            <li class="active">Report Tank
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Work order No.</label>
                                    <input type="text" class="form-control" id="txtsearchwono" name="txtsearchwono" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Quantity</label>
                                    <input type="text" class="form-control" id="txtsearchquantity" name="txtsearchquantity" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Supervisor</label>
                                    <select id="lstsuper" name="lstsuper" class="selectpicker" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Technician</label>
                                    <select id="lsttech" name="lsttech" class="selectpicker" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label for="field-1" class="control-label">From Date</label>
                            </div>
                            <div class="col-md-2">
                                <label for="field-1" class="control-label">To Date</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="From Date" id="txtfrmdate" />
                                    <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="To Date" id="txttodate" />
                                    <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-primary waves-effect waves-light" onclick="clearsearch()">Clear</button>
                                <button class="btn btn-primary waves-effect waves-light" onclick="getSearchWorkOrder()">Search</button>
                            </div>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->

                <div class="panel">
                    <div class="panel-body">
                        <div class="" style='overflow: scroll; overflow-y: hidden;'>
                            <table id="tablelist" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>WO No.</th>
                                        <th>Quantity</th>
                                        <th>Supervisor</th>
                                        <th>Technician</th>
                                        <th>Action Done</th>
                                        <th>Created Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
    </div>
    <!-- modal-Page -->




    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <!-- Required datatable js -->
    <script src="assets/plugins/datatables/newdatatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="assets/plugins/datatables/newdatatables/dataTables.buttons.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/buttons.bootstrap4.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/jszip.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/pdfmake.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/vfs_fonts.js"></script>
    <script src="assets/plugins/datatables/newdatatables/buttons.html5.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/buttons.print.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="assets/plugins/datatables/newdatatables/dataTables.responsive.min.js"></script>
    <script src="assets/plugins/datatables/newdatatables/responsive.bootstrap4.min.js"></script>


    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>

    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/moment.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.js"></script>
    <script src="Scripts/filedownload.js"></script>

    <script>

        $(document).ready(function () {
            var resizefunc = [];
            var currentDate = new Date();

            // Date Picker

            $('#txtfrmdate').datetimepicker({ format: 'DD/MM/YYYY' });
            $('#txttodate').datetimepicker({ format: 'DD/MM/YYYY' });

            bindTabletSupervisor();
            bindTabletTechnician();
            getSearchWorkOrder();
        });

        function clearsearch() {
            $('#txtfrmdate').val("");
            $('#txttodate').val("");
            $('#txtsearchwono').val("");
            $('#txtsearchquantity').val("");
            $('#lstsuper').val("0");
            $('#lstsuper').selectpicker('refresh');
            $('#lsttech').val("0");
            $('#lsttech').selectpicker('refresh');
            getSearchWorkOrder();
        }

        function getSearchWorkOrder() {

            var frmdate = $('#txtfrmdate').val();
            var todate = $('#txttodate').val();

            var fd = '', td = '';
            if (frmdate == null || frmdate == '') {

            }
            else {
                frmdate = $("#txtfrmdate").val().split('/');
                fd = frmdate[1] + '/' + frmdate[0] + '/' + frmdate[2] + ' 00:00:00';

            }
            if (todate == null || todate == '') {

            }
            else {
                todate = $("#txttodate").val().split('/');
                td = todate[1] + '/' + todate[0] + '/' + todate[2] + ' 23:59:59';
            }
            //alert($('#txtsearchwono').val());
            var woid = $('#txtsearchwono').val();
            if ($('#txtsearchwono').val() == null || $('#txtsearchwono').val() == '') {
                woid = 0;
            }
            var quantity = $('#txtsearchquantity').val();
            if ($('#txtsearchquantity').val() == null || $('#txtsearchquantity').val() == '') {
                quantity = 0;
            }
            var supr = $('#lstsuper').val();
            if ($('#lstsuper').val() == null || $('#lstsuper').val() == "0") {
                supr = 0;
            }
            var tech = $('#lsttech').val();
            if ($('#lsttech').val() == null || $('#lsttech').val() == "0") {
                tech = 0;
            }

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetReportTank',
                data: 'woid=' + woid + '&quantity=' + quantity + '&supid=' + supr + '&techid=' + tech + '&frmdate=' + fd + '&todate=' + td,
                async: true,
                success: function (data) {

                    $('#tablelist').dataTable().fnClearTable();
                    $('#tablelist').dataTable().fnDestroy();

                    $.each(data, function (i, item) {
                        var trHTML = '';


                        var date = new Date(parseInt(item.CreatedDate.replace("/Date(", "").replace(")/", ""), 10));
                        var ds = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + ' ' + formatAMPM(date);

                        trHTML += '<tr><td>' + item.WOID + '</td><td>' + item.WOQuantity + '</td><td>' + item.supervisorname + '</td><td>' + item.technicianname + '</td><td>' + item.ActionDone + '</td><td>' + ds + '</td></tr>';
                        $('#tablelist').append(trHTML);
                    });
                    var table = $('#tablelist').DataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "bSort": false,
                        buttons: ['excel']
                    });

                    table.buttons().container()
                            .appendTo('#tablelist_wrapper .col-md-6:eq(0)');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }
        function formatAMPM(date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        }


        function bindTabletSupervisor() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTabletUsers',
                data: "user=0&tabletroleid=1",
                async: true,
                success: function (data) {
                    $("#lstsuper").append("").val("").html("");
                    $("#lstsuper").append($("<option></option>").val("0").html("Select"));
                    $.each(data, function (key, val) {
                        $("#lstsuper").append($("<option></option>").val(val.ID).html(val.Name));
                    });
                    $('#lstsuper').selectpicker('refresh');
                }
            });
        }
        function bindTabletTechnician() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetTabletUsers',
                data: "user=0&tabletroleid=2",
                async: true,
                success: function (data) {
                    $("#lsttech").append("").val("").html("");
                    $("#lsttech").append($("<option></option>").val("0").html("Select"));
                    $.each(data, function (key, val) {
                        $("#lsttech").append($("<option></option>").val(val.ID).html(val.Name));
                    });
                    $('#lsttech').selectpicker('refresh');
                }
            });
        }

    </script>
</asp:Content>





