﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserRole.aspx.cs" Inherits="UserRole" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Plugin Css-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

    <!-- Plugins css-->
    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <style>
        .permissionGrid {
            background-color: #EFEFEF;
            width: 100%;
        }

            .permissionGrid th {
                font-weight: bold;
            }

            .permissionGrid th, td {
                padding: 5px;
                text-align: center;
                border-left: solid 1px #FFF;
                border-bottom: solid 1px #FFF;
            }

        input.largerCheckbox {
            width: 15px;
            height: 15px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Roles</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li>
                                <a href="#">User</a>
                            </li>
                            <li class="active">Roles
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="m-b-30">
                                    <button class="btn btn-primary waves-effect waves-light btnAdd" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="resetForm()">Add New</button>
                                </div>
                            </div>
                        </div>

                        <div class="" style='overflow: scroll; overflow-y: hidden;'>
                            <table id="userroletable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->

    </div>

    <div class="modal fade" id="modal-delete-confirm" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Confirm</h4>
                </div>

                <div class="modal-body">
                    Are you sure, you want to delete current record!
                </div>

                <div class="modal-footer">
                    <input type="hidden" id="hidDeleteID" name="deleteID" value="0" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info" id="btnDelete" onclick="deleteData()">Continue</button>
                </div>
            </div>
        </div>
    </div>


    <!-- sample modal content -->
    <div class="modal fade bs-example-modal-lg" id="modal-AddEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="width: 60%;">
            <div class="modal-content">
                <form name="frmAddEdit" id="frmAddEdit" method="post">
                    <input type="hidden" id="hidEditID" name="editID" value="0" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Role Detail</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Name</label>
                                    <input type="text" class="form-control" id="txtname" maxlength="30" name="name" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">User Type</label>
                                    <select id="lstusrtyp" name="lstusrtyp" class="selectpicker" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Permissions</label>
                                </div>
                            </div>
                        </div>
                        <div id="dvslmdp" class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxdoor" type="checkbox" />
                                        <label for="chkbxdoor">
                                            Door Type
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxtank" type="checkbox" />
                                        <label for="chkbxtank">
                                            Tank Type
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxgrp" type="checkbox" />
                                        <label for="chkbxgrp">
                                            GRP Type
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxgft" type="checkbox" />
                                        <label for="chkbxgft">
                                            Glass Fyber Type
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="chkbxalumtype" type="checkbox" />
                                        <label for="chkbxalumtype">
                                            Aluminium Type
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="dvfm" class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <%--<label for="field-1" class="control-label">Permissions</label>--%>
                                    <div id="permissionGrid"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="form-group">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal" onclick="resetForm()">Close</button>
                                <button class="btn btn-primary waves-effect waves-light" type="submit">Save changes</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            var resizefunc = [];

            $("#frmAddEdit").validate({
                rules: {
                    "name": "required",
                },
                messages: {
                    "name": "Role Name is required",
                },
                submitHandler: function (form) {
                    saveChanges();
                    return false;
                }
            });

            resetForm();
            loadPermissionPages();
            getUserRoles();
            bindusertypes();
        });

        $('#lstusrtyp').change(function () {
            hideshow($('#lstusrtyp').val());
        });

        function hideshow(id) {
            if (id == 1 || id == "") {
                $("#dvslmdp").hide();
                $("#dvfm").show();
            } else {
                $("#dvslmdp").show();
                $("#dvfm").hide();
            }
        }

        function loadPermissionPages() {
            return $.ajax(
            {
                dataType: "json",
                type: "Post",
                data: "",
                url: 'AdminWeb.asmx/GetPermissionPages',
                context: $("#permissionGrid"),
                beforeSend: function () {
                    $(this).addClass("loadingInput");
                },
                complete: function () {
                    $(this).removeClass("loadingInput");
                },
                success: function (data) {
                    permissionPages = data;
                    loadPermissionTypes();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    /*alert("textStatus->" + textStatus);
                    alert("errorThrown->" + errorThrown);
                    if (textStatus == "timeout") {
                        alert("Request Timeout");
                    }*/
                }
            });
        }

        function loadPermissionTypes() {
            return $.ajax(
            {
                dataType: "json",
                type: "Post",
                data: "",
                url: 'AdminWeb.asmx/GetPermissionTypes',
                context: $("#permissionGrid"),
                beforeSend: function () {
                    $(this).addClass("loadingInput");
                },
                complete: function () {
                    $(this).removeClass("loadingInput");
                },
                success: function (data) {
                    permissionTypes = data;
                    grid = "<table class='permissionGrid'><tr><th>Modules</th>";
                    for (t = 0; t < permissionTypes.length; t++) {
                        grid += "<th>" + permissionTypes[t]["TB_TYPE"] + "</th>";
                    }
                    grid += "</tr>";

                    for (p = 0; p < permissionPages.length; p++) {
                        grid += "<tr><td>" + permissionPages[p]["TB_PAGE_TITLE"] + "</td>";

                        for (t = 0; t < permissionTypes.length; t++) {
                            grid += "<td>";
                            if (permissionPages[p]["TB_PERM_TYPES"].indexOf(permissionTypes[t]["TB_ID"]) > -1) {
                                grid += "<input type='checkbox' id='chk_" + permissionPages[p]["TB_ID"] + "_" + permissionTypes[t]["TB_ID"] + "' class='chkPerm largerCheckbox' data-id='" + permissionPages[p]["TB_ID"] + "_" + permissionTypes[t]["TB_ID"] + "'>";
                            }
                            grid += "</td>";
                        }

                        grid += "</tr>";
                    }

                    grid += "</table>";

                    $(this).html(grid);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    /*alert("textStatus->" + textStatus);
                    alert("errorThrown->" + errorThrown);
                    if (textStatus == "timeout") {
                        alert("Request Timeout");
                    }*/
                }
            });
        }

        function saveChanges() {
            var allowdoortype = $(chkbxdoor).prop("checked") == false ? "0" : "1";
            var allowalutype = $(chkbxalumtype).prop("checked") == false ? "0" : "1";
            var allowtanktype = $(chkbxtank).prop("checked") == false ? "0" : "1";
            var allowgrptype = $(chkbxgrp).prop("checked") == false ? "0" : "1";
            var allowgfhtype = $(chkbxgft).prop("checked") == false ? "0" : "1";

            permissions = "";
            $(".chkPerm").each(function () {
                permissions += $(this).attr("data-id") + "=" + $(this).prop("checked") + ",";
            });
            permissions = permissions.substring(0, permissions.length - 1);
            var usertypeid = $("#lstusrtyp").val();
            if (usertypeid == null || usertypeid == "") {
                $.Notification.autoHideNotify('warning', 'top right', 'User Type', 'Please select User Type.');
            }
            else {
                return $.ajax({
                    dataType: "json",
                    type: "Post",
                    url: 'AdminWeb.asmx/SaveRoles',
                    data: "id=" + $("#hidEditID").val() + "&name=" + $("#txtname").val() + "&permissions=" + permissions + "&allowdoortype=" + allowdoortype
                    + "&allowalutype=" + allowalutype + "&allowtanktype=" + allowtanktype + "&allowgrptype=" + allowgrptype + "&allowgfhtype=" + allowgfhtype
                    + "&usertypeid=" + $("#lstusrtyp").val(),
                    //context: $("#table-list"),
                    success: function (data) {
                        if (data[0]["Result"] > 0) {
                            $.Notification.autoHideNotify('success', 'top right', 'Success', 'Role has been saved successfully.');
                            $('#modal-AddEdit').modal('toggle');
                            resetForm();
                            getUserRoles();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        /*alert("textStatus->" + textStatus);
                        alert("errorThrown->" + errorThrown);
                        if (textStatus == "timeout") {
                            alert("Request Timeout");
                        }*/
                    }
                });
            }
        }

        function bindusertypes() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetUserTypes',
                data: 'id=0',
                async: true,
                success: function (data) {
                    $("#lstusrtyp").append("").val("").html("");
                    $("#lstusrtyp").append($("<option></option>").val("").html("Select"));

                    $.each(data, function (key, val) {
                        $("#lstusrtyp").append($("<option></option>").val(val.ID).html(val.UserTypesName));
                    });
                    $('#lstusrtyp').selectpicker('refresh');
                    hideshow($('#lstusrtyp').val());
                }
            });
        }

        function getUserRoles() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetRoles',
                data: "role=0&showadmin=0",
                async: true,
                success: function (data) {

                    $('#userroletable').dataTable().fnClearTable();
                    $('#userroletable').dataTable().fnDestroy();
                    $.each(data, function (i, item) {

                        var trHTML = '';

                        trHTML += '<tr><td>' + item.TB_NAME + '</td><td>' +
                            '<button type="button" class="btn btn-info waves-effect btnEdit" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="filluserroledata(' + item.TB_ID + ')"><i class="fa fa-pencil-square-o"></i></button><button class="btn btn-danger waves-effect waves-light btnDelete" onclick="confirmDelete(' + item.TB_ID + ')"><i class="fa fa-times"></i></button>' + '</td></tr>';

                        $('#userroletable').append(trHTML);

                    });

                    $('#userroletable').dataTable({
                        "bPaginate": true,
                        "bLengthChange": true,
                        "bFilter": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "bSort": false
                        //order: [[0, 'desc']]
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function filluserroledata(id) {
            resetForm();
            $.ajax(
            {
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetRoles',
                data: "role=" + id + "&showadmin=1",
                async: true,
                context: $("#table-list-data"),
                //beforeSend: function () {
                //    blockIt("table-list-data");
                //},
                //complete: function () {
                //    unblockIt("table-list-data");
                //},
                success: function (data) {
                    $("#hidEditID").val(data[0].TB_ID);
                    $("#txtname").val(data[0].TB_NAME);
                    $('#lstusrtyp').val(data[0].TB_USER_TYPE_ID);
                    $('#lstusrtyp').selectpicker('refresh');
                    hideshow(data[0].TB_USER_TYPE_ID);

                    if (data[0].AllowDoorType == 0 ? $("#chkbxdoor").prop("checked", false) : $("#chkbxdoor").prop("checked", true));
                    if (data[0].AllowAluminiumType == 0 ? $("#chkbxalumtype").prop("checked", false) : $("#chkbxalumtype").prop("checked", true));
                    if (data[0].AllowTankType == 0 ? $("#chkbxtank").prop("checked", false) : $("#chkbxtank").prop("checked", true));
                    if (data[0].AllowGRPType == 0 ? $("#chkbxgrp").prop("checked", false) : $("#chkbxgrp").prop("checked", true));
                    if (data[0].AllowGFHType == 0 ? $("#chkbxgft").prop("checked", false) : $("#chkbxgft").prop("checked", true));

                    //alert(data[0].TB_NAME)
                    permissions = data[0].TB_PERMISSIONS.split(",");
                    for (i = 0; i < permissions.length; i++) {
                        if (permissions[i].split("=")[1] == "1") {
                            $("#chk_" + permissions[i].split("=")[0]).prop("checked", true);
                        }
                        else {
                            $("#chk_" + permissions[i].split("=")[0]).prop("checked", false);
                        }
                    }
                    jQuery('#modal-AddEdit').modal('show', { backdrop: 'static' })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    /*alert("textStatus->" + textStatus);
                    alert("errorThrown->" + errorThrown);
                    if (textStatus == "timeout") {
                        alert("Request Timeout");
                    }*/
                }
            });
        }

        function editData(id) {
            editItem = $("#listObj" + id);
            $("#hidEditID").val(editItem.attr("data-id"));
            $("#txtName").val(editItem.attr("data-name"));
            permissions = editItem.attr("data-permissions").split(",");
            for (i = 0; i < permissions.length; i++) {
                if (permissions[i].split("=")[1] == "1") {
                    $("#chk_" + permissions[i].split("=")[0]).prop("checked", true);
                }
                else {
                    $("#chk_" + permissions[i].split("=")[0]).prop("checked", false);
                }
            }
            jQuery('#modal-AddEdit').modal('show', { backdrop: 'static' })
        }

        function resetForm() {
            $("#chkbxdoor").prop("checked", false);
            $("#chkbxtank").prop("checked", false);
            $("#chkbxgrp").prop("checked", false);
            $("#chkbxgft").prop("checked", false);
            $("#chkbxalumtype").prop("checked", false);
            $('#lstusrtyp').val("");
            $('#lstusrtyp').selectpicker('refresh');
            hideshow($('#lstusrtyp').val());
            $("#hidEditID").val("0");
            $("#hidDeleteID").val("0");
            $("#txtname").val("");
            $(".chkPerm").prop("checked", false);

            var validator = $("#frmAddEdit").validate();
            validator.resetForm();
        }

        function confirmDelete(id) {
            $("#hidDeleteID").val(id);
            jQuery('#modal-delete-confirm').modal('show', { backdrop: 'static' });
        }

        function deleteData() {
            if ($("#hidDeleteID").val() != "0") {
                return $.ajax(
                {
                    dataType: "json",
                    type: "Post",
                    data: "role=" + $("#hidDeleteID").val(),
                    url: 'AdminWeb.asmx/DeleteRoles',
                    context: $("#lstArea"),
                    //beforeSend: function () {
                    //    blockIt("table-list");
                    //},
                    //complete: function () {
                    //    unblockIt("table-list");
                    //},
                    success: function (data) {
                        if (data[0]["Result"] == "-1") {
                            alert("Role cannot be deleted");
                        }
                        else if (data[0]["Result"] > 0) {
                            $.Notification.autoHideNotify('warning', 'top right', 'Deleted', 'User role deleted successfully.');
                            $('#modal-delete-confirm').modal('toggle');
                            resetForm();
                            getUserRoles();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        /*alert("textStatus->" + textStatus);
                        alert("errorThrown->" + errorThrown);
                        if (textStatus == "timeout") {
                            alert("Request Timeout");
                        }*/
                    }
                });
            }
        }

    </script>


</asp:Content>




