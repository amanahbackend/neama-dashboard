﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Dashboard</h4>
                        <p class="text-muted page-title-alt">Welcome to Neama !</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget-panel widget-s-1 bg-white">
                            <i class="md md-assessment text-dark"></i>
                            <h2 id="lblcreated" class="m-0 text-dark font-600"></h2>
                            <div class="text-muted m-t-5">WO Created</div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget-panel widget-s-1 bg-white">
                            <i class="md md-assignment text-primary"></i>
                            <h2 id="lblcompleted" class="m-0 text-dark font-600"></h2>
                            <div class="text-muted m-t-5">Completed WO</div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget-panel widget-s-1 bg-white">
                            <i class="md  md-assignment-late text-warning"></i>
                            <h2 id="lblpending" class="m-0 text-dark font-600"></h2>
                            <div class="text-muted m-t-5">Pending WO</div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6">
                        <div class="widget-panel widget-style-2 bg-white">
                            <i class="md md-account-child text-custom"></i>
                            <h2 id="lbluser" class="m-0 text-dark font-600"></h2>
                            <div class="text-muted m-t-5">Users</div>
                        </div>
                    </div>
                </div>

                <!-- end row -->


            </div>
            <!-- container -->

        </div>
        <!-- content -->
    </div>

    <script src="assets/pages/jquery.dashboard.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $.Notification.autoHideNotify('custom', 'top right', 'Welcome', 'Welcome to the Neama Home Page.');
            getDashboardStatistics();
        });


        function getDashboardStatistics() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetDashboardStatistics',
                data: "",
                async: true,
                success: function (data) {
                    $.each(data, function (i, val) {

                        $("#lblcreated").text(val.WOCreated);
                        $("#lblpending").text(val.WOPending);
                        $("#lblcompleted").text(val.WOCompleted);
                        $("#lbluser").text(val.Users);
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

    </script>
</asp:Content>


