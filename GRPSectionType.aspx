﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GRPSectionType.aspx.cs" Inherits="GRPSectionType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Plugin Css-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />
    <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

    <link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- Plugins css-->
    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">GRP Section Types</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="Default.aspx">Home</a>
                            </li>
                            <li>
                                <a href="#">Settings</a>
                            </li>
                            <li class="active">GRP Section Types
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">

                        <%--<div class="row">
                            <div class="col-sm-6">
                                <div class="m-b-30">
                                    <button class="btn btn-primary waves-effect waves-light btnAdd" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="resetForm()">Add New</button>
                                </div>
                            </div>
                        </div>--%>

                        <div class="" style='overflow: scroll; overflow-y: hidden;'>
                            <table id="tablelist" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Process Station</th>
                                        <th>Machine</th>
                                        <th>Product</th>
                                        <th>Picture</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->
                </div>
                <!-- end Panel -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
    </div>

    <!-- MODAL -->
    <div class="modal fade" id="modal-delete-record" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Confirm</h4>
                </div>

                <div class="modal-body">
                    Are you sure, you want to delete current record!
                </div>

                <div class="modal-footer">
                    <input type="hidden" id="hidDelID" name="deleteID" value="0" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info" id="btnDelete" onclick="DeleteGRPSetionType()">Continue</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end Modal -->


    <!-- sample modal content -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" id="modal-AddEdit" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form name="frmAddEdit" id="frmAddEdit" method="post">
                    <input type="hidden" id="hidEditID" name="hidEditID" value="0" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="resetForm()">×</button>
                        <h4 class="modal-title" id="myModalLabel">GRP Section Detail</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Process Station</label>
                                    <select class="selectpicker" id="lstprostation" name="lstprostation" data-live-search="true" data-style="btn-white">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Machine</label>
                                    <input type="text" class="form-control" id="txtmachine" name="txtmachine" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Product Name</label>
                                    <input type="text" class="form-control" id="txtproductname" name="txtproductname" placeholder="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Picture</label>
                                    <input type="file" id="filPicture" name="filPicture" class="filestyle" data-placeholder="No file" data-size="sm" data-max-option="1" accept="image/*" data-buttonname="btn-white" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal" onclick="resetForm()">Close</button>
                            <button class="btn btn-primary waves-effect waves-light" type="submit" id="btnaddedit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <script type="text/javascript">
        var resizefunc = [];

        $(document).ready(function () {
            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#frmAddEdit").validate({
                rules: {
                    "txtmachine": "required",

                },
                messages: {
                    "txtmachine": "Machine Name  is required",
                },
                submitHandler: function (form) {
                    if ($("#hidEditID").val() > 0) {
                        EditGRPSectionType();
                    }
                    else {
                        AddGRPSectionType();
                    }
                    return false;
                }
            });
            bindprocessstations();
            getHistoryData();

        });

        function resetForm() {
            $("#hidEditID").val("0");
            $("#hidDelID").val("0");
            $("#txtmachine").val("");
            $("#txtproductname").val("");
            $("#lstprostation").val("0");
            $('#lstprostation').selectpicker('refresh');
            $("#filPicture").val("");

            $("#frmAddEdit").trigger('reset');
            var validator = $("#frmAddEdit").validate();
            validator.resetForm();
        }

        function getHistoryData() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetGRPSectionTypes',
                data: 'id=0' + '&productname=' + $("#txtproductname").val(),
                async: true,
                success: function (data) {

                    $('#tablelist').dataTable().fnClearTable();
                    $('#tablelist').dataTable().fnDestroy();

                    $.each(data, function (i, item) {
                        var trHTML = '', picture = '';
                        picture = '<img src="' + item.Picture + '" width="100" height="50" />';

                        trHTML += '<tr><td>' + item.ProcessStationName + '</td><td>' + item.MachineNumber + '</td><td>' + item.ProductName + '</td><td>' + picture + '</td><td>' +
                            '<button type="button" class="btn btn-info waves-effect btnEdit" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="FillGRPSectionType(' + item.ID + ')"><i class="fa fa-pencil-square-o"></i></button><button class="btn btn-danger waves-effect waves-light btnDelete" onclick="confirmDelete(' + item.ID + ')"><i class="fa fa-times"></i></button>' + '</td></tr>';
                        $('#tablelist').append(trHTML);
                    });
                    $('#tablelist').dataTable({
                        "bPaginate": true,
                        "bLengthChange": true,
                        "bFilter": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "bSort": false
                        //order: [[0, 'desc']]
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function FillGRPSectionType(id) {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetGRPSectionTypes',
                data: 'id=' + id + '&productname=' + $("#txtproductname").val(),
                async: true,
                success: function (data) {
                    $.each(data, function (i, item) {
                        //$("#txtproductname").val(item.Productname);

                        $("#lstprostation").val(item.ProcessStationID);
                        $('#lstprostation').selectpicker('refresh');
                        $("#txtproductname").val(item.ProductName);
                        $("#txtmachine").val(item.MachineNumber);
                        $("#hidEditID").val(item.ID);
                    })
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function AddGRPSectionType() {
            var picture = "";

            var dd = new FormData();
            var d = "id=" + $("#hidEditID").val();
            d += '&processstaionid=' + $("#lstprostation").val();
            d += '&machine=' + $("#txtmachine").val();
            d += '&productname=' + $("#txtproductname").val();

            var files = $("#filPicture").get(0).files;
            if (files.length > 0) {
                dd.append("picture", files[0]);
            }
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditGRPSectionType?' + d,
                data: dd,
                cache: false,
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "GRP Section Type has been saved successfully");
                        resetForm();
                        getHistoryData();
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function EditGRPSectionType() {
            var picture = "";

            var dd = new FormData();
            var d = "id=" + $("#hidEditID").val();
            d += '&processstaionid=' + $("#lstprostation").val();
            d += '&machine=' + $("#txtmachine").val();
            d += '&productname=' + $("#txtproductname").val();

            var files = $("#filPicture").get(0).files;
            if (files.length > 0) {
                dd.append("picture", files[0]);
            }
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/AddEditGRPSectionType?' + d,
                data: dd,
                cache: false,
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('success', 'top right', 'Success', "GRP Section Type has been updated successfully");
                        resetForm();
                        getHistoryData();
                        $('#modal-AddEdit').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function confirmDelete(id) {
            $("#hidDelID").val(id);
            jQuery('#modal-delete-record').modal('show', { backdrop: 'static' });
        }

        function DeleteTankType() {
            var id = $('input#hidDelID').val();

            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/DeleteGRPSectionType',
                data: "id=" + id,
                async: true,
                success: function (data) {
                    if (data[0]["Result"] > 0) {
                        $.Notification.autoHideNotify('warning', 'top right', 'Deleted', 'GRP Section Type deleted successfully.');
                        getHistoryData();
                        $('#modal-delete-record').modal('toggle');
                    }
                    else {
                        $.Notification.autoHideNotify('error', 'top right', 'Error', 'Something went Wrong Please try again.!!');
                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function bindprocessstations() {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: 'AdminWeb.asmx/GetProcessStation',
                data: 'id=0',
                async: true,
                success: function (data) {
                    $("#lstprostation").append("").val("").html("");
                    //$("#lstprostation").append($("<option></option>").val("0").html("Select"));

                    $.each(data, function (key, val) {
                        $("#lstprostation").append($("<option></option>").val(val.ID).html(val.StationName));
                    });
                    $('#lstprostation').selectpicker('refresh');
                }
            });
        }


    </script>

    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>
</asp:Content>







